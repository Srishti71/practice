package April_25;

import java.util.ArrayList;
import java.util.List;

public class RestoreIP {

    List<String> result = new ArrayList<>();

    // "2.5525511135"
    public void restoreIP(String curr, String currString, int start, int dotCount) {

        if (dotCount == 3) {
            if (curr.length() <= 3) {
                int val = Integer.parseInt(curr);
                if ((val <= 255 && val > 0 && !curr.startsWith("0")) || (val == 0 && curr.length() == 1)) {
                    currString += "." + curr;
                    result.add(currString);
                    System.out.println(currString);
                    return;
                }
            }
        }

        for (int i = start + 1; i <= start + 3 && i < curr.length(); i++) { // 0
            String newSub = curr.substring(start, i); //

            int currValue = Integer.parseInt(newSub);
            if ((currValue <= 255 && currValue > 0 && !newSub.startsWith("0")) || (currValue == 0 && newSub.length() == 1)) {
                if (currString.isEmpty()) {
                    restoreIP(curr.substring(i), newSub, 0, dotCount + 1);
                } else {
                    restoreIP(curr.substring(i), currString + "." + newSub, 0, dotCount + 1);
                }
            }

        }
    }

    public static void main(String s[]) {
        RestoreIP restore = new RestoreIP();
        restore.restoreIP("010010", "", 0, 0);
    }
}
