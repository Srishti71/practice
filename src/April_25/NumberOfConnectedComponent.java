package April_25;

import java.util.*;

public class NumberOfConnectedComponent {

    Map<Integer, List<Integer>> map = new HashMap<>();
    Set<Integer> seen = new HashSet<>();

    public int countComponents(int n, int[][] edges) {

        for (int i[] : edges) {
            List<Integer> list1 = map.getOrDefault(i[0], new ArrayList());
            List<Integer> list2 = map.getOrDefault(i[1], new ArrayList());
            list1.add(i[1]);
            list2.add(i[0]);
            map.put(i[0], list1);
            map.put(i[1], list2);

        }
        System.out.println(map);

        int count = 0;
        for (int i = 0; i < n; i++) {
            if (!seen.contains(i)) {

                dfs(i);
                count++;
            }
        }
        System.out.println(count);
        return count;

    }

    private void dfs(int current) {
        seen.add(current);
        List<Integer> list = map.get(current);
        if (list != null) {

            for (int i : list) {
                if (!seen.contains(i)) {
                    dfs(i);
                }
            }
        }
    }

    public static void main(String s[]) {
        NumberOfConnectedComponent numb = new NumberOfConnectedComponent();
        int[][] edges = {{0, 1}, {1, 2}, {0, 2}, {3, 4}};
        numb.countComponents(5, edges);
    }
}
