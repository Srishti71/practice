package April_25;

import java.util.*;

class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {
        val = 0;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val) {
        val = _val;
        neighbors = new ArrayList<Node>();
    }

    public Node(int _val, ArrayList<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
}

public class CloneGraph {


    public static void main(String s[]) {

    }

    Map<Node, Node> map = new HashMap<>();
    Set<Node> visited = new HashSet();


    public Node cloneGraph(Node node) {

        Node newNode = map.getOrDefault(node, new Node(node.val));
        map.put(node, newNode);

        return dfs(node, newNode);
    }

    private Node dfs(Node node, Node newNode) {

        visited.add(node);

        for (Node nodes : node.neighbors) {
            if(!visited.contains(nodes)) {
                Node neighbour = map.getOrDefault(nodes, new Node(node.val));
                map.put(nodes, neighbour);
                newNode.neighbors.add(nodes);
                dfs(nodes, neighbour);
            }
        }
        return newNode;
    }
}
