package March_20;

import java.util.*;

public class SubListWithDistinctInt {

    public int countSubStrWithDistinctInt(int[] A, int K) {

        int len = A.length;
        List<Integer> list = new ArrayList<>();

        for (int a : A) {
            list.add(a);
        }

        int count = 0;
        for (int i = 0; i <= len - K; i++) {
            for (int j = i + K; j <= len; j++) {
                if (checkForDistinctChar(list.subList(i, j), K))
                    count++;
            }
        }

        System.out.println(count);

        return count;
    }


    private boolean checkForDistinctChar(List<Integer> curr, int k) {
        Set<Integer> element = new HashSet<>();
        for (int i = 0; i < curr.size(); i++) {
            element.add(curr.get(i));
        }

        if (element.size() == k)
            return true;

        return false;
    }

    public static void main(String s[]) {
        SubListWithDistinctInt sub = new SubListWithDistinctInt();
        int[] array = {1, 2, 1, 3, 4};
        sub.countSubStrWithDistinctInt(array, 3);
    }
}



