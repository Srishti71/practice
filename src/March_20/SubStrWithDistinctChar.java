package March_20;

import java.util.HashSet;
import java.util.Set;

public class SubStrWithDistinctChar {

    public int countSubStrWithDistinctChar(String s, int k) {

        int len = s.length();

        int count = 0;
        for (int i = 0; i <= len-k; i++) {
            for (int j = i + k; j <= len; j++) {
                if (checkForDistinctChar(s.substring(i, j), k))
                    count++;
            }
        }

        System.out.println(count);

        return count;
    }

    private boolean checkForDistinctChar(String curr, int k) {
        Set<Character> element = new HashSet<>();
        for (int i = 0; i < curr.length(); i++) {
            element.add(curr.charAt(i));
        }

        if (element.size() == k)
            return true;

        return false;
    }

    public static void main(String s[]) {
        SubStrWithDistinctChar sub = new SubStrWithDistinctChar();
        sub.countSubStrWithDistinctChar("pqpqs", 2);
    }

}
