package March_20;

import javafx.util.Pair;

import java.util.*;

// https://leetcode.com/discuss/interview-question/356150

public class TreasureIsland2 {

    public int findMinimumRoute(char[][] grid) {

        List<Integer> source = new ArrayList<>();

        Set<Integer> dest = new HashSet<>();

        int r = grid.length;
        int c = grid[0].length;

        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                if (grid[i][j] == 'S') {
                    source.add(i * c + j);
                } else if (grid[i][j] == 'X') {
                    dest.add(i * c + j);
                }
            }
        }
        int[][] dir = {{0, 1}, {1, 0}, {-1, 0}, {0, -1}};
        int min = Integer.MAX_VALUE;

        for (int s : source) {
            Queue<Pair<Integer, Integer>> queue = new LinkedList<Pair<Integer, Integer>>();
            Set<Integer> visited = new HashSet();
            queue.add(new Pair(s, 0));

            while (!queue.isEmpty()) {
                Pair curr = queue.poll();
                int x = (Integer) curr.getKey();
                visited.add(x);
                int value = (Integer) curr.getValue();
                if (dest.contains(x)) {
                    //queue.clear();
                    min = Math.min(min, value);
                    break;
                }
                for (int[] d : dir) {
                    int x1 = (x / c + d[0]);
                    int y1 = (x % c + d[1]);
                    int newCur = x1 * c + y1;
                    if (x1 >= 0 && x1 < r && y1 >= 0 && y1 < c && !visited.contains(newCur) && grid[x1][y1] != 'D' ) {
                        queue.add(new Pair(newCur, value + 1));
                    }
                }
            }
        }

        return min;

    }

    public static void main(String s[]) {
        TreasureIsland2 treasure = new TreasureIsland2();
        char[][] grid = {{'S', 'O', 'O', 'S', 'S'},
                {'D', 'O', 'D', 'O', 'D'},
                {'O', 'O', 'O', 'O', 'X'},
                {'X', 'D', 'D', 'O', 'O'},
                {'X', 'D', 'D', 'D', 'O'}};

        System.out.println(treasure.findMinimumRoute(grid));
    }
}
