package March_20;

public class SpiralMatrix {

    public int[][] generateMatrix(int n) {

        int[][] matrix = new int[n][n];
        int v = 1;
        for (int k = 0; k <= n / 2; k++) {
            for (int j = k; j < n - k; j++) {
                matrix[k][j] = v;
                v++;
            }

            for (int j = k + 1; j < n - k; j++) {
                matrix[j][n - 1 - k] = v;
                v++;
            }

            for (int j = n - k - 2; j >= k; j--) {
                matrix[n - 1 - k][j] = v;
                v++;
            }

            for (int j = n - k - 2; j > k; j--) {
                matrix[j][k] = v;
                v++;
            }

        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.println(matrix[i][j]);
            }
        }
        return matrix;
    }
    public static void main(String s[]) {
        SpiralMatrix sm = new SpiralMatrix();
        sm.generateMatrix(1);
    }
}
