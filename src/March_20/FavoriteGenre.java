package March_20;

import java.util.*;

public class FavoriteGenre {

    public Map<String, List<String>> findfavoritegenre(Map<String, List<String>> userMap, Map<String, List<String>> genreMap) {


        Map<String, String> songToGenre = new HashMap<>();

        for (String g : genreMap.keySet()) {

            List<String> song = genreMap.get(g);

            for (String s : song) {
                songToGenre.put(s, g);

            }
        }

        Map<String, List<String>> result = new HashMap<>();

        for (String user : userMap.keySet()) {
            List<String> songs = userMap.get(user);
            int max = 0;
            Map<String, Integer> genreCount = new HashMap<>();

            for (String song : songs) {
                String gen = songToGenre.get(song);
                genreCount.put(gen, genreCount.getOrDefault(gen, 0) + 1);
                max = Math.max(genreCount.get(gen), max);
            }


            for (String s : genreCount.keySet()) {
                if (genreCount.get(s) == max) {
                    List<String> listOfGenre = result.getOrDefault(user, new ArrayList<>());
                    listOfGenre.add(s);
                    result.put(user, listOfGenre);
                }
            }
        }
        System.out.println(result);
        return result;
    }

    public static void main(String s[]) {
        FavoriteGenre fav = new FavoriteGenre();

        Map<String, List<String>> userSongs = new HashMap<>();
        userSongs.put("David", Arrays.asList("song1", "song2", "song3", "song4", "song8"));
        userSongs.put("Emma", Arrays.asList("song5", "song6", "song7"));

        Map<String, List<String>> genreSongs = new HashMap<>();
        genreSongs.put("Rock", Arrays.asList("song1", "song3"));
        genreSongs.put("Dubstep", Arrays.asList("song7"));

        genreSongs.put("Techno", Arrays.asList("song2", "song4"));
        genreSongs.put("pop", Arrays.asList("song5", "song6"));
        genreSongs.put("Jazz", Arrays.asList("song8", "song9"));

        fav.findfavoritegenre(userSongs, genreSongs);
    }
}
