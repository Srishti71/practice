package March_20;

import March_19.ReorderDataLogs;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

// https://leetcode.com/problems/reorganize-string/
public class ReorganiseString {

    public String reorganizeString(String S) {
        Map<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < S.length(); i++) {
            map.put(S.charAt(i), map.getOrDefault(S.charAt(i), 0) + 1);
        }

        PriorityQueue<Pair<Character, Integer>> pq = new PriorityQueue<Pair<Character, Integer>>((a, b) -> b.getValue() - a.getValue());

        for(Character c: map.keySet()) {
            pq.add(new Pair(c, map.get(c)));
        }

        char prev = ' ';
        StringBuilder sb = new StringBuilder();

        while (!pq.isEmpty()) {
            Pair pair = pq.poll();
            Character cur = (Character) pair.getKey();
            if (cur != prev) {
                sb.append(cur);
                int value = (Integer) pair.getValue() - 1;
                if (value > 0) {
                    pq.add(new Pair(cur, value));
                }
                prev = cur;
            } else {
                if (pq.isEmpty()) {
                    return " ";
                } else {

                    Pair newPair = pq.poll();
                    Character newCur = (Character) newPair.getKey();
                    pq.add(pair);

                    sb.append(newCur);
                    int value = (Integer) newPair.getValue() - 1;
                    if (value > 0) {
                        pq.add(new Pair(newCur, value));
                    }
                    prev = newCur;
                }
            }
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    public static void main(String s[]) {
        String S = "aaab";
        ReorganiseString re = new ReorganiseString();
        re.reorganizeString(S);
    }
}
