package March_20;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.Queue;

public class MaxMinAltitudes {

    int max = Integer.MIN_VALUE;

    public int findMaxMinAltitude(int[][] matrix) {
        bfs(matrix);
        return max;
    }

    public void bfs(int[][] matrix) {

        int maxRes = Integer.MIN_VALUE;
        Queue<Pair<Integer, Integer>> queue = new LinkedList<>();
        int r = matrix.length, c = matrix[0].length;

        int[][] dirs = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};
        boolean visited[][] = new boolean[r][c];
        queue.add(new Pair(0, Integer.MAX_VALUE));
        visited[0][0] = true;

        while (!queue.isEmpty()) {

            Pair ele = queue.poll();
            int e = (Integer) ele.getKey();
            int x = e / c;
            int y = e % c;
            int minSoFar = (Integer) ele.getValue();

            System.out.print (x + " " + y + " ---- " );
            for (int dir[] : dirs) {
                int x1 = x + dir[0];
                int y1 = y + dir[1];

                if (x1 >= 0 && x1 < r && y1 >= 0 && y1 < c && !visited[x1][y1]) {
                    if (x1 == r - 1 && y1 == c - 1) {
                        maxRes = Math.max(minSoFar, maxRes);
                    } else {
                        int minValue = Math.min(minSoFar, matrix[x1][y1]);
                        queue.add(new Pair(x1 * c + y1, minValue));
                        visited[x1][y1] = true;
                    }

                }
            }
        }
        System.out.println("Max Result: " + maxRes);

    }

    public static void main(String s[]) {
        MaxMinAltitudes maxMin = new MaxMinAltitudes();
        int[][] matrux = {{3, 4, 6, 3, 4},
                {0, 2, 1, 1, 7},
                {8, 8, 3, 2, 7},
                {4, 1, 2, 0, 0},
                {4, 6, 5, 4, 3}};
        maxMin.findMaxMinAltitude(matrux);
    }


}
