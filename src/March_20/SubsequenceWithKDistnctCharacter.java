package March_20;

import java.util.HashSet;
import java.util.Set;

public class SubsequenceWithKDistnctCharacter {

    Set<String> set = new HashSet<>();

    public int countSubStrWithDistinctChar(String s, int k) {

        generateSetOfDistinctChar(s, k, 0, "");
        return set.size();
    }

    private void generateSetOfDistinctChar(String s, int k, int index, String curr) {

        if (curr.length() >= k && !set.contains(curr)) {
            checkForDistinctChar(curr, k);
        }

        if (index >= s.length()) {
            return;
        }

        for (int i = index; i < s.length(); i++) {
            String newCur = curr + s.charAt(i);
            generateSetOfDistinctChar(s, k, i + 1, newCur);
        }
    }

    private void checkForDistinctChar(String curr, int k) {
        Set<Character> element = new HashSet<>();

        for (int i = 0; i < curr.length(); i++) {
            element.add(curr.charAt(i));
        }

        if (element.size() == k)
            set.add(curr);
    }

    public static void main(String s[]) {
        SubsequenceWithKDistnctCharacter sub = new SubsequenceWithKDistnctCharacter();
        sub.countSubStrWithDistinctChar("pqpqs", 2);
        System.out.println(sub.set);
        System.out.println(sub.set.size());
    }
}
