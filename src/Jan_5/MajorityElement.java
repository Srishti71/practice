package Jan_5;

import java.util.ArrayList;
import java.util.List;

public class MajorityElement {

    public List<Integer> majorityElement(int[] nums) {

        List<Integer> list = new ArrayList<>();

        int element1 = 0, count1 = 0, element2 = 0, count2 = 0, element3 = 0, count3 = 0;
        int idx = 1;
        for (int i = 0; i < nums.length; i++) {

            if (nums[i] == element1) {
                count1++;
                continue;
            }

            if (nums[i] == element2) {
                count2++;
                continue;
            }

            if (nums[i] == element3) {
                count3++;
                continue;
            }

            switch (idx) {
                case 0:
                    count1--;
                    if (count1 <= 0) {
                        element1 = nums[i];
                        count1 = 1;
                    }
                    break;

                case 1:
                    count2--;
                    if (count2 <= 0) {
                        element2 = nums[i];
                        count2 = 1;
                    }
                    break;

                case 2:
                    count3--;
                    if (count3 <= 0) {
                        element3 = nums[i];
                        count3 = 1;
                    }
                    break;

            }
            idx = (idx + 1) % 3;
        }

        int totalA = 0, totalB = 0, totalC = 0;

        for (int i : nums) {
            if (i == element1) totalA++;
            else if (i == element2) totalB++;
            else if (i == element3) totalC++;
        }

        if (count1 > 0 && totalA > (nums.length / 3)) list.add(element1);
        if (count2 > 0 && totalB > (nums.length / 3)) list.add(element2);
        if (count3 > 0 && totalC > (nums.length / 3)) list.add(element3);

        return list;
    }
}
