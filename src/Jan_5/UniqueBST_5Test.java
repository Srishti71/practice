package Jan_5;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UniqueBST_5Test {

    UniqueBST_5 uniqueBST_5;
    List<TreeNode> treeNodeList;

    @org.junit.Before
    public void setUp() throws Exception {
        uniqueBST_5 = new UniqueBST_5();
        treeNodeList = new ArrayList<>();
        treeNodeList.clear();
    }


    @Test
    public void givenOneReturnArrayWithOne() {
        treeNodeList.add(new TreeNode(1));
        assertEquals(treeNodeList.get(0).val, uniqueBST_5.generateTrees(1).get(0).val);
    }

    @Test
    public void givenTwoReturnArrayOfTwoTreeNodes() {
        TreeNode node = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        node.right = node2;

        TreeNode node3 = new TreeNode(1);
        TreeNode node4 = new TreeNode(2);
        node4.left = node3;

        treeNodeList.add(node);
        treeNodeList.add(node4);

        List<TreeNode> list = uniqueBST_5.generateTrees(2);
        assertEquals(treeNodeList.size(), list.size());

        int i = 0;
        for(TreeNode tn : treeNodeList) {
            assertEquals(tn.val, list.get(i).val);
        }
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }
}