package Jan_5;

import java.util.Stack;

public class KthSmallestElement {

    int value = 0;
    int element = 0;

    public int kthSmallest(TreeNode root, int k) {
        int value = findSmallestElement(root, k);
        System.out.println("Value: " + value);
        return element;
    }

    public int findSmallestElement(TreeNode root, int k) {
        Stack<TreeNode> stack = new Stack();

        while (true) {

            while (root != null) {
                stack.add(root);
                root = root.left;
            }
            if (stack.isEmpty())
                return -1;
            root = stack.pop();
            if (--k == 0) return root.val;

            root = root.right;
        }
    }

    public static void main(String args[]) {

        KthSmallestElement kthSmallestElement = new KthSmallestElement();
        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);
        TreeNode node6 = new TreeNode(6);

        node5.left = node3;
        node5.right = node6;

        node3.right = node4;
        node3.left = node2;
        node2.left = node1;

        kthSmallestElement.kthSmallest(node5, 3);
    }
}