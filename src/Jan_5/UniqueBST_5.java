package Jan_5;

import java.util.ArrayList;
import java.util.List;

public class UniqueBST_5 {
    TreeNode node;

    public static void main(String s[]) {
        UniqueBST_5 uniqueBST_5 = new UniqueBST_5();
        uniqueBST_5.generateTrees(3);
    }

    public List<TreeNode> generateTrees(int n) {
        List<TreeNode> nodeList = generateTrees(1,n);
        printListElement(nodeList);
       return nodeList;
    }

    private List<TreeNode> generateTrees(int start, int n) {

        List<TreeNode> list = new ArrayList<>();

        if(start == n) {
            TreeNode node = new TreeNode(start);
            list.add(node);
            return list;
        } else if(start > n) {
            return null;
        }

        for( int i = start; i <= n; i++) {

            List<TreeNode> left = generateTrees(start, i-1);
            List<TreeNode> right = generateTrees(i + 1, n);

            if(left == null && right != null) {
                for(TreeNode node : right) {
                    TreeNode newNode = new TreeNode(i);
                    newNode.right = node;
                    list.add(newNode);
                }
            }

            else if(left != null && right == null) {
                for(TreeNode node : left) {
                    TreeNode newNode = new TreeNode(i);
                    newNode.left = node;
                    list.add(newNode);
                }
            } else if(left != null && right != null) {
                for(TreeNode l: left) {
                    for(TreeNode r: right) {
                        TreeNode newNode = new TreeNode(i);
                        newNode.left = l;
                        newNode.right = r;
                        list.add(newNode);
                    }
                }
            }
        }
        return list;
    }



    private void printListElement(List<TreeNode> newList) {
        for (TreeNode tree : newList) {
            System.out.println("Print Element: ");
            printTree(tree);
            System.out.println();
        }
    }

    private void printTree(TreeNode tree) {

        TreeNode node = tree;

        if (node != null) {
            System.out.print(node.val + " ");
            printTree(node.left);
            printTree(node.right);
        }
    }
}
