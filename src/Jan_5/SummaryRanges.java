package Jan_5;

import java.util.ArrayList;
import java.util.List;

/*
https://leetcode.com/problems/summary-ranges/
 */
public class SummaryRanges {

    public List<String> summaryRanges(int[] nums) {
        List<String> strArray = new ArrayList<>();

        if (nums.length == 0) {
            return strArray;
        } else if (nums.length == 1) {
            strArray.add(nums[0] + "");
            return strArray;
        }

        int start = nums[0], prev = nums[0];

        for (int i = 1; i < nums.length; i++) {

            if (prev + 1 == nums[i]) {
                prev = nums[i];
                continue;
            } else {
                if (start == prev) {
                    strArray.add(prev + "");
                } else {
                    strArray.add(start + "->" + prev);
                }
                start = nums[i];
                prev = nums[i];
            }
        }

        if (start == prev) {
            strArray.add(prev + "");
        } else {
            strArray.add(start + "->" + prev);
        }

        return strArray;
    }

    public static void main(String s[]) {
        SummaryRanges summaryRanges = new SummaryRanges();
        int[] nums = {0, 1};
        System.out.println(summaryRanges.summaryRanges(nums));
    }
}
