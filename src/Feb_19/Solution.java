package Feb_19;

import common.TreeNode;

import java.util.Stack;

public class Solution {

    double closestDist = Integer.MAX_VALUE;
    int closestNum = 0;

    public static void main(String s[]) {

        Solution solution = new Solution();
        TreeNode root = new TreeNode(4);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(5);
        TreeNode node3 = new TreeNode(1);
        TreeNode node4 = new TreeNode(3);


        //closestValue(root.left, target);

        //closestValue(root.right, target);

        System.out.println(solution.simplifyPath("/home//foo/"));
        System.out.println(solution.closestNum);
    }
    public int closestValue(TreeNode root, double target) {

        if(root == null) {
            return 0;
        }

        closestDist = Math.abs(root.val- target);
        closestNum = root.val;

        findClosestNumber(root.left, target);

        findClosestNumber(root.right, target);
        return closestNum;
    }


    public void findClosestNumber(TreeNode root, double target) {

        if(root != null) {
            double currDist = Math.abs(root.val - target);
            if(currDist < closestDist) {
                closestDist = currDist;
                closestNum = root.val;
            }
            findClosestNumber(root.left, target);
            findClosestNumber(root.right, target);

        }

    }

    public String simplifyPath(String path) {
        if(path.isEmpty()) {
            return "";
        }

        Stack<String> stack = new Stack();

        String paths[] = path.split("/");
        for( String p: paths) {
            if(p.isEmpty() || p.equals(".")){
                continue;
            }

            else if(p.equals("..")) {
                if(!stack.isEmpty())
                    stack.pop();
            } else {
                stack.add(p);
            }
        }

        String sb = new String();

        while(!stack.isEmpty()) {
            if(!sb.isEmpty())
                sb = stack.pop()+"/"+sb;
            else {
                sb= stack.pop();
            }
        }
        return sb.length() > 0 ? "/"+sb : "/";
    }
}
