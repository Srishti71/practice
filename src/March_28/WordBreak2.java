package March_28;

import Jan_26.WordLadder;

import java.util.*;

public class WordBreak2 {

    Set<String> result = new HashSet<>();
    Map<String, Set> map = new HashMap<>();

    public static void main(String s[]) {
        WordBreak2 wordBreak2 = new WordBreak2();
        wordBreak2.wordBreakTwo("catsanddog", Arrays.asList("cat", "cats", "and", "sand", "dog"));
    }

    public Set<String> wordBreakTwo(String s, List<String> dict) {
        int len = s.length();
        LinkedList<String>[] dp = new LinkedList[len + 1];
        LinkedList<String> initial = new LinkedList();
        initial.add("");
        dp[0] = initial;

        for (int i = 1; i <= len; i++) {
            LinkedList list = new LinkedList();
            for (int j = 0; j < i; j++) {
                if (dp[j].size() > 0 && dict.contains(s.substring(j, i))) {
                    for (String s1 : dp[j]) {
                        list.add(s1 + (s1.equals("") ? "" : " ") + s.substring(j, i));
                    }
                }
            }
            System.out.println(i + " " + list);
            dp[i] = list;
        }

        System.out.println("Result: "+dp[len]);
        return new HashSet(dp[len]);

    }



}
