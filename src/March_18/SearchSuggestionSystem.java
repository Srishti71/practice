package March_18;

import common.Trie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchSuggestionSystem {

    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        List<List<String>> results = new ArrayList<>();

        Arrays.sort(products);
        Trie root = new Trie();
        for (String prod : products) {
            Trie ptr = root;

            for (int i = 0; i < prod.length(); i++) {
                int index = prod.charAt(i) - 'a';

                if (ptr.children[index] == null) {
                    ptr.children[index] = new Trie();
                }

                Trie t = ptr.children[index];
                if (t.words.size() < 3) {
                    t.words.add(prod);
                }
                ptr = ptr.children[index];
            }
        }

        Trie ptr = root;
        boolean patternFound = true;

        for (int i = 0; i < searchWord.length(); i++) {
            int index = searchWord.charAt(i) - 'a';
            if (ptr.children[index] != null && patternFound) {
                ptr = ptr.children[index];
                results.add(ptr.children[index].words);
            } else {
                patternFound = false;
                results.add(new ArrayList<>());
            }
        }
        return results;
    }

    public void print(String[] products) {
        for (String s : products) {
            System.out.println(s);
        }
    }

    public static void main(String s[]) {
        String products[] = {"mobile", "mouse", "moneypot", "monitor", "mousepad"}, searchWord = "mouse";

        SearchSuggestionSystem suggestionSystem = new SearchSuggestionSystem();
        suggestionSystem.suggestedProducts(products, searchWord);
    }
}
