package March_18;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

class Tomatoes {
    int x;
    int y;
    int time;

    public Tomatoes(int x, int y, int time) {
        this.x = x;
        this.y = y;
        this.time = time;
    }
}

public class RottenTomatoes {

    public int orangesRotting(int[][] grid) {
        Queue<Integer> queue = new LinkedList<>();

        int n = grid.length;
        int m = grid[0].length;
        int[][] dir = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (grid[i][j] == 2) {
                    int code = i * m + j;
                    queue.add(code);
                    map.put(code, 0);
                }

            }
        }

        int min = 0;

        while (!queue.isEmpty()) {
            System.out.println("Inside");
            int t = queue.poll();
            int x = t / m;
            int y = t % n;
            for (int[] d : dir) {
                int row = x + d[0];
                int col = y + d[1];
                if (row >= 0 && row < n && col >= 0 && col < m && grid[row][col] == 1) {
                    grid[row][col] = 2;
                    int code = row * m + col;
                    queue.add(code);
                    map.put(code, map.get(t) + 1);
                    min = map.get(code);
                }

            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (grid[i][j] == 1)
                    return -1;
            }
        }
        return min;
    }

    public static void main(String s[]) {
        RottenTomatoes rottenTomatoes = new RottenTomatoes();
        int grid[][] = {{2, 1, 1},
                {1, 1, 0},
                {0, 1, 1}};
        System.out.println(rottenTomatoes.orangesRotting(grid));
    }
}
