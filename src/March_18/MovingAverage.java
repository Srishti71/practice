package March_18;

import java.util.LinkedList;
import java.util.Queue;

public class MovingAverage {

    int capacity = 0;
    Queue<Integer> queue = new LinkedList<>();
    int size = 0;
    double sum = 0.0;

    public MovingAverage(int size) {
        capacity = size;
    }

    public double next(int val) {
        int removeElement = 0;
        if(!queue.isEmpty() && size == capacity) {
            removeElement = queue.poll();
            size--;
        }

        queue.add(val);
        size++;
        sum += val - removeElement;
        return (sum /size);
    }

    public static void main(String s[]) {
        MovingAverage m = new MovingAverage(3);
        System.out.println(m.next(1) );
        System.out.println(m.next(10));// = (1 + 10) / 2
        System.out.println(m.next(3));// = (1 + 10 + 3) / 3
        System.out.println(m.next(5));// = (10 + 3 + 5) / 3
    }
}
