package March_18;

import March_7.MaximalSquare;

public class MaxConsecutiveOnes {

    public int findMaxConsecutiveOnes(int[] nums) {
        int maxValue = 0;

        int currOnes = 0;
        for( int i : nums) {
            if(i ==0){
                maxValue = Math.max(currOnes, maxValue);
                currOnes = 0;
            } else {
                currOnes += 1;
            }
        }

        maxValue = Math.max(maxValue, currOnes);
        System.out.println(maxValue);
        return maxValue;
    }

    public static void main(String s[]) {

        MaxConsecutiveOnes maxConsecutiveOnes = new MaxConsecutiveOnes();
        int[] nums = {1,1,0,1,1,1};
        maxConsecutiveOnes.findMaxConsecutiveOnes(nums);
    }

}
