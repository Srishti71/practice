package March_18;

import java.util.*;

public class FrequentReviewKeyword {

    public List<String> findKMostFrequentElements(List<String> keywords, List<String> reviews, int k) {

        Map<String, Integer> map = new TreeMap<>();
        for (String review : reviews) {
            String[] words = review.split(" ");
            for (String w : words) {
                for (String element : keywords) {
                    if (w.trim().equalsIgnoreCase(element)) {
                        map.put(w, map.getOrDefault(w, 0) + 1);
                        break;
                    }
                }
            }
        }

        List<Map.Entry<String, Integer>> list = new ArrayList(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> t, Map.Entry<String, Integer> t1) {
                return t1.getValue().compareTo(t.getValue());// - t1;
            }
        });

        System.out.println(list);
        List<String> results = new ArrayList<>();
        for (int i = 0; i < k && i < list.size(); i++) {
            results.add(list.get(i).getKey());
        }
        System.out.println(results);
        return results;
    }


    public static void main(String s[]) {
        FrequentReviewKeyword frequent = new FrequentReviewKeyword();
        List<String> keywords = Arrays.asList("anacell", "betacellular", "cetracular", "deltacellular", "eurocell", "anacell", "betacellular", "cetracular", "anacell", "betacellular", "cetracular");
        String[] keywordsNew = {"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"};

        List<String> reviews = Arrays.asList("I love anacell Best services; Best services provided by anacell",
                "betacellular has great services",
                "deltacellular provides much better services than betacellular",
                "cetracular is worse than anacell",
                "Betacellular is better than deltacellular.");
        int k = 4;

        //frequent.findKMostFrequentElements(keywords, reviews, k);

        frequent.topKFrequent(keywordsNew, k);
    }


    public List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> map = new TreeMap<>();
        for (String w : words) {
            map.put(w, map.getOrDefault(w, 0) + 1);
        }

        List<String> list = new ArrayList<String>(map.keySet());
        Collections.sort(list, (a, b) -> map.get(b) - map.get(a));
        List<String> results = new ArrayList<>();

        for (int i = 0; i < k && i < map.size(); i++) {
            results.add(list.get(i));
        }
        System.out.println(results);
        return results;
    }

    public List<Integer> topKFrequent(int[] words, int k) {
        Map<Integer, Integer> map = new TreeMap<>();
        for (int w : words) {
            map.put(w, map.getOrDefault(w, 0) + 1);
        }

        List<Integer> list = new ArrayList();

        for (int ele : map.keySet()) {
            list.add(ele);
        }

        Collections.sort(list, (a, b) -> map.get(b) - map.get(a));

        return list.subList(0, k);
    }
}
