package March_3;

/*
https://leetcode.com/problems/tiling-a-rectangle-with-the-fewest-squares/
 */
public class TileRectangeWithSquares {

    public static void main(String s[]) {
        TileRectangeWithSquares tileRectangeWithSquares = new TileRectangeWithSquares();
        tileRectangeWithSquares.tilingRectangle(2, 3);
    }

    public int tilingRectangle(int n, int m) {
        int memo[][] = new int[n + 1][m + 1];
        int value = computeMinSquares(n, m, memo);
       // System.out.println(value);
        return value;
    }

    private int computeMinSquares(int n, int m, int[][] memo) {

        System.out.println(n + " " + m);
        if (n == 0) return 0;
        if (m == 0) return 0;

        if (n == 1) return m;
        if (m == 1) return n;

        if (memo[n][m] != 0) return memo[m][n];
        int noSqures = Integer.MAX_VALUE;

        int largestSquare = Math.min(m, n);

        for (int i = 1; i <= largestSquare; i++) {

            int up = computeMinSquares(n - i, i, memo);
            int leftDiag = computeMinSquares(n, m - i, memo);

            int left = computeMinSquares(i, m - i, memo);
            int upDiag = computeMinSquares(n - i, m, memo);
            noSqures = Math.min(noSqures, Math.min(up + leftDiag, left + upDiag));
        }

        if (noSqures != Integer.MAX_VALUE) ++noSqures;

        memo[n][m] = noSqures;
        return noSqures;
    }
}
