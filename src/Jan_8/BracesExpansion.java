package Jan_8;

import java.util.ArrayList;
import java.util.List;

public class BracesExpansion {
    public List<String> performBracesExpension(String word, int index) {

        List<String> listOfWords = new ArrayList(); // lisOfWord = []  lisOfWord = []
        StringBuilder newString = new StringBuilder(); // "" ""
        List<String> nextList = new ArrayList();

        for (int i = index; i < word.length(); i++) { // abcd{e,f}gh  e,f}gh
            if (word.charAt(i) == '{') {
                if (newString.length() != 0) {
                    listOfWords.add(newString.toString()); // lisOfWord = [abcd]
                    newString.setLength(0); // newString = ""
                }
                nextList = performBracesExpension(word, i+1); //


            } else if (word.charAt(i) == '}') {
                if (newString.length() != 0) {

                    listOfWords.add(newString.toString()); // listOfWords = [e, f]
                    newString.setLength(0); // newString = ""
                }
                return listOfWords;
            } else if (word.charAt(i) == ',') {
                if (newString.length() != 0) {
                    listOfWords.add(newString.toString()); // listOfWords = [e]
                    newString.setLength(0); // newString = ""
                }
            } else {
                newString.append(word.charAt(i)); // newString = abcd  newString = f
            }
        }
        if (newString.length() != 0) {
            listOfWords.add(newString.toString());
        }

        System.out.println(listOfWords);
        return listOfWords;
    }

    public static void main(String s[]) {
        BracesExpansion bracesExpansion = new BracesExpansion();
        System.out.print(bracesExpansion.performBracesExpension("abcd{e,f}gh", 0));
    }
}
