package Jan_8;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BracesExpansionTest {

    BracesExpansion bracesExpansion;
    @Before
    public void setUp() {
        bracesExpansion = new BracesExpansion();
    }
    @Test
    public void givenStringWithoutBarcesReturnsBraces() {
        String word ="abcd";
        List<String> list = new ArrayList<>();
        list.add("ab{cd");
        assertEquals(list, bracesExpansion.performBracesExpension(word, 0));
    }
}