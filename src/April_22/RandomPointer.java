package April_22;


class Node {
    int val;
    Node next;
    Node random;

    public Node(int val) {
        this.val = val;
        this.next = null;
        this.random = null;
    }
}

public class RandomPointer {

    public static void main(String s[]) {

    }

    public Node copyRandomList(Node head) {

        // adding new Node as next element of the node

        Node node = head;
        while (node != null) {
            Node newNode = new Node(node.val);
            newNode.next = node.next;
            newNode.random = node.random;
            node.next = newNode;
            node = newNode.next;
        }

        // Changing the random
        node = head.next;
        while (node != null) {
            Node rand = node.random;
            if (rand != null)
                node.random = rand.next;
            else
                node.random = null;
            if (node.next != null)
                node = node.next.next;
            else
                node = null;
        }


        Node head2 = head.next;
        Node node1 = head;
        node = head2.next;

        while (node != null && node1 != null) {

            if (node1.next != null)
                node1.next = node1.next.next;
            else {
                node1.next = null;
            }
            if (node.next != null)
                node.next = node.next.next;
            else {
                node.next = null;
            }

            node1 = node1.next;
            node = node.next;
        }
        return head2;
    }

}
