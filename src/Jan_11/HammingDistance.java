package Jan_11;

public class HammingDistance {

    public void findHammingDistanceBetweenTeoIntegers(int i , int j) {
        int distance = 0;
        int xor = i ^ j;

        while(xor != 0) {
            distance++;
            xor = xor & (xor-1);
        }

        System.out.print(distance);
    }

    public static void main(String s[]) {
        HammingDistance hammingDistance = new HammingDistance();
        hammingDistance.findHammingDistanceBetweenTeoIntegers(1, 4);
    }
}
