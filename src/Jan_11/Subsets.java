package Jan_11;

import java.util.*;

public class Subsets {

    static List<List<Integer>> set = new ArrayList<>();
    static int k = 0;

    public void generateSubSets(List<Integer> list , int index, List<Integer> generated) {

        if(generated.size() == k) {
            set.add(new ArrayList<>(generated));
            return;
        }
        for( int i = index; i < list.size(); i++) {
            generated.add(list.get(i));
            generateSubSets(list, i +1, generated);
            generated.remove(generated.size()-1);
        }
    }

    public static void main(String s[]) {

        Subsets subsets = new Subsets();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        for( k =0 ; k < list.size() + 1; k++)
            subsets.generateSubSets(list, 0, new ArrayList<>());

        for(List<Integer> li: set) {
            System.out.println(li);
        }
    }
}
