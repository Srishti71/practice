package Jan_11;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Pairs {
    int a, b;

    Pairs(int a, int b) {
        this.a = a;
        this.b = b;
    }
}

public class PalindromePairs {

    public Set<int[]> findPalindromePairs(String[] words) {

        Set<int[]> listOfPairs = new HashSet<>();
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j < words.length; j++) {

                if (i == j)
                    continue;
                if (isPalindrome(words[i] + words[j])) {
                    int array1[] = {i, j};
                    listOfPairs.add(array1);
                }
            }
        }

        printElement(listOfPairs);

        return listOfPairs;
    }

    private void printElement(Set<int[]> listOfPairs) {

        for (int[] array : listOfPairs) {
            System.out.println(array[0] + " " + array[1]);
        }
    }

    private boolean isPalindrome(String s) {
        int length = s.length();
        for (int i = 0; i < length / 2; i++) {

            if (s.charAt(i) != s.charAt(length - 1 - i)) return false;
        }
        return true;
    }


    public static void main(String s[]) {
        PalindromePairs palindromePairs = new PalindromePairs();
        String words[] = {"bat", "tab", "cat"};
        palindromePairs.findPalindromePairs(words);
    }

}
