package Feb_14;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/*

https://leetcode.com/problems/meeting-scheduler/

 */
public class MeetingScheduler {

    public static void main(String s[]) {
        MeetingScheduler meetingScheduler = new MeetingScheduler();
        int[][] slots1 = {{10, 50}, {60, 120}, {140, 210}};
        int[][] slots2 = {{0, 15}, {60, 70}, {140, 210}};
        System.out.println(meetingScheduler.minAvailableDuration(slots1, slots2, 12));

    }

    public List<Integer> minAvailableDuration(int[][] slots1, int[][] slots2, int duration) {

        int i = 0, j = 0;

        while (i < slots1.length && j < slots2.length) {

            if (slots1[i][0] < slots2[j][0]) {
                int ele = slots2[j][0] + duration;
                if (ele <= slots1[i][1]) {
                    if (ele <= slots2[j][1]) {
                        List newList = new ArrayList<Integer>();
                        newList.add(slots2[j][0]);
                        newList.add(slots2[j][0] + duration);
                        return newList;
                    } else {
                        j++;
                    }
                } else{
                    i++;
                }
            } else if (slots1[i][0] > slots2[j][0]) {
                int ele = slots1[i][0] + duration;
                if (ele <= slots2[j][1] ) {
                    if(ele <= slots1[i][1]) {
                        List newList = new ArrayList<Integer>();
                        newList.add(slots1[i][0]);
                        newList.add(slots1[i][0] + duration);
                        return newList;

                    } else{
                        i++;
                    }
                } else {
                    j++;
                }
            } else {
                if (slots1[i][0] + duration <= slots1[i][1]) {
                    if (slots1[i][0] + duration <= slots2[j][1]) {
                        List newList = new ArrayList<Integer>();
                        newList.add(slots1[i][0]);
                        newList.add(slots1[i][0] + duration);
                        return newList;
                    } else {
                        j++;
                    }
                } else {
                    i++;
                }
            }
        }

        return new ArrayList();
    }

}
