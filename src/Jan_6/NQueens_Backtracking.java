package Jan_6;

import java.util.Arrays;

public class NQueens_Backtracking {
    static int n = 4;

    public boolean findPlaceForNQueuens(int[][] board, int row, int[] columnPlacement) {

        if (row == n) {
            printSetUp(columnPlacement);
            return true;
        } else {
            for (int i = 0; i < n; i++) {
                if (isSafe(board, row, i, columnPlacement)) {
                    columnPlacement[i] = row;
                    board[row][i] = 1;
                    if (findPlaceForNQueuens(board, row + 1, columnPlacement)) {
                        return true;
                    }
                    board[row][i] = 0;
                    columnPlacement[i] = -1;
                }
            }
        }
        return false;
    }

    private boolean isSafe(int[][] board, int row, int i, int[] columnPlacement) {
        //check in the same row
        for (int col = 0; col < n; col++) {
            if (board[row][col] == 1)
                return false;
        }

        //check in the same col
        for (int r = 0; r < n; r++) {
            if (board[r][i] == 1)
                return false;
        }

        //check upper diagonal left
        int r, c;
        for (r = row, c = i; r >= 0 && c >= 0; r--, c--) {
            if (board[r][c] == 1)
                return false;
        }

        // check upper right
        for (r = row, c = i; r >= 0 && c < n; r--, c++) {
            if (board[r][c] == 1)
                return false;
        }
        return true;
    }

    private void printSetUp(int[] columnPlacement) {
        for (int i : columnPlacement) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void main(String s[]) {
        NQueens_Backtracking nqueens = new NQueens_Backtracking();
        int[][] board = new int[n][n];
        int[] columnPlacement = new int[n];
        Arrays.fill(columnPlacement, -1);
        nqueens.findPlaceForNQueuens(board, 0, columnPlacement);
    }
}
