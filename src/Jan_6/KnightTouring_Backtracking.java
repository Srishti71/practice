package Jan_6;

public class KnightTouring_Backtracking {

    static int n = 8;
    static int[] move_x = {-1, -2, -2, -1, 2, 2, 1, 1};
    static int[] move_y = {2, 1, -1, -2, 1, -1, -2, 2};

    public boolean findValidRoute(int[][] board, int row, int col, int count) {

        if (count >= n * n) {
            printBoardElement(board);
            return true;
        }

        for (int i = 0; i < n; i++) {

            int new_x = move_x[i] + row;
            int new_y = move_y[i] + col;

            if (isSafe(board, new_x, new_y)) {
                board[new_x][new_y] = count;
                if (findValidRoute(board, new_x, new_y, count + 1)) {
                    return true;
                }
                board[new_x][new_y] = -1;

            }

        }
        return false;

    }

    private boolean isSafe(int[][] board, int new_x, int new_y) {

        return new_x >= 0 && new_y >= 0 && new_x < 8 && new_y < 8 && board[new_x][new_y] == -1;
    }

    private boolean boardIsFull(int[][] board) {

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == -1)
                    return false;
            }
        }
        return true;
    }

    private void printBoardElement(int[][] board) {

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void main(String s[]) {

        KnightTouring_Backtracking knightTouring = new KnightTouring_Backtracking();
        int[][] board = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = -1;
            }
        }

        board[0][0] = 0;
        knightTouring.findValidRoute(board, 0, 0, 1);

    }
}
