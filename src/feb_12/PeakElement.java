package feb_12;

public class PeakElement {

    public int findPeakElement(int[] nums) {
        if (nums.length <= 1) {
            return 0;
        }

        int length = nums.length;
        int prev = nums[0];
        int index = 0;
        for (int i = 1; i < nums.length - 1; i++) {
            if (nums[i] > prev && nums[i] > nums[i + 1])
                return i;
            else {
                prev = nums[i];
            }
        }

        if (nums[length - 1] > prev) {
            return length - 1;
        } else
            return index;
    }

    public static void main(String s[]) {
        PeakElement peakElement = new PeakElement();
        int[] nums = {1, 2, 1, 3, 5, 6, 4};

        peakElement.repeatedSubstringPattern("ababab");
        //System.out.println(peakElement.findPeakElement(nums));
    }


    public boolean repeatedSubstringPattern(String s) {
        if (s.isEmpty() || s.length() < 2) {
            return false;
        }

        int mid = s.length()/2;

        for( int i = mid; i>0; i--) {
            String subString = s.substring(0, i);
            String newString="";
            boolean flag = false;
            while(newString.length() < s.length()) {
                newString = newString + subString;
                flag = true;
            }
            if(newString.equals(s) && flag) {
                return true;
            }
        }
        return false;

    }

    private boolean checkAllElementIsSame(String s) {

        char ch = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) != ch)
                return false;
        }
        return true;
    }


}
