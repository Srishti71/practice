package Jan_29;

import java.util.*;

public class ReconstructQueue {

    public int[][] reconstructQueue(int[][] people) {
        Arrays.sort(people, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                // if the heights are equal, compare k-values
                return o1[0] == o2[0] ? o1[1] - o2[1] : o2[0] - o1[0];
            }
        });

        List<int[]> output = new LinkedList<>();
        for(int[] p : people){
            System.out.println(p[0] + " "+ p[1]);
            output.add(p[1], p);
        }
        int[][] newArray = output.toArray(new int[people.length][2]);
        System.out.println("--------------------------");
        for( int i =0; i< newArray.length; i++) {
            System.out.println(newArray[i][0] + " "+ newArray[i][1]);
        }
        return newArray;
    }

    public static void main(String s[]) {
        ReconstructQueue reconstructQueue = new ReconstructQueue();
        int[][] array = {{7, 0}, {4, 4}, {7, 1}, {5, 0}, {6, 1}, {5, 2}};
        reconstructQueue.reconstructQueue(array);
    }

}
