package April_9;

import java.util.*;

public class WordLadder2 {

    public static void main(String s[]) {
        WordLadder2 wordLadder = new WordLadder2();
        wordLadder.findLadders("hit", "cog", Arrays.asList("hot", "dot", "dog", "lot", "log", "cog"));
    }

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {

        if (!wordList.contains(endWord)) {
            return new ArrayList();
        }

        List<List<String>> result = new ArrayList();
        List<List<String>> finalResult = new ArrayList();
        Queue<List<String>> queue = new LinkedList<>();

        queue.add(Arrays.asList(beginWord));
        Set<String> visited = new HashSet<>();
        int length = wordList.size();

        Map<String, Set<String>> wordDiffSet = new HashMap();

        Set<String> cur1 = wordDiffSet.getOrDefault(beginWord, new HashSet<>());
        for (int j = 0; j < length; j++) {
            if (!cur1.contains(wordList.get(j))) {
                if (oneCharApart(beginWord, wordList.get(j)))
                    cur1.add(wordList.get(j));
            }
        }
        wordDiffSet.put(beginWord, cur1);


        for (int i = 0; i < length; i++) {
            Set<String> cur = wordDiffSet.getOrDefault(wordList.get(i), new HashSet<>());
            for (int j = 0; j < length; j++) {
                if (i == j)
                    continue;
                if (!cur.contains(wordList.get(j))) {
                    if (oneCharApart(wordList.get(i), wordList.get(j)))
                        cur.add(wordList.get(j));
                }
            }
            wordDiffSet.put(wordList.get(i), cur);
        }

        while (!queue.isEmpty()) {

            List<String> curr = queue.poll();
            int size = curr.size();
            String word = curr.get(size - 1);
            visited.add(word);

            if (word.equals(endWord)) {
                if (!result.contains(curr)) result.add(curr);
                continue;
            }

            Set<String> set = wordDiffSet.getOrDefault(word, new HashSet<>());
            System.out.println(set + " " + word);
            for (String ele : set) {
                if (!visited.contains(ele)) {
                    List<String> newList = new ArrayList(curr);
                    newList.add(ele);
                    queue.add(newList);
                }
            }
        }

        int min = Integer.MAX_VALUE;
        for (List<String> l : result) {
            min = Math.min(min, l.size());
        }

        for (List<String> l : result) {
            if (l.size() == min) {
                finalResult.add(l);
            }
        }
        System.out.println(result);
        return finalResult;
    }

    private boolean oneCharApart(String ele, String word) {

        if (ele.length() != word.length() || ele.equals(word))
            return false;

        int diff = 0;
        for (int i = 0; i < ele.length(); i++) {
            if (ele.charAt(i) != word.charAt(i))
                diff += 1;

            if (diff > 1)
                return false;
        }
        return true;
    }
}
