package April_9;

import java.util.*;

public class Maze {

    public static void main(String s[]) {
        Maze mazePrg = new Maze();
        int[][] maze = {{0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0},
                {1, 1, 0, 1, 1},
                {0, 0, 0, 0, 0}};

        int[] start = new int[]{0, 4};
        int destination[] = new int[]{3, 2};
        System.out.println(mazePrg.hasPath(maze, start, destination));
    }

    public boolean hasPath(int[][] maze, int[] start, int[] destination) {

        Queue<int[]> queue = new LinkedList<>();
        queue.add(start);

        int row = maze.length;
        int col = maze[0].length;

        boolean[][] visited = new boolean[row][col];
        int dirs[][] = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};
        visited[start[0]][start[1]] = true;
        while (!queue.isEmpty()) {
            int[] element = queue.remove();

            if (element[0] == destination[0] && element[1] == destination[1]) {
                System.out.println("Destination arrived");
                return true;

            }
            for (int[] dir : dirs) {
                int x = element[0] + dir[0];
                int y = element[1] + dir[1];
                while (x >= 0 && x < row && y >= 0 && y < col && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                }
                x -= dir[0];
                y -= dir[1];
                if (!visited[x][y]) {
                    queue.add(new int[]{x, y});
                    visited[element[0]][element[1]] = true;
                }
            }
        }
        return false;
    }

}
