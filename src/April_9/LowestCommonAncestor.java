package April_9;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class LowestCommonAncestor {

    public static void main(String s[]) {

        // [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
        TreeNode root = new TreeNode(3);
        TreeNode node1 = new TreeNode(5);
        TreeNode node2 = new TreeNode(1);
        TreeNode node3 = new TreeNode(6);
        TreeNode node4 = new TreeNode(2);
        TreeNode node5 = new TreeNode(0);
        TreeNode node6 = new TreeNode(8);
        TreeNode node7 = new TreeNode(7);
        TreeNode node8 = new TreeNode(4);

        root.left = node1;
        root.right = node2;
        node1.left = node3;
        node1.right = node4;

        node2.left = node5;
        node2.right = node6;

        node4.left = node7;
        node4.right = node8;

        LowestCommonAncestor lca = new LowestCommonAncestor();
        TreeNode n = lca.lowestCommonAncestor(root, node1, node8);
        if (n != null)
            System.out.println(n.val);
    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {

        if (root == null)
            return null;

        if (root == p || root == q) {
            if (root == p) {
                if (search(root.left, q) || search(root.right, q)) {
                    return root;
                }
                return null;
            } else {
                if (search(root.left, p) || search(root.right, p))
                    return root;
                return null;
            }
        }

        List<TreeNode> foundNodes = new ArrayList<>();
        traverse(root.left, p, q, foundNodes);
        if (foundNodes.isEmpty()) {
            return lowestCommonAncestor(root.right, p, q);
        } else if (foundNodes.contains(p) && foundNodes.contains(q)) {
            return lowestCommonAncestor(root.left, p, q);
        } else if (foundNodes.contains(p)) {
            if (search(root.right, q))
                return root;
            else
                return null;
        } else {
            if (search(root.right, p))
                return root;
            else
                return null;
        }
    }

    private void traverse(TreeNode root, TreeNode p, TreeNode q, List<TreeNode> list) {
        if (root != null) {
            if (root == p)
                list.add(p);
            if (root == q)
                list.add(q);
            traverse(root.left, p, q, list);
            traverse(root.right, p, q, list);
        }
    }

    private boolean search(TreeNode left, TreeNode p) {
        if (left == null)
            return false;

        if (left == p)
            return true;
        return search(left.left, p) || search(left.right, p);
    }
}
