package April_9;

import java.util.Arrays;
import java.util.List;

public class KnightDailer {

    public int knightDialer(int N) {
        int sum = 0;
        int[][] memo = new int[10][N];
        List<Integer>[] listOfConnection = buildConnection();

        for (int i = 0; i <= 9; i++) {
            sum += dfs(i, N - 1, listOfConnection, memo);  // I1: 0, 2, graph
        }
        return sum;
    }


    int dfs(int currentPosition, int hops, List<Integer>[] connection, int[][] memo) {

        if (hops == 0) {
            return 1;
        }

        if (memo[currentPosition][hops] != 0) {
            return memo[currentPosition][hops];
        }

        int localSum = 0;
        List<Integer> listOfPossbileNextPlace = connection[currentPosition];

        for (Integer connect : listOfPossbileNextPlace) { //
            localSum += dfs(connect, hops - 1, connection, memo);
        }

        memo[currentPosition][hops] = localSum;
        return localSum;
    }


    public List<Integer>[] buildConnection() {
        List<Integer>[] list = new List[10];

        list[0] = Arrays.asList(4, 6);
        list[1] = Arrays.asList(6, 8);

        list[2] = Arrays.asList(7, 9);
        list[3] = Arrays.asList(4, 8);

        list[4] = Arrays.asList(3, 9, 0);
        list[5] = Arrays.asList();

        list[6] = Arrays.asList(1, 7, 0);
        list[7] = Arrays.asList(2, 6);

        list[8] = Arrays.asList(3, 1);
        list[9] = Arrays.asList(2, 4);
        return list;

    }
}
