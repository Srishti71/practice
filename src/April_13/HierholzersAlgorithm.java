package April_13;

import java.util.*;

/*

void printCircuit(vector< vector<int> > adj)
{
    // adj represents the adjacency list of
    // the directed graph
    // edge_count represents the number of edges
    // emerging from a vertex
    unordered_map<int,int> edge_count;

    for (int i=0; i<adj.size(); i++)
    {
        //find the count of edges to keep track
        //of unused edges
        edge_count[i] = adj[i].size();
    }

    if (!adj.size())
        return; //empty graph

    // Maintain a stack to keep vertices
    stack<int> curr_path;

    // vector to store final circuit
    vector<int> circuit;

    // start from any vertex
    curr_path.push(0);
    int curr_v = 0; // Current vertex

    while (!curr_path.empty())
    {
        // If there's remaining edge
        if (edge_count[curr_v])
        {
            // Push the vertex
            curr_path.push(curr_v);

            // Find the next vertex using an edge
            int next_v = adj[curr_v].back();

            // and remove that edge
            edge_count[curr_v]--;
            adj[curr_v].pop_back();

            // Move to next vertex
            curr_v = next_v;
        }

        // back-track to find remaining circuit
        else
        {
            circuit.push_back(curr_v);

            // Back-tracking
            curr_v = curr_path.top();
            curr_path.pop();
        }
    }

    // we've got the circuit, now print it in reverse
    for (int i=circuit.size()-1; i>=0; i--)
    {
        cout << circuit[i];
        if (i)
           cout<<" -> ";
    }
}

 */
public class HierholzersAlgorithm {

    public static void main(String s[]) {


        List<List<Integer>> adj1 = new ArrayList(), adj2 = new ArrayList<>();

        // Input Graph 1


        // Build the edges
        adj1.add(0, Arrays.asList(1));
        adj1.add(1, Arrays.asList(2));
        adj1.add(2, Arrays.asList(0));


        // Input Graph 2

        adj2.add(0, Arrays.asList(1, 6));
        adj2.add(1, Arrays.asList(2));
        adj2.add(2, Arrays.asList(0, 3));
        adj2.add(3, Arrays.asList(4));
        adj2.add(4, Arrays.asList(2, 5));
        adj2.add(5, Arrays.asList(0));
        adj2.add(6, Arrays.asList(4));
        printCircuit(adj2);
    }

    static void printCircuit(List<List<Integer>> adj) {
        Map<Integer, Integer> edge_count = new HashMap();
        for (int i = 0; i < adj.size(); i++) {
            //find the count of edges to keep track
            //of unused edges
            edge_count.put(i, adj.get(i).size());
        }
        if (adj.isEmpty())
            return;

        Stack<Integer> curr_path = new Stack();
        List<Integer> circuit = new ArrayList<>();
        curr_path.push(0); // stack = [0]

        int curr_v = 0; // Current vertex

        while (!curr_path.empty()) {
            // If there's remaining edge
            if (edge_count.get(curr_v) != 0) {

                // Push the vertex
                curr_path.push(curr_v); // [0, 0]

                // Find the next vertex using an edge
                int next_v_size = adj.get(curr_v).size();
                int next_v = adj.get(curr_v).get(next_v_size - 1);


                // and remove that edge
                int currentCount = edge_count.get(curr_v);
                edge_count.put(curr_v, currentCount - 1);
                adj.get(curr_v).remove(next_v_size - 1);


                // Move to next vertex
                curr_v = next_v;
            }
            // back-track to find remaining circuit
            else
            {
                circuit.add(curr_v);

                // Back-tracking
                curr_v = curr_path.peek();
                curr_path.pop();
            }
        }

        System.out.println(circuit);

    }
}