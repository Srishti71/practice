package April_13;

import java.util.PriorityQueue;

public class MinimumCostOfConnectingSticks {

    public static void main(String s[]) {

    }

    public int connectSticks(int[] sticks) {

        PriorityQueue<Integer> queue = new PriorityQueue<>();

        for (int stick : sticks) {
            queue.add(stick);
        }
        int cost = 0;

        while (queue.size() >= 2) {
            int first = queue.poll();
            int second = queue.poll();

            int newStick = first + second;
            cost += newStick;
            queue.add(newStick);
        }

        return cost;
    }
}
