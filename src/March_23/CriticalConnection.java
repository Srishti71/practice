package March_23;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class CriticalConnection {

    Set<Integer> ap = new HashSet<>();
    Set<Integer> visited = new HashSet<>();

    int[] parent;
    int dt[];
    int low[];
    LinkedList[] adj;
    int time = 0;

    public Set<Integer> articulationPoint(int n, int[][] connection) {

        parent = new int[n];
        dt = new int[n];
        low = new int[n];
        adj = new LinkedList[n];

        for (int i = 0; i < n; i++) {
            parent[i] = -1;
            adj[i] = new LinkedList();
        }


        for (int i = 0; i < connection.length; i++) {
            adj[connection[i][0]].add(connection[i][1]);
            adj[connection[i][1]].add(connection[i][0]);
        }

        for (int i = 0; i < n; i++) {
            if (!visited.contains(i))
                dfs(i, visited, dt, low, parent, ap);

        }

        System.out.println(ap);

        return ap;

    }

    private void dfs(int i, Set<Integer> visited, int[] dt, int[] low, int[] parent, Set<Integer> ap) {

        int children = 0;
        visited.add(i);
        dt[i] = low[i] = ++time;

        Iterator<Integer> itr = adj[i].iterator();
        while (itr.hasNext()) {

            int v = itr.next();

            if(v == parent[i])
                continue;

            if(!visited.contains(v)) {
                children ++;
                parent[v] = i;

                dfs(v, visited, dt, low, parent, ap);

                low[i] = Math.min(low[i], low[v]);

                if(parent[i] == -1 && children >1) {
                    ap.add(i);
                }

                if(parent[i] != -1 && low[v] >= dt[i])
                    ap.add(i);
            }

            else {
                low[i] = Math.min(low[i], dt[v]);
            }

        }
    }

    public static void main(String s[]) {
        CriticalConnection cc= new CriticalConnection();
        int[][] connection = {{1, 0}, {0, 2}, {2, 1}, {0, 3}, {3, 4}};
        cc.articulationPoint(5, connection);
    }
}
