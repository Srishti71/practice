package March_23;

public class StringDivides {

    public String gcdOfStrings(String str1, String str2) {

        int len1 = str1.length();
        int len2 = str2.length();

        String s1 = len1 > len2 ? str2 : str1;

        //lets assume len 1 < len 2
        if (s1.equals(str1)) {
            for (int i = len1; i > 0; i--) {
                StringBuilder newString = getStringBuilder(len2, s1, i);
                if (newString.toString().equals(str2)) {
                    return s1.substring(0, i);
                }
            }
        } else if (s1.equals(str2)) {
            for (int i = len2; i > 0; i--) {
                StringBuilder newString = getStringBuilder(len1, s1, i);
                if (newString.toString().equals(str1)) {
                    return s1.substring(0, i);
                }
            }

        }
        return "";
    }

    private StringBuilder getStringBuilder(int len2, String s1, int i) {
        String subStr = s1.substring(0, i);
        StringBuilder newString = new StringBuilder();
        while (newString.length() < len2) {
            newString.append(subStr);
        }
        return newString;
    }

    public static void main(String s[]) {
        StringDivides sd = new StringDivides();
        System.out.println("Return: "+sd.gcdOfStrings("TAUXXTAUXXTAUXXTAUXXTAUXX",
                "TAUXXTAUXXTAUXXTAUXXTAUXXTAUXXTAUXXTAUXXTAUXX"));
    }
}
