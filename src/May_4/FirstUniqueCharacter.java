package May_4;

import java.util.*;

public class FirstUniqueCharacter {
    public int firstUniqChar(String s) { // "loveleetcode"
        Map<Character, Integer> map = new HashMap();
        Set<Character> duplicate = new HashSet();

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (duplicate.contains(ch)) {
                map.remove(ch);
            } else {
                map.put(ch, i);
                duplicate.add(ch);
            }

        }

        List<Character> list = new ArrayList(map.keySet());
        Collections.sort(list, (a, b) -> map.get(a) - map.get(b));

        if (!list.isEmpty())
            return map.get(list.get(0));

        return -1;
    }

    public static void main(String s[]) {
        FirstUniqueCharacter firstUniqueCharacter = new FirstUniqueCharacter();
        System.out.println(firstUniqueCharacter.firstUniqChar("leetcode"));
    }
}
