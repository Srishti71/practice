package April_10;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingChar {

    public static void main(String s[]) {
        LongestSubstringWithoutRepeatingChar lrc = new LongestSubstringWithoutRepeatingChar();
        lrc.lengthOfLongestSubstring("aa");
    }

    public int lengthOfLongestSubstring(String s) {
        int low = 0, high = 1;

        if (s == null)
            return 0;

        if (s.length() <= 1) {
            return s.length();
        }

        int maxLength = 0;
        while (low < high && high <= s.length()) {
            String sub = s.substring(low, high);
            System.out.println(sub);
            if (isUnique(sub)) {
                high++;
                maxLength = Math.max(maxLength, high - low - 1);
            } else {
                low++;
            }
        }
        System.out.println(maxLength);
        return maxLength;
    }

    private boolean isUnique(String substring) {

        Set<String> set = new HashSet<>();
        for (int i = 0; i < substring.length(); i++) {
            String s = substring.charAt(i) + "";
            if (set.contains(s)) {
                set.add(s);
                return false;
            } else {
                set.add(s);
            }
        }
        return set.size() == substring.length();
    }
}
