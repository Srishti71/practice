package March_14;

import common.Node;
/*
https://leetcode.com/problems/flatten-a-multilevel-doubly-linked-list/
 */
public class FlattenedDDL {

    public Node flatten(Node head) {
        if (head == null)
            return null;

        if (head.next == null && head.child == null) {
            return head;
        }

        while (head != null) {
            Node next = head.next;

            if (head.child != null) {
                head.next = head.child;
                head.child.prev = head;
                head.child = null;
                Node newNode = flatten(head.next);
                if (newNode != null) {
                    newNode.next = next;
                    if (next != null) next.prev = newNode;
                }
            }

            if (head.next == null) {
                break;
            }
            head = head.next;
        }

        return head;
    }

    public static void main(String s[]) {
        Node head = new Node(1);
        Node node1 = new Node(2);
        Node node2 = new Node(3);
        Node node3 = new Node(4);
        Node node4 = new Node(5);
        Node node5 = new Node(6);
        Node node6 = new Node(7);

        Node node7 = new Node(8);

        head.next = node1;
        node1.prev = head;
        node1.child = node2;

        node2.next = node3;
        node3.prev = node2;
        node3.child = node4;

        node4.child = node5;
        node5.prev = node4;

        node5.next = node6;
        node6.prev = node5;

        node2.next = node3;
        node3.prev = node2;

        node3.child = node7;

        Node main = head;
        FlattenedDDL flattenedDDL = new FlattenedDDL();
        flattenedDDL.flatten(head);
        printElements(main);
    }

    private static void printElements(Node node) {

        Node head = node;

        while (head != null) {
            System.out.println(head.val);
            head = head.next;
        }
    }
}