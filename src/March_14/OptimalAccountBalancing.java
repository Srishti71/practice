package March_14;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptimalAccountBalancing {

    public int minTransfers(int[][] transactions) {
        Map<Integer, Integer> maps = new HashMap<>();

        for (int[] element : transactions) {
            maps.put(element[0], maps.getOrDefault(element[0], 0) + element[2]);
            maps.put(element[1], maps.getOrDefault(element[1], 0) - element[2]);
        }

        List<Integer> list = new ArrayList<>();
        for (int value : maps.values()) {
            if (value != 0)
                list.add(value);
        }
        return optimizeBalanaceSplit(list, 0);
    }

    private int optimizeBalanaceSplit(List<Integer> list, int i) {

        System.out.println(list);
        if (i == list.size())
            return 0;

        int curr = list.get(i);
        if (curr == 0)
            return optimizeBalanaceSplit(list, i + 1);
        int min = Integer.MAX_VALUE;
        for (int j = i + 1; j < list.size(); j++) {
            int next = list.get(j);
            if (curr * next < 0) {
                list.set(j, curr + next);
                min = Math.min(min, 1 + optimizeBalanaceSplit(list, i + 1));
                list.set(j, next);
                if (curr + next == 0) {
                    break;
                }
            }
        }
        return min;
    }

    public static void main(String s[]) {
        OptimalAccountBalancing optimalAccountBalancing = new OptimalAccountBalancing();
        int[][] transactions = {{0, 1, 10}, {2, 0, 5}};
        System.out.println(optimalAccountBalancing.minTransfers(transactions));
    }
}
