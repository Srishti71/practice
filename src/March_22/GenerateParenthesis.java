package March_22;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GenerateParenthesis {

    public List<String> generateParenthesis(int n) {

        List<Set<String>> list = new ArrayList();

        Set<String> set = new HashSet<>();
        set.add("");
        list.add(set);

        for (int i = 1; i <= n; i++) {
            Set<String> prev = list.get(i - 1);
            Set<String> newSet = new HashSet<>();

            for (String s : prev) {
                if (s.isEmpty()) {
                    newSet.add("()");
                } else {
                    for (int j = 0; j < s.length(); j++) {
                        String newString = s.substring(0, j) + "()" + s.substring(j);
                        newSet.add(newString);
                    }
                }

            }

            list.add(newSet);
        }

        System.out.println(new ArrayList(list.get(n)));
        return new ArrayList(list.get(n));

    }

    public static void main(String s[]) {
        GenerateParenthesis gen = new GenerateParenthesis();
        gen.generateParenthesis(3);
    }
}
