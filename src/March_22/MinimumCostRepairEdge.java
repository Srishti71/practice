package March_22;

import Jan_7.DistanceBetweenBikesTest;
import March_11.MinimumInsertionPalindrome;

import java.util.*;

public class MinimumCostRepairEdge {

    Map<Integer, List<DistanceWeight>> map = new HashMap<>();

    public void minCostEdge(int n, int[][] edge, int[][] connections) {

        buildGraph(n, edge, connections);

        PriorityQueue<DistanceWeight> pq = new PriorityQueue<>((a, b) -> a.cost - b.cost);

        int minDistanceReq = 0;
        DistanceWeight d = new DistanceWeight(1, 0);
        pq.add(d);
        Set<Integer> seen = new HashSet<>();
        while (!pq.isEmpty()) {
            DistanceWeight dw = pq.poll();
            if (seen.size() == n) {
                System.out.println(minDistanceReq);
                return;
            } else {
                if (!seen.contains(dw.to)) {
                    minDistanceReq += dw.cost;
                    List<DistanceWeight> distanceWeights = map.get(dw.to);
                    pq.addAll(distanceWeights);
                    seen.add(dw.to);
                }
            }
        }

        System.out.println(minDistanceReq);
    }

    private void buildGraph(int N, int[][] edge, int[][] connections) {

        for (int i = 1; i <= N; i++) {
            map.put(i, new ArrayList<DistanceWeight>());
        }
        for (int[] e : edge) {
            List<DistanceWeight> list = map.get(e[0]);
            List<DistanceWeight> list1 = map.get(e[1]);

            list.add(new DistanceWeight(e[1], 0));

            list1.add(new DistanceWeight(e[0], 0));
            map.put(e[0], list);
            map.put(e[1], list1);

        }


        for (int[] c : connections) {
            List<DistanceWeight> list = map.getOrDefault(c[0], new ArrayList<DistanceWeight>());
            List<DistanceWeight> list1 = map.getOrDefault(c[1], new ArrayList<DistanceWeight>());

            list.add(new DistanceWeight(c[1], c[2]));

            list1.add(new DistanceWeight(c[0], c[2]));
            map.put(c[0], list);
            map.put(c[1], list1);
        }
    }

    class DistanceWeight {
        int to;
        int cost;

        DistanceWeight(int to, int cost) {
            this.to = to;
            this.cost = cost;
        }
    }

    public static void main(String s[]) {
        MinimumCostRepairEdge min = new MinimumCostRepairEdge();
        int[][] edge = {{1, 4}, {4, 5}, {2, 3}};
        int[][] connections = {{1, 2, 5}, {1, 3, 10}, {1, 6, 2}, {5, 6, 5}};
        min.minCostEdge(6, edge, connections);
    }
}
