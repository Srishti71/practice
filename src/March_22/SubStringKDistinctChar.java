package March_22;

import March_20.SubStrWithDistinctChar;

import java.util.HashSet;
import java.util.Set;

public class SubStringKDistinctChar {

    public Set<String> distnctChar(String s, int k) {
        Set<String> result = new HashSet<String>();
        int[] characters = new int[26];

        int low = 0, high = 0;

        while (low <= high && high < s.length()) {
            characters[s.charAt(high) - 'a']++;

            while (characters[s.charAt(high) - 'a'] != 1) {
                characters[s.charAt(low) - 'a']--;
                low++;
            }
            if (high - low + 1 == k) {
                result.add(s.substring(low, high + 1));
                characters[s.charAt(low) - 'a']--;
                low++;
            }
            high++;
        }
        System.out.println(result);
        return result;
    }

    public static void main(String s[]) {
        SubStringKDistinctChar sub = new SubStringKDistinctChar();
        sub.distnctChar("awaglknagawunagwkwagl", 4);
    }
}
