package March_22;

import javafx.util.Pair;

import java.util.PriorityQueue;

public class KClosestPointToOrigin {

    public int[][] kClosest(int[][] points, int K) {
        PriorityQueue<Pair<int[], Double>> pq = new PriorityQueue<Pair<int[], Double>>((a, b) -> {

            Double v1 = (Double) a.getValue();
            Double v2 = (Double) b.getValue();
            if (v1 < v2) {
                return -1;
            }
            else if (v1 > v2) {
                return 1;
            } else{
                return 0;
            }
        });


        for (int[] point : points) {
            double distance = Math.sqrt((point[0] * point[0]) + (point[1] * point[1]));
            pq.add(new Pair(point, distance));
        }
        int[][] result = new int[K][2];
        for (int i = 0; i < K; i++) {
            Pair<int[], Double> pair = pq.poll();
            int[] element = (int[]) pair.getKey();
            result[i][0] = element[0];
            result[i][1] = element[1];
        }

        return result;
    }

    public static void main(String s[]) {
        KClosestPointToOrigin kClose = new KClosestPointToOrigin();
        int[][] points = {{1, 3}, {-2, 2}};
        kClose.kClosest(points, 1);
    }

}
