package March_22;

import java.util.ArrayList;
import java.util.List;

public class Solution {

    public static void main(String s[]) {
        Solution sol = new Solution();
        int[] states = {1, 2, 3, 4};
        sol.cellCompete(states, 4);
    }

    // METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
    public List<Integer> cellCompete(int[] states, int days) {
        // WRITE YOUR CODE HERE

        List<Integer> result = new ArrayList();


        for (int j = 0; j < days; j++) {

            for (int i = 0; i < states.length; i++) {

                if (i == 0) {
                    if (states[i + 1] == 1)
                        result.add(1);
                    else
                        result.add(0);
                } else if (i == days - 1) {
                    if (states[i - 1] == 0) {
                        result.add(0);
                    } else {
                        result.add(1);
                    }
                } else {
                    if (states[i - 1] != states[i + 1]) {
                        result.add(1);
                    } else {
                        result.add(0);
                    }

                }
            }

            for (int e = 0; e < result.size(); e++) {
                states[e] = result.get(e);
            }
        }

        return result;

    }


}
