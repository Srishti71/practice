package Jan_23;

import java.util.ArrayDeque;
import java.util.Deque;


class Pair {
    int index;
    int value;

    Pair(int index, int value) {
        this.index = index;
        this.value = value;
    }
}

public class RemoveKDigits {

    public String  removeKdigits(String num, int k) {
        Deque<Character> stack = new ArrayDeque<>();
        if (k == num.length())
            return "0";
        int idx = 0;

        while( idx < num.length()) {
            char curr = num.charAt(idx);

            if(stack.isEmpty() || stack.peek() <= curr || k == 0 ) {
                stack.push(curr);
                idx++;
            }
            else {
                stack.pop();
                k--;
            }
        }

        System.out.println(stack +" "+ stack.peekLast());

        StringBuilder sb = new StringBuilder();

        while (!stack.isEmpty() && stack.peekLast() == '0') // remove head '0's
            stack.pollLast();

        while (stack.size() > k) {// if takes lease than k step to make a non-descending stack, cut remain digits from end, since they are larger in the stack.
            sb.append(stack.pollLast());
        }

        String ans = sb.toString();
        System.out.println(ans);
        return ans.length() == 0 ? "0" : ans;
    }

    public static void main(String s[]) {

        RemoveKDigits removeKDigits = new RemoveKDigits();
        removeKDigits.removeKdigits("10243", 3);
    }
}
