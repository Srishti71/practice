package March_15;

public class LicenseKeyFormatting {


    public String licenseKeyFormatting(String S, int K) {
        if(S== null) {
            return null;
        }
        int len = S.length() - 1;
        String newString = "";
        while (len >= 0) {
            int i = 0;
            while (i < K && len >= 0) {
                if (S.charAt(len) != '-') {
                    newString = S.charAt(len) + newString;
                    i++;
                }
                len--;
            }
            if (len >= 0)
                newString = "-" + newString;
        }
        return newString.toUpperCase();
    }

    public static void main(String s[]) {
        LicenseKeyFormatting key = new LicenseKeyFormatting();
        System.out.println(key.licenseKeyFormatting("2-5g-3-J", 2));
    }
}
