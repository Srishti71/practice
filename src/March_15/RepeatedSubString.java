package March_15;

public class RepeatedSubString {

    public boolean repeatedSubstringPattern(String s) {

        if (s == null)
            return false;

        int len = s.length();

        if (len == 1) {
            return false;
        }
        int mid = len / 2;

        for (int i = mid; i > 0; i--) {

            if (len % i != 0) continue;

            String subStr = s.substring(0, i);
            String newSubStr = subStr;

            while (newSubStr.length() < len) {
                newSubStr += subStr;
            }

            if (newSubStr.equals(s))
                return true;
        }
        return false;
    }

    public static void main(String s[]) {
        RepeatedSubString repeatedSubString = new RepeatedSubString();
        System.out.println(repeatedSubString.repeatedSubstringPattern("abab"));
    }
}
