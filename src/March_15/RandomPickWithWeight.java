package March_15;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
https://leetcode.com/problems/random-pick-with-weight/
 */
public class RandomPickWithWeight {

    List<Integer> list = new ArrayList<>();
    Random rand = new Random();

    public RandomPickWithWeight(int[] w) {
        int total = 0;

        for (int element : w) {
            total += element;
            list.add(total);
        }
    }

    public int pickIndex() {
        int target = rand.nextInt();
        int lo = 0;
        int high = list.get(list.size() - 1);

        while (lo != high) {
            int mid = (lo + high) / 2;
            if (target >= list.get(mid)) lo = mid + 1;
            else high = mid;
        }
        return lo;
    }
}
