package March_15;

import java.util.HashSet;
import java.util.Set;

public class StudentAttendance {

    Set<String> set = new HashSet<>();

    public int checkRecord(int n) {
        String s = "ALP";
        dfs(s, "", n, 0, 0);
        return set.size();
    }

    private void dfs(String main, String curr, int n, int countA, int countL) {

        if (curr.length() == n) {
            set.add(curr);
            System.out.println(curr);
            return;
        }

        for (int i = 0; i < main.length(); i++) {
            if(main.charAt(i)=='A') {
                if(countA < 1) {
                    String newStr = curr;
                    dfs(main, curr + main.charAt(i), n, 1, 0);
                    curr = newStr;
                }

            } else if(main.charAt(i)=='L') {
                if(countL < 2) {
                    String newStr = curr;
                    dfs(main, curr + main.charAt(i), n, countA, countL + 1);
                    curr = newStr;
                }
            } else {
                String newStr = curr;
                dfs(main, curr + main.charAt(i), n, countA, 0);
                curr = newStr;
            }
        }
    }
    public static void main(String s[]) {
        StudentAttendance studentAttendance = new StudentAttendance();
        System.out.println(studentAttendance.checkRecord(3));
    }
}