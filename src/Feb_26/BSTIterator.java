package Feb_26;

import common.TreeNode;

import java.util.Stack;

public class BSTIterator {

    Stack<TreeNode> stack = new Stack<>();

    public static void main(String s[]) {
        BSTIterator bstIterator = new BSTIterator();
        TreeNode root = new TreeNode(7);
        TreeNode ch1 = new TreeNode(3);
        TreeNode ch2 = new TreeNode(15);
        TreeNode ch3 = new TreeNode(9);
        TreeNode ch4 = new TreeNode(20);

        root.left = ch1;
        root.right = ch2;

        ch2.left = ch3;
        ch2.right = ch4;
        bstIterator.addElementToStack(root);

        bstIterator.next();
        bstIterator.next();
        bstIterator.hasNext();
        bstIterator.next();
        bstIterator.hasNext();
        bstIterator.next();
        bstIterator.hasNext();
        bstIterator.next();
        bstIterator.hasNext();
    }

    public void addElementToStack(TreeNode node) {

        if (node != null) {
            stack.push(node);
            addElementToStack(node.left);
        }
    }

    public int next() {

        TreeNode node = stack.pop();

        if (node.right != null) {
            addElementToStack(node.right);
        }
        System.out.println(node.val);
        return node.val;
    }

    public boolean hasNext() {
        System.out.println(!(stack.isEmpty()));
        return !(stack.isEmpty());
    }
}
