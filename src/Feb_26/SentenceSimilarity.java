package Feb_26;

import java.util.*;

/*
Given two sentences words1, words2 (each represented as an array of strings), and a list of similar word pairs pairs, determine if two sentences are similar.

For example, words1 = ["great", "acting", "skills"] and words2 = ["fine", "drama", "talent"] are similar, if the similar word pairs are pairs = [["great", "good"], ["fine", "good"], ["acting","drama"], ["skills","talent"]].

Note that the similarity relation is transitive. For example, if "great" and "good" are similar, and "fine" and "good" are similar, then "great" and "fine" are similar.

Similarity is also symmetric. For example, "great" and "fine" being similar is the same as "fine" and "great" being similar.

Also, a word is always similar with itself. For example, the sentences words1 = ["great"], words2 = ["great"], pairs = [] are similar, even though there are no specified similar word pairs.

Finally, sentences can only be similar if they have the same number of words. So a sentence like words1 = ["great"] can never be similar to words2 = ["doubleplus","good"].

Note:

The length of words1 and words2 will not exceed 1000.
The length of pairs will not exceed 2000.
The length of each pairs[i] will be 2.
The length of each words[i] and pairs[i][j] will be in the range [1, 20].
 */
public class SentenceSimilarity {

    /*
     words1 = ["great", "acting", "skills"] and words2 = ["fine", "drama", "talent"]
     pairs = [["great", "good"], ["fine", "good"], ["acting","drama"], ["skills","talent"]]
     */

    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, List<List<String>> pairs) {
            Stack<String> queue = new Stack<>();
            Set<String> similarWords = new HashSet<>();

            if (words1.length != words2.length)
                return false;

            lable:
            for (int i = 0; i < words1.length; i++) {
                if(similarWords.contains(words1)) {
                    continue;
                }
                queue.add(words1[i]);
                Set<String> visited = new HashSet<>();
                while (!queue.isEmpty()) {
                    String element = queue.pop();
                    visited.add(element);
                    if (element.equals(words2[i])) {
                        queue.clear();
                        similarWords.add(element);
                        similarWords.add(words2[i]);
                        continue lable;
                    } else {
                        for (List<String> list : pairs) {
                            if (list.get(0).equals(element) && !visited.contains(list.get(1))) {
                                queue.add(list.get(1));
                            } else if (list.get(1).equals(element) && !visited.contains(list.get(0)))
                                queue.add(list.get(0));
                        }
                    }
                }
                return false;
            }
            return true;
    }

    public static void main(String s[]) {
        SentenceSimilarity sentenceSimilarity = new SentenceSimilarity();
        String word1[] = {"great", "acting", "skills"};
        String word2[] = {"fine", "drama", "talent"};
        List<List<String>> listOfList = new ArrayList<>();
        List<String> list1 = new ArrayList();
        list1.add("great");
        list1.add("good");

        listOfList.add(list1);

        List<String> list2 = new ArrayList();
        list2.add("fine");
        list2.add("great");
        listOfList.add(list2);

        List<String> list3 = new ArrayList();
        list3.add("acting");
        list3.add("drama");
        listOfList.add(list3);

        List<String> list4 = new ArrayList();
        list4.add("skills");
        list4.add("talent");
        listOfList.add(list4);

        System.out.println(sentenceSimilarity.areSentencesSimilarTwo(word1, word2, listOfList));
    }
}
