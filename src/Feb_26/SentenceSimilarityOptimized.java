package Feb_26;

import java.lang.reflect.Array;
import java.util.*;

public class SentenceSimilarityOptimized {

    Map<String, List<String>> maps = new HashMap();

    public void processData(List<List<String>> pairs) {

        for (List<String> list : pairs) {
            List<String> l1 = maps.getOrDefault(list.get(0), new ArrayList<>());
            if (!l1.contains(list.get(1))) {
                l1.add(list.get(1));
            }
            maps.put(list.get(0), l1);

            List<String> l2 = maps.getOrDefault(list.get(1), new ArrayList<>());
            if (!l2.contains(list.get(0))) {
                l2.add(list.get(0));
            }
            maps.put(list.get(1), l2);
        }
    }

    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, List<List<String>> pairs) {
        processData(pairs);

        if (words1.length != words2.length) {
            return false;
        }

        lable:
        for (int i = 0; i < words1.length; i++) {
            String w1 = words1[i], w2 = words2[i];
            if (w1.equals(w2))
                continue;
            Set<String> seen = new HashSet<>();

            Stack<String> stack = new Stack<>();
            stack.push(w1);

            while (!stack.isEmpty()) {
                String element = stack.pop();
                seen.add(element);
                if (element.equals(w2)) {
                    continue lable;
                } else {

                    List<String> listOfString = maps.get(element);
                    if (listOfString != null) {
                        for (String e : listOfString) {
                            if (!seen.contains(e)) {
                                stack.push(e);
                            }

                        }
                    }
                }
            }
            return false;
        }

        return true;
    }

    public static void main(String s[]) {
        SentenceSimilarityOptimized sentenceSimilarity = new SentenceSimilarityOptimized();
        String word1[] = {"great", "acting", "skills"};
        String word2[] = {"fine", "drama", "talent"};
        List<List<String>> listOfList = new ArrayList<>();
        List<String> list1 = new ArrayList();
        list1.add("great");
        list1.add("good");

        listOfList.add(list1);

        List<String> list2 = new ArrayList();
        list2.add("fine");
        list2.add("great");
        listOfList.add(list2);

        List<String> list3 = new ArrayList();
        list3.add("acting");
        list3.add("drama");
        listOfList.add(list3);

        List<String> list4 = new ArrayList();
        list4.add("skills");
        list4.add("talent");
        listOfList.add(list4);

        System.out.println(sentenceSimilarity.areSentencesSimilarTwo(word1, word2, listOfList));
    }

    /*

["great","acting","skills"]
["fine","painting","talent"]
[["great","fine"],["drama","acting"],["skills","talent"]]

     */
}
