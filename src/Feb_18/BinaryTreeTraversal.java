package Feb_18;

import com.sun.source.tree.Tree;
import common.TreeNode;
import javafx.util.Pair;

import java.util.*;

public class BinaryTreeTraversal {

    List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> levelOrder(TreeNode root) {

        Stack<Pair<TreeNode, Integer>> queue = new Stack();

        queue.add(new Pair<>(root, 0));

        while (!queue.isEmpty()) {
            Pair<TreeNode, Integer> element = queue.pop();
            TreeNode node = element.getKey();
            int height = element.getValue();
            if (height == list.size()) {
                List<Integer> innerList = new ArrayList<>();
                innerList.add(node.val);
                list.add(innerList);
            } else if (height < list.size()) {
                List<Integer> innerList = list.get(height);
                innerList.add(node.val);
            }

            if(node.left != null)
                queue.add(new Pair(node.left, height + 1));

            if(node.right != null)
                queue.add(new Pair(node.right, height + 1));
        }
        printList(list);
        return list;
    }

    private void printList(List<List<Integer>> list) {

        for (List l : list) {
            System.out.println(l);
        }
    }

    public static void main(String s[]) {
        BinaryTreeTraversal traversal = new BinaryTreeTraversal();

        TreeNode root = new TreeNode(3);
        TreeNode node1 = new TreeNode(9);
        TreeNode node2 = new TreeNode(20);
        TreeNode node3 = new TreeNode(15);
        TreeNode node4 = new TreeNode(7);

        root.left = node1;
        root.right = node2;

        node2.left = node3;
        node2.right = node4;

        traversal.levelOrder(root);
    }
}
