package April_19;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MedianOfMedian {

    public static void main(String s[]) {
        List<Integer> num = Arrays.asList(7, 10, 4, 3, 20, 15);
        MedianOfMedian med = new MedianOfMedian();
        System.out.println(med.findMedian(num, 2));
    }

    //
    public int findMedian(List<Integer> nums, int k) {
        if (nums.isEmpty() || nums.size() < k) {
            return -1;
        }
        if (nums.size() < 5) {
            Collections.sort(nums);
            return nums.get(k);
        }
        int median = findMedianOfMedian(nums);
        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();

        for (int i = 0; i < nums.size(); i++) {
            int curr = nums.get(i);
            if (curr < median)
                left.add(curr);
            else
                right.add(curr);
        }

        if (k < left.size()) {
            return findMedian(left, k);
        } else {
            return findMedian(right, k - left.size());
        }
    }

    private int findMedianOfMedian(List<Integer> nums) {

        ArrayList<Integer> newArray = new ArrayList<>();
        ArrayList<Integer> median = new ArrayList<>();

        for (int i = 0; i < nums.size(); i++) {
            if (i != 0 && i % 5 == 0) {
                Collections.sort(newArray);
                median.add(newArray.get(newArray.size() / 2));
                newArray.clear();
            }
            newArray.add(nums.get(i));

        }
        Collections.sort(newArray);
        median.add(newArray.get(newArray.size() / 2));

        if (median.isEmpty()) {
            return -1;
        }

        Collections.sort(median);
        return median.get(median.size() / 2);
    }
}
