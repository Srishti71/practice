package April_19;

public class QuickSelect {
    public static void main(String s[]) {

        QuickSelect quick = new QuickSelect();
        int[] num = {7, 10, 4, 3, 20, 15};
        System.out.println(quick.findKthSmallestElement(num, 0, num.length - 1, 1));
    }

    public int findKthSmallestElement(int[] array, int low, int high, int k) {

        int partition = partition(array, low, high);

        if (partition == k) {
            return array[partition];
        }

        if (k < partition) {
            return findKthSmallestElement(array, low, partition - 1, k);
        } else {
            return findKthSmallestElement(array, partition + 1, high, k);
        }
    }

    private int partition(int[] array, int low, int high) {

        int pivot = array[high], pivotLow = low;
        for (int i = low; i <= high; i++) {
            if (array[i] < pivot) {
                int temp = array[pivotLow];
                array[pivotLow] = array[i];
                array[i] = temp;
                pivotLow++;
            }
        }

        int temp = array[pivotLow];
        array[pivotLow] = array[high];
        array[high] = temp;

        return pivotLow;
    }
}
