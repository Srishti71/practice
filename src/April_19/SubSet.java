package April_19;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SubSet {
    Set<List<Integer>> set = new HashSet<>();

    public static void main(String s[]) {
        SubSet subSet = new SubSet();
        subSet.set.clear();

        List<List<Integer>> list = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        //Arrays.sort(nums);
        //O(n*2^n)

        int[] nums = {1, 2, 3};
        subSet.backtrack(list, temp, nums, 0);
        System.out.println(list);
    }

    //0, 1, 2
    private void backtrack(List<List<Integer>> list,
                           List<Integer> tempList,
                           int[] nums, int start) {

        list.add(new ArrayList<>(tempList));
        for (int i = start; i < nums.length; i++) {

            tempList.add(nums[i]);

            backtrack(list, tempList, nums, i + 1);

            tempList.remove(tempList.size() - 1); //O(n)
        }
    }
}
