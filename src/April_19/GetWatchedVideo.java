package April_19;

import javafx.util.Pair;

import java.util.*;

public class GetWatchedVideo {

    public static void main(String s[]) {
        GetWatchedVideo watched = new GetWatchedVideo();

        List<List<String>> watchedVideos = Arrays.asList(Arrays.asList("bjwtssmu"), Arrays.asList("aygr", "mqls"), Arrays.asList("vrtxa", "zxqzeqy", "nbpl", "qnpl"),
                Arrays.asList("r", "otazhu", "rsf"), Arrays.asList("bvcca", "ayyihidz", "ljc", "fiq", "viu"));
        int[][] friends = {{3, 2, 1, 4}, {0, 4}, {4, 0}, {0, 4}, {2, 3, 1, 0}};
        int id = 3;
        int level = 1;

        watched.watchedVideosByFriends(watchedVideos, friends, id, level);

    }

    // watchedVideos = [["A","B"],["C"],["B","C"],["D"]], friends = [[1,2],[0,3],[0,3],[1,2]]
    public List<String> watchedVideosByFriends(List<List<String>> watchedVideos, int[][] friends, int id, int level) {

        Queue<Pair<Integer, Integer>> queue = new LinkedList<>();
        queue.add(new Pair<Integer, Integer>(id, 0));
        int number = friends.length;

        boolean seen[] = new boolean[number];
        seen[id] = true;
        Map<String, Integer> map = new HashMap();

        while (!queue.isEmpty()) {
            Pair item = queue.remove();
            int curr = (int) item.getKey();
            int currLevel = (int) item.getValue();

            if (currLevel > level)
                break;

            if (currLevel == level) {
                List<String> videos = watchedVideos.get(curr);
                for (String movie : videos) {
                    map.put(movie, map.getOrDefault(movie, 0) + 1);
                }
            } else {
                int[] friendsOf = friends[curr]; // 0
                for (int fr : friendsOf) {
                    if (!seen[fr]) {
                        queue.add(new Pair(fr, currLevel + 1));
                        seen[fr] = true;
                    }

                }

            }
        }
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(map.entrySet());

        Collections.sort(list, (a, b) -> a.getValue() - b.getValue() == 0 ? a.getKey().compareTo(b.getKey()) : a.getValue() - b.getValue());

        List<String> result = new ArrayList();
        for (Map.Entry<String, Integer> entry : list) {
            result.add(entry.getKey());
        }

        System.out.println(result);
        return result;
    }

}