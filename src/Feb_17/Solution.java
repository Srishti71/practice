package Feb_17;

import java.util.Arrays;
/*
https://leetcode.com/problems/rotated-digits/solution/

 */
public class Solution {


    public int rotatedDigits(int N) {
        int count = 0;

        for (int i = 1; i <= N; i++) {
            if (canRotate(i, false))
                count++;
        }
        return count;
    }

    public static void main(String s[]) {
        Solution solution = new Solution();
        System.out.println("Solution:" + solution.canRotate(101, false) + "false");
        System.out.println("Solution:" + solution.canRotate(102, false) + "true");

        System.out.println("Solution:" + solution.canRotate(32, false) + "true");

        System.out.println(solution.rotatedDigits(857));
        int[] arr = {1, 0, 0, 0};
        System.out.println(solution.maxDistToClosest(arr));
    }

    public boolean canRotate(int n, boolean flag) {
        if (n == 0) return flag;

        int d = n % 10;
        if (d == 3 || d == 4 || d == 7) return false;
        if (d == 0 || d == 1 || d == 8) return canRotate(n / 10, flag);
        return canRotate(n / 10, true);
    }


    public int maxDistToClosest(int[] seats) {

        int length = seats.length;
        int[] leftDist = new int[length];
        int[] rightDist = new int[length];

        Arrays.fill(leftDist, length);
        Arrays.fill(rightDist, length);

        for (int i = 0; i < length; i++) {
            if (seats[i] == 1) leftDist[i] = 0;
            else if (i > 0) leftDist[i] = leftDist[i - 1] + 1;
        }

        for (int i = length - 1; i >= 0; i--) {
            if (seats[i] == 1) rightDist[i] = 0;
            else if (i < length - 1) rightDist[i] = rightDist[i - 1] + 1;
        }

        int maxDist = Integer.MIN_VALUE;

        for (int i = 0; i < length; i++) {
            if (seats[i] == 0) {
                maxDist = Math.max(maxDist, Math.min(leftDist[i], rightDist[i]));
            }
        }
        return maxDist;
    }
}
