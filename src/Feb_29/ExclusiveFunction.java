package Feb_29;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

class Element {
    int id;
    int time;
    int action;

    Element(int id, int time, int action) {
        this.id = id;
        this.time = time;
        this.action = action;
    }
}

public class ExclusiveFunction {

    public int[] exclusiveTime(int n, List<String> logs) {
        if (n == 1) {
            int result[] = new int[1];
            String last[] = logs.get(logs.size() - 1).split(":");
            result[0] = Integer.parseInt(last[2]) + 1;
            System.out.println(result[0]);
            return result;
        }
        Stack<Element> stack = new Stack();

        int array[] = new int[n];

        for (int i = 0; i < logs.size(); i++) {
            String[] element = logs.get(i).split(":");
            if (element[1].equals("start")) {
                int newStart = Integer.parseInt(element[2]);
                if (!stack.isEmpty()) {
                    Element top = stack.peek();
                    top.action = top.action + (newStart - top.time);
                }
                stack.push(new Element(Integer.parseInt(element[0]), newStart, 1));
            } else if (element[1].equals("end")) {
                Element e = stack.pop();
                int time = Integer.parseInt(element[2]);
                if (time == e.time) {
                    array[e.id] = array[e.id] + e.action;
                } else {
                    array[e.id] = array[e.id] + e.action + (time - e.time);
                }

                if (!stack.isEmpty()) {
                    Element top = stack.peek();
                    top.time = time + 1;
                }
            }
        }
        printElement(array);
        return array;
    }

    private void printElement(int[] array) {
        for (int e : array) {
            System.out.println(e);
        }
    }

    public static void main(String s[]) {
        List<String> list = Arrays.asList(
                "0:start:0","1:start:1","1:end:10000000","0:end:100000000");

        int n = 2;
        ExclusiveFunction exclusive = new ExclusiveFunction();
        exclusive.exclusiveTime(n, list);
    }
}
