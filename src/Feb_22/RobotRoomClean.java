package Feb_22;


import java.util.HashSet;
import java.util.Set;

enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}

interface Robot {
    // returns true if next cell is open and robot moves into the cell.
    // returns false if next cell is obstacle and robot stays on the current cell.
    boolean move();

    // Robot will stay on the same cell after calling turnLeft/turnRight.
    // Each turn will be 90 degrees.
    void turnLeft();

    void turnRight();

    // Clean the current cell.
    void clean();
}

public class RobotRoomClean {

    Set<String> set = new HashSet<>();

    public void cleanRoom(Robot robot) {
        cleanRoomHelper(robot, "-1,0", "0,0", Direction.UP);
    }

    // Coordinates are in the form of "0,0"
    private void cleanRoomHelper(Robot robot, String nextCord, String prevCord, Direction direction) {

        if (set.contains(nextCord)) {
            return;
        }
        if (robot.move()) {
            robot.clean();
            String newNextCord = getNextCoordinates(nextCord, direction);
            cleanRoomHelper(robot, newNextCord, newNextCord, direction);
        }

        robot.turnLeft();
        Direction newDir = getNewDirection(direction, Direction.LEFT);
        String newNextCord = getNextCoordinates(prevCord, newDir);
        cleanRoomHelper(robot, newNextCord, prevCord, newDir);

        robot.turnRight();
        newDir = getNewDirection(direction, Direction.RIGHT);
        newNextCord = getNextCoordinates(prevCord, newDir);
        cleanRoomHelper(robot, newNextCord, prevCord, newDir);
    }

    private Direction getNewDirection(Direction direction, Direction moveDir) {

        if (moveDir == Direction.LEFT) {
            if (direction == Direction.UP) {
                return Direction.LEFT;
            } else if (direction == Direction.DOWN) {
                return Direction.RIGHT;
            } else if (direction == Direction.LEFT) {
                return Direction.DOWN;
            } else {
                return Direction.UP;
            }
        } else {
            if (direction == Direction.UP) {
                return Direction.RIGHT;
            } else if (direction == Direction.DOWN) {
                return Direction.LEFT;
            } else if (direction == Direction.LEFT) {
                return Direction.UP;
            } else {
                return Direction.DOWN;
            }
        }
    }

    private String getNextCoordinates(String nextCord, Direction direction) {
        String cords[] = nextCord.split(",");
        if (direction == Direction.UP) {
            return (Integer.parseInt(cords[0]) - 1) + "," + cords[1];
        } else if (direction == Direction.DOWN) {
            return (Integer.parseInt(cords[0]) + 1) + "," + cords[1];
        } else if (direction == Direction.LEFT) {
            return cords[0] + "," + (Integer.parseInt(cords[1]) - 1);
        } else {
            return cords[0] + "," + (Integer.parseInt(cords[1]) + 1);
        }
    }
}
