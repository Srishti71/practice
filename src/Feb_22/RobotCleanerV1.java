package Feb_22;

import java.util.HashSet;
import java.util.Set;

public class RobotCleanerV1 {
    public static void main(String args[]) {
        int[][] room = {
                {0, 1, 1, 1},
                {1, 1, 1, 1},
                {1, 1, 0, 0},
                {1, 1, 1, 1},
        };
        int[] pos = {1, 1};
        Robot robot = new RobotImpl(room, pos);
        new RobotCleanerV1().cleanRoom(robot);
    }

    interface Robot {
        boolean move();

        void turnLeft();

        void turnRight();

        void clean();
    }

    static int[][] directions = new int[][] { {-1,0}, {0,1}, {1,0}, {0, -1}};

    public void cleanRoom(Robot robot) {
        Set<String> visited = new HashSet<>();
        dfs(robot, 0, 0, 0, visited);
    }

    private void dfs(Robot robot, int x, int y, int arrow, Set<String> visited) {
        String path = x + "-" + y;
        if(visited.contains(path)) {
            return;
        }

        visited.add(path);
        robot.clean();

        for(int n = 0; n < 4; n++) {
            if(robot.move()) {
                // go all the way till cannot move, then back one step
                int nx = x + directions[arrow][0];
                int ny = y + directions[arrow][1];

                // recurse
                dfs(robot, nx, ny, arrow, visited);

                // backtrack

                // rotate 180 degrees to change direction of movement
                robot.turnLeft();;
                robot.turnLeft();

                // go back to the previous valid point
                robot.move();

                // rotate 180 degrees to have the "start" position for previous valid point
                robot.turnRight();;
                robot.turnRight();
            }

            robot.turnRight(); // or turnRight();
            arrow = (arrow + 1) % 4;
        }
    }

    static class RobotImpl implements Robot {

        int[] pos;
        int[][] room;
        int dir = 0; // up: 0, right: 1, down: 2, left: 3

        public RobotImpl(int[][] room, int[] pos) {
            this.room = room;
            this.pos = pos;
        }

        @Override
        public boolean move() {
            int x = 0, y = 0;
            switch (dir) {
                case 0:
                    x = -1;
                    break;
                case 1:
                    y = 1;
                    break;
                case 2:
                    x = 1;
                    break;
                case 3:
                    y = -1;
                    break;
            }
            int newRow = pos[0] + x;
            int newCol = pos[1] + y;
            boolean outOfbounds = (newRow < 0 || newCol < 0 || newRow > room.length - 1 || newCol > room[0].length - 1);
            if (outOfbounds || room[newRow][newCol] == 0) {
                return false;
            }
            pos = new int[]{newRow, newCol};
            System.out.println("Robot at: " + pos[0] + " , " + pos[1]);

            return true;
        }

        @Override
        public void turnLeft() {
            dir -= 1;
            if (dir < 0) dir = 3;
        }

        @Override
        public void turnRight() {
            dir += 1;
            if (dir > 3) dir = 0;
        }

        @Override
        public void clean() {
            System.out.println("Cleaning  " + pos[0] + " , " + pos[1]);
        }
    }
}
