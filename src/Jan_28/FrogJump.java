package Jan_28;

import java.util.HashSet;
import java.util.Stack;


/*
[0,1,3,5,6,8,12,17]

   Return true. The frog can jump to the last stone by jumping
1 unit to the 2nd stone, then 2 units to the 3rd stone, then
2 units to the 4th stone, then 3 units to the 6th stone,
4 units to the 7th stone, and 5 units to the 8th stone.

0 1 = 1
1 2= 3
3 2 = 5
5 3 = 8
8 4 = 12
12 5 = 17
 */

public class FrogJump {


    public boolean canCross(int[] stones) {

        if (stones == null) {
            return false;
        } else if (stones.length <= 1) {
            return true;
        }

        HashSet<Integer> stonesPositions = new HashSet();
        for (int i = 0; i < stones.length; i++) {
            stonesPositions.add(stones[i]);
        }

        Stack<Integer> position = new Stack<>();
        Stack<Integer> jumps = new Stack<>();
        int destination = stones[stones.length - 1];

        position.add(0);
        jumps.add(0);

        while (!position.isEmpty()) {
            int pos = position.pop();
            int jump = jumps.pop();

            if (pos == destination) {
                return true;
            }

            for (int i = jump - 1; i <= jump + 1; i++) {
                if (i <= 0)
                    continue;

                int next = pos + i;
                if (stonesPositions.contains(next)) {
                    position.add(next);
                    jumps.add(i);
                }
            }
        }
        return false;

    }

    public static void main(String s[]) {
        FrogJump frogJump = new FrogJump();
        int[] stones = {0, 1, 2, 3, 4, 8, 9, 11};
        System.out.println(frogJump.canCross(stones));
    }
    /*
    Take Away:

    1. Some recursive problems can be solved using stacks. Especially when there is one condition that you know should return the value.
    2. Sometimes figuring out the condition when the solution does not exist can reduce the complexity drastically.
    3. Use HashSet instead of going through the list for the entire array to check certain condition.

    4. Could use the Binary search as the list are sorted. considering the gaps.
    5. 
     */
}
