package Jan_28;

import java.util.HashSet;

/*
0,1,2,3,4,8,9,11
https://leetcode.com/problems/frog-jump/
 */
public class FrogJumpRecursive {

    public boolean canCross(int[] stones) {
        if (stones == null) {
            return false;
        } else if (stones.length <= 1) {
            return true;
        }

        HashSet<Integer> stonesPositions = new HashSet();
        for (int i = 0; i < stones.length; i++) {
            stonesPositions.add(stones[i]);
        }

        return canFrogJump(stonesPositions, 0, 0, stones[stones.length - 1]);
    }

    private boolean canFrogJump(HashSet<Integer> stonesPositions, int position, int jump, int destination) {

        if (position == destination) {
            return true;
        } else if (position > destination) {
            return false;
        }

        for (int i = jump - 1; i <= jump + 1; i++) {
            if (i <= 0)
                continue;
            int next = position + i;
            if (stonesPositions.contains(next)) {
                return canFrogJump(stonesPositions, next, i, destination);
            }
        }
        return false;
    }


    public static void main(String s[]) {
        FrogJump frogJump = new FrogJump();

        int[] stones = {0, 1, 3, 5, 6, 8, 12, 17};
        System.out.println(frogJump.canCross(stones));
    }

}
