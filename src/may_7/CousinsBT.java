package may_7;

import April_18.CourseScheduler;
import common.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

class Cousin {
    TreeNode node;
    int depth;
    TreeNode parent;

    Cousin(TreeNode node, int depth, TreeNode parent) {
        this.node = node;
        this.depth = depth;
        this.parent = parent;
    }
}

public class CousinsBT {

    public boolean isCousins(TreeNode root, int x, int y) {
        Queue<Cousin> queue = new LinkedList();
        queue.add(new Cousin(root, 0, null));
        int parent = -1;
        int depth = -1;
        int count = 0;
        while (!queue.isEmpty()) {
            Cousin c = queue.poll();
            int p = c.parent.val;
            int d = c.depth;
            if (c.node.val == x || c.node.val == y) {
                if (count == 1) {
                    if (parent != p && depth == d) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    count = 1;
                    parent = p;
                    depth = d;
                }
            }

            if (c.node.left != null)
                queue.add(new Cousin(c.node.left, d + 1, c.node));
            if (c.node.right != null)
                queue.add(new Cousin(c.node.right, d + 1, c.node));
        }

        return false;
    }
}
