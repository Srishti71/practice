package Jan_22;

public class RemoveKDigits {

    long minValue = Long.MAX_VALUE;

    public void removeDigits(String word, String currentWord, int k, int index) {

        if (k == 0) {
            if (!currentWord.isEmpty())
                minValue = Math.min(minValue, Long.parseLong(currentWord));
            return;
        } else if (index >= word.length())
            return;

        removeDigits(word, currentWord + word.charAt(index), k - 1, index + 1);
        removeDigits(word, currentWord, k, index + 1);

    }

    public String removeKdigits(String num, int k) {
        if(num.length() == k)
            return "0";
        removeDigits(num, "", num.length() - k, 0);
        return String.valueOf(minValue);
    }

    public static void main(String s[]) {
        RemoveKDigits removeKDigits = new RemoveKDigits();
        String word = "1432";
        int k = 1000;
        System.out.print(removeKDigits.removeKdigits(word, k));
    }
}
