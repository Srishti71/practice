package March_11;

import java.util.Arrays;

public class MinimumInsertionPalindrome {

    static int dp[][];

    public int minInsertions(String s) {
        int len = s.length();
        char[] array = s.toCharArray();
        // return len - findLargestPalindromicSubSeq(array, 0, len - 1);
        dp = new int[len][len];

        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                dp[i][j] = -1;
            }
        }

        findLargestPalindromicSubSeqDP(array, 0, len - 1);
        return len - dp[0][len - 1];
    }


    private int findLargestPalindromicSubSeqDP(char array[], int l, int r) {

        if (dp[l][r] != -1) {
            return dp[l][r];
        }

        if (l > r) {
            return 0;
        }

        if (l == r) {
            return 1;
        }

        if (array[l] == array[r]) {
            dp[l][r] = findLargestPalindromicSubSeqDP(array, l + 1, r - 1) + 2;
        } else {

            dp[l][r] = Math.max(findLargestPalindromicSubSeqDP(array, l + 1, r), findLargestPalindromicSubSeqDP(array, l, r - 1));
        }

        return dp[l][r];
    }

    public static void main(String s[]) {
        MinimumInsertionPalindrome minimumInsertionPalindrome = new MinimumInsertionPalindrome();
        System.out.println(minimumInsertionPalindrome.minInsertions("leetcode"));
    }


    private int findLargestPalindromicSubSeq(char array[], int l, int r) {

        if (l > r) {
            return 0;
        }

        if (l == r) {
            return 1;
        }

        if (array[l] == array[r]) {
            return findLargestPalindromicSubSeq(array, l + 1, r - 1) + 2;
        }


        return Math.max(findLargestPalindromicSubSeq(array, l + 1, r), findLargestPalindromicSubSeq(array, l, r - 1));
    }

}
