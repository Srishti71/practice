package March_10;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

// https://leetcode.com/problems/count-of-smaller-numbers-after-self/
public class CountSmallerNumber {

    public List<Integer> countSmaller(int[] nums) {
        int length = nums.length;
        if (length == 0)
            return new ArrayList<Integer>();

        int count[] = new int[length];
        count[length - 1] = 0;

        List<Integer> list = new ArrayList<>();
        for (int i = length - 1; i >= 0; i--) {
            int pos = findPosition(list, nums[i]);
            count[i] = pos;
        }

        List<Integer> results = new ArrayList<>();
        for (int e : count) {
            results.add(e);
        }

        System.out.println(results);
        return results;
    }

    public static void main(String s[]) {
        CountSmallerNumber countSmallerNumber = new CountSmallerNumber();
        int test[] = {10, 27, 10, 35, 12, 22, 28, 8, 19, 2, 12, 2, 9, 6, 12, 5, 17, 9, 19, 12, 14, 6, 12, 5, 12, 3, 0, 10, 0, 7, 8, 4, 0, 0, 4, 3, 2, 0, 1, 0};
        int res[] =  {10, 27, 10, 35, 12, 22, 28, 8, 19, 2, 12, 2, 9, 6, 12, 5, 17, 9, 19, 12, 14, 6, 12, 5, 12, 3, 0, 10, 0, 7, 8, 4, 0, 0, 4, 3, 2, 0, 1, 0};

        int[] nums = {26, 78, 27, 100, 33, 67, 90, 23, 66, 5, 38, 7, 35, 23, 52, 22, 83, 51, 98, 69, 81, 32, 78, 28, 94, 13, 2, 97, 3, 76, 99, 51, 9, 21, 84, 66, 65, 36, 100, 41};
        countSmallerNumber.countSmaller(nums);
    }


    private int findPosition(List<Integer> list, int num) {

        int left = 0;
        int right = list.size() - 1;

        // if(left == right == 0) compare new number with list.get(0)
        // else 0, 1, mid = 0 , compare new number with mid , if new number is < mid, go left, if left > right, add to left-- move elements
        // compare new with mid, if mid < num, left > right
        // else left < right compute mid and continue
        // [1,3,5] = 2
        // compared with 3
        // left = 0 , right = 0
        while (left <= right) {
            int mid = (left + right) / 2;
            int M = list.get(mid);
            if (M >= num) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    private static void insert(List<Integer> list, int num, Integer[] res, int i) {
        int lo = 0;
        int hi = list.size() - 1;

        while (lo <= hi) {
            int mid = (lo + hi) / 2;
            int M = list.get(mid);
            if (M >= num) {
                hi = mid - 1;
            } else {
                lo = mid + 1;
            }
        }
        list.add(lo, num);
        res[i] = lo;
    }
    /*
    Edges Case:
    Just on element
    empty?
    Same number


    Soln:

    Go right to left

      for every elemnt find numbers smaller than it.
      count[]
      count[length -1] = 0;
      for( int i= array.length -2; i>=0; i--){
      int count = 0;
        for(int j = i+1; j < length; j++){
        if(array[j] < array[i])
        count++;
       }


       count[i] = count;
      }


      2nd solution:

      [5,2,6,1]

      [(1,3), (2,1), (5, 0), (6, 2)]

      [0, 1, 2, 1]
     */
}
