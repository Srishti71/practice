package March_24;

import java.util.*;

public class Solution {

    int[][] adjacencyMatrix;
    int times = 0;

    public static void main(String s[]) {
        Solution sol = new Solution();
        List<List<Integer>> grid = new ArrayList();
        List<Integer> l1 = Arrays.asList(1, 2); //{{}
        List<Integer> l2 = Arrays.asList(1, 3);
        grid.add(l1);
        grid.add(l2);

        List<Integer> l3 = Arrays.asList(2, 4); //{{}
        List<Integer> l4 = Arrays.asList(3, 4);
        List<Integer> l5 = Arrays.asList(3, 6); //{{}
        List<Integer> l6 = Arrays.asList(6, 7); //{{}
        List<Integer> l7 = Arrays.asList(4, 5); //{{}


        grid.add(l3);
        grid.add(l4);
        grid.add(l5);
        grid.add(l6);
        grid.add(l7);
        sol.criticalRouters(7, 7, grid);

    }


    boolean[] seen;

    // METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
    List<Integer> criticalRouters(int numRouters, int numLinks,
                                  List<List<Integer>> links) {
        // WRITE YOUR CODE HERE

        //needs to use tarjens algorithm

        seen = new boolean[numRouters];
        List<Integer> necessaryRouters = new ArrayList();


        int[] low = new int[numRouters];
        int[] dist = new int[numRouters];
        int[] parent = new int[numRouters];

        adjacencyMatrix = new int[numRouters][numRouters];

        for (int i = 0; i < numRouters; i++) {
            for (int j = 0; j < numRouters; j++) {
                parent[i] = -1;
                adjacencyMatrix[i][j] = -1;
            }

        }

        // build graph
        buildNetwork(links);

        for (int i = 0; i < numRouters; i++) {
            if (!seen[i])
                findCriticalRouters(i, dist, low, parent, necessaryRouters);
        }

        System.out.println(necessaryRouters);
        return necessaryRouters;
    }

    public void findCriticalRouters(int i, int[] dist, int[] low, int[] parent, List<Integer> necessaryRouters) {

        System.out.println("Element: "+ i);
        int child = 0;
        seen[i] = true;
        low[i] = dist[i] = times++;

        int[] itr = adjacencyMatrix[i];

        for (int j = 0; j < itr.length; j++) {

            if (itr[j] == -1)
                continue;

            int v = j;

            if (!seen[v]) {
                child++;
                parent[v] = i;

                findCriticalRouters(v, dist, low, parent, necessaryRouters);

                low[i] = Math.min(low[v], low[i]);

                if (parent[i] == -1 && child > 1) {
                    necessaryRouters.add(i + 1);
                }

                if (parent[i] != -1 && low[v] >= dist[i]) {
                    necessaryRouters.add(i + 1);
                }
            }

            if (v != parent[i]) {
                low[i] = Math.min(dist[v], low[i]);
            }
        }
    }

    public void buildNetwork(List<List<Integer>> links) {

        for (List link : links) {

            adjacencyMatrix[(Integer) link.get(0) - 1][(Integer) link.get(1) - 1] = 1;
            System.out.println( adjacencyMatrix[(Integer) link.get(0) - 1][(Integer) link.get(1) - 1]);
            adjacencyMatrix[(Integer) link.get(1) - 1][(Integer) link.get(0) - 1] = 1;
        }

    }
}
