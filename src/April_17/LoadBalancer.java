package April_17;

public class LoadBalancer {

    public static void main(String s[]) {
        LoadBalancer loadBalancer = new LoadBalancer();
        System.out.println(loadBalancer.splitBalance(new int[]{1, 3, 4, 2, 2, 2, 1, 1, 2}));
    }

    public boolean splitBalance(int[] nums) {

        int low = 0, high = nums.length - 1;
        int sum = 0, sumL = nums[low], sumH = nums[high];

        for (int item : nums) {
            sum += item;
        }

        while (low < high) {
            if (sumL == sumH) {
                int remaining = sum - sumL * 2 - nums[low + 1] - nums[high - 1];
                if (remaining == sumH)
                    return true;
                low++;
                high--;
                sumL += nums[low];
                sumH += nums[high];

            } else if (sumH > sumL) {
                low++;
                sumL += nums[low];
            } else {
                high--;
                sumH += nums[high];
            }
        }

        return false;
    }
}
