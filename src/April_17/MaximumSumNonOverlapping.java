package April_17;

public class MaximumSumNonOverlapping {

    public static void main(String s[]) {
        MaximumSumNonOverlapping max = new MaximumSumNonOverlapping();
        max.maxSumTwoNoOverlap(new int[]{0, 6, 5, 2, 2, 5, 1, 9, 4}, 1, 2);
    }

    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        int len = A.length;
        int maxLeft[] = new int[len];
        int maxRight[] = new int[len];

        int sum = 0;
        for (int i = 0; i < M; i++) {
            maxLeft[i] = 0;
            sum += A[i];
        }

        maxLeft[M] = sum;
        for (int i = M + 1; i < len; i++) {
            sum = sum - A[i - M - 1] + A[i - 1];
            maxLeft[i] = sum;

        }

        for (int i = 1; i < len; i++) {
            maxLeft[i] = Math.max(maxLeft[i - 1], maxLeft[i]);
        }
        sum = 0;
        for (int i = len - 1; i >= len - M; i--) {
            maxRight[i] = 0;
            sum += A[i];
        }
        maxRight[len - M - 1] = sum;

        for (int i = len - 2 - M; i >= 0; i--) {
            sum = sum - A[i + M + 1] + A[i + 1];
            maxRight[i] = sum;

        }
        for (int i = len - 2; i >= 0; i--) {
            maxRight[i] = Math.max(maxRight[i + 1], maxRight[i]);
        }

        sum = 0;
        for (int i = 0; i < L; i++) {
            sum += A[i];
        }

        int result = 0;
        result = Math.max(result, sum + Math.max(maxLeft[0], maxRight[L - 1]));
        for (int i = L; i < len; i++) {

            sum = sum - A[i - L] + A[i];
            result = Math.max(result, sum + Math.max(maxLeft[i - L + 1], maxRight[i]));

        }


        return result;
    }

    private void print(int[] maxLeft) {
        for (int i : maxLeft) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
