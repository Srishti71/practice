package April_17;

import java.util.HashMap;
import java.util.Map;

public class DecodeString {

    Map<Integer, Integer> memo = new HashMap<>();

    public static void main(String s[]) {
        DecodeString decodeString = new DecodeString();
        System.out.println(decodeString.recursiveWithMemo(0, "1234"));
        System.out.println(decodeString.memo);
    }

    //3,  3
    private int recursiveWithMemo(int index, String str) {

        // If you reach the end of the string
        // Return 1 for success.
        if (index == str.length()) {
            return 1;
        }

        // If the string starts with a zero, it can't be decoded
        if (str.charAt(index) == '0') {
            return 0;
        }

        if (index == str.length()-1) {
            return 1;
        }

        // Memoization is needed since we might encounter the same sub-string.
        if (memo.containsKey(index)) {
            return memo.get(index);
        }

        int ans = recursiveWithMemo(index+1, str);
        if (Integer.parseInt(str.substring(index, index+2)) <= 26) {
            ans += recursiveWithMemo(index+2, str);
        }

        // Save for memoization
        memo.put(index, ans);

        return ans;

    }
}
