package April_17;

import java.util.*;

public class EmployeeImportance {

    Map<Integer, Employee> map = new HashMap();

    public int findImportance(List<Employee> employees, int id) {

        buildMap(employees);

        System.out.println(map);
        for (Employee employee : employees) {
            getImportance(employee);
        }
        for (Integer i : map.keySet()) {
            System.out.println(i + " ---> " + map.get(i).importance);
        }
        return map.get(id).importance;
    }

    private void buildMap(List<Employee> employees) {

        for (Employee e : employees) {
            map.put(e.id, e);
        }
    }

    private int getImportance(Employee employee) {

        int sum = employee.importance;
        for (Integer subOrdinate : employee.subordinates) {
            sum += getImportance(map.get(subOrdinate));
        }
        employee.importance = sum;
        return sum;
    }

    public static void main(String s[]) {
        EmployeeImportance employeeImportance = new EmployeeImportance();
        Employee e1 = new Employee(1);
        e1.importance = 5;
        e1.subordinates = Arrays.asList(2, 3);
        Employee e2 = new Employee(2);
        e2.importance = 3;
        e2.subordinates = Arrays.asList(4);

        Employee e3 = new Employee(3);
        e3.importance = 4;
        List<Employee> list = new ArrayList<>();

        Employee e4 = new Employee(4);
        e4.importance = 1;
        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);

        employeeImportance.findImportance(list, 2);
    }
}
