package April_17;

import java.util.List;

public class InfiniteArray {

    static public int findFirstOne(List<Integer> array) { // [0 0 0 0 0 0 0 0 0 1 1  1  1 1 1 1111]
        //  0 1 2 3 4 5 6 7 8 9 10 11

        int n = 0;
        int low = (int) Math.pow(2, n) - 1;
        int high = (int) Math.pow(2, n) - 1;

        while (array.get(high) != 1 && high < array.size()) {


            if (array.get(high) == 0) { //

                low = high; //
                n++;
                high = ((int) Math.pow(2, n)) - 1; //
            }

        }
        if (high >= array.size())

            return -1;
        else
            return binarySearch(low, high, array);

    }

    //9.      10
    static public int binarySearch(int start, int end, List<Integer> array) {

        while (start < end) {

            int mid = (start + end) / 2; // mid =

            if (array.get(mid) == 0) {
                start = mid + 1;// start = 9
            } else {
                end = mid; // high = 10
            }
        }

        return start;

    }

}
