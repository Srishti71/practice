package April_17;

import common.TreeNode;

public class IdenticalBinaryTree {

    public static void main(String s[]) {

        TreeNode root1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);

        root1.left = node3;
        root1.right = node2;
        node2.left = node5;
        node2.right = node4;

        TreeNode root2 = new TreeNode(1);
        TreeNode node6 = new TreeNode(2);
        TreeNode node7 = new TreeNode(3);
        TreeNode node8 = new TreeNode(4);
        TreeNode node9 = new TreeNode(5);

        root2.left = node6;
        root2.right = node7;
        node6.left = node8;

        IdenticalBinaryTree identicalBinaryTree = new IdenticalBinaryTree();

        System.out.println(identicalBinaryTree.isIdentical(root1, root2));

    }

    public boolean isIdentical(TreeNode node1, TreeNode node2) {

        if (node1 == null && node2 == null)
            return true;

        if (node1 != null && node2 == null || node1 == null && node2 != null)
            return false;

        return node1.val == node2.val && isIdentical(node1.left, node2.right) && isIdentical(node1.right, node2.left);
    }

}
