package April_17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidBinaryTree {

    Map<Integer, List<Integer>> parentMap = new HashMap();
    Map<Integer, Integer> indegreeMap = new HashMap();

    public static void main(String s[]) {
        ValidBinaryTree valid = new ValidBinaryTree();
        int[][] pairs = {{1, 2}, {2, 1}};
        System.out.println(valid.validBT(pairs));
    }

    public boolean validBT(int[][] pairs) {

        for (int[] pair : pairs) {
            int parent = pair[0];
            int child = pair[1];

            List<Integer> list = parentMap.getOrDefault(parent, new ArrayList<>());
            list.add(child);
            parentMap.put(parent, list);
            indegreeMap.put(child, indegreeMap.getOrDefault(child, 0) + 1);

            if (list.size() > 2 || indegreeMap.get(child) > 1) {
                return false;
            }
        }

        List<Integer> root = new ArrayList<>();
        for (Integer potentialRoot : parentMap.keySet()) {
            if (indegreeMap.get(potentialRoot) == null) {
                root.add(potentialRoot);
            }
        }

        if (root.size() != 1) {
            return false;
        }

        return true;
    }

}
