package April_17;

public class SplitArray {

    public int splitArray(int[] nums, int m) {

        int max = 0, min = 0;
        for (int i = 0; i < nums.length; i++) {
            max += nums[i];
            min = Math.max(min, nums[i]);
        }
        if (m == 1) {
            return max;
        }
        int res = 0;

        while (min < max) {
            int mid = min + (max - min) / 2;

            if (isPossible(nums, mid, m)) {
                res = mid;
                max = mid - 1;

            } else {
                min = mid + 1;
            }
        }
        return res;
    }

    private boolean isPossible(int[] nums, int mid, int m) {

        int k = 1;
        int currSum = 0;
        for (int i : nums) {
            if (currSum + i > mid) {
                currSum = i;
                k++;
                if (k > m) {
                    return false;
                }
            } else {
                currSum += i;
            }
        }
        return true;
    }

    public static void main(String s[]) {
        SplitArray sa = new SplitArray();
        int[] nums = {7, 2, 5, 10, 8};
        System.out.println(sa.splitArray(nums, 2));
    }
}
