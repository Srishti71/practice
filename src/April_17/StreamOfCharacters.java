package April_17;

import common.TrieNode;

import java.util.ArrayList;
import java.util.List;

public class StreamOfCharacters {

    TrieNode root = new TrieNode();
    List<Character> list = new ArrayList();

    public static void main(String s[]) {
        String[] words = {"cd", "f", "kl"};
        StreamOfCharacters stream = new StreamOfCharacters(words);
        System.out.println(stream.query('a'));
        System.out.println(stream.query('b'));
        System.out.println(stream.query('c'));
        System.out.println(stream.query('d'));
        System.out.println(stream.query('e'));
        System.out.println(stream.query('f'));
        System.out.println(stream.query('g'));
    }

    public StreamOfCharacters(String[] words) {
        for (String word : words) {
            constructTrie(word);
        }
    }

    public boolean query(char letter) {
        list.add(letter);
        TrieNode node = root;
        int size = list.size();
        for (int i = size - 1; i >= 0; i--) {
            int index = list.get(i) - 'a';
            if (node.children[index] == null)
                return false;
            else {
                if (node.children[index].isWord)
                    return true;
                node = node.children[index];
            }
        }
        return false;
    }

    public void constructTrie(String word) {
        TrieNode node = root;

        for (int i = word.length() - 1; i >= 0; i--) {
            int index = word.charAt(i) - 'a';
            if (node.children[index] == null) {
                node.children[index] = new TrieNode();
            }
            node = node.children[index];
        }
        node.isWord = true;
    }

}
