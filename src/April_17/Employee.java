package April_17;

import java.util.ArrayList;
import java.util.List;

public class Employee {

    public int id;
    public int importance;
    public List<Integer> subordinates = new ArrayList<>();

    Employee(int id) {
        this.id = id;
    }
}
