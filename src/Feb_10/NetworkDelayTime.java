package Feb_10;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Stack;

class Table {
    int node;
    int cost;
    int prevNode;

    Table(int cost, int prevNode) {
        this.cost = cost;
        this.prevNode = prevNode;
    }
}

public class NetworkDelayTime {

    public static void main(String s[]) {
        NetworkDelayTime networkDelayTime = new NetworkDelayTime();

        int[][] times = {{1, 2, 1}};

        int N = 2, K = 2;
        System.out.println(networkDelayTime.networkDelayTime(times, N, K));

    }

    public int networkDelayTime(int[][] times, int N, int K) {

        Table[] queue = new Table[N + 1];

        boolean[] visited = new boolean[N + 1];

        //boolean[] unvisited
        for (int i = 1; i <= N; i++) {
            queue[i] = new Table(Integer.MAX_VALUE, 0);
        }

        queue[K] = new Table(0, 0);

        int element = hasLowestElement(queue, visited);
        while (element != -1) {

            visited[element] = true;
            for (int i = 0; i < times.length; i++) {
                if (times[i][0] == element) {
                    if (queue[times[i][1]].cost > queue[times[i][0]].cost + times[i][2]) {
                        queue[times[i][1]].cost = queue[times[i][0]].cost + times[i][2];
                    }
                }
            }

            element = hasLowestElement(queue, visited);
        }
        return findMinCost(queue);
    }

    private int findMinCost(Table[] queue) {
        int maxCost = Integer.MIN_VALUE;
        for (int i = 1; i < queue.length; i++) {
            if( queue[i].cost == Integer.MAX_VALUE)
                return -1;

            maxCost = Math.max(maxCost, queue[i].cost);
        }
        System.out.println("maxCost" +maxCost+" "+Integer.MIN_VALUE);
        if (maxCost == Integer.MIN_VALUE)
            return -1;
        return maxCost;
    }


    private int hasLowestElement(Table[] queue, boolean[] visited) {
        int minIndex = -1;
        int minValue = Integer.MAX_VALUE;
        for (int i = 1; i < queue.length; i++) {
            if (!visited[i]) {
                if (minValue > queue[i].cost) {
                    minIndex = i;
                    minValue = queue[i].cost;
                }
            }
        }
        return minIndex;

    }

    public int networkDelay(int[][] times, int N, int K) {

        Stack<Pair<Integer, Integer>> queue = new Stack<Pair<Integer, Integer>>();

        queue.add(new Pair(K, 0));

        int[] visited = new int[N + 1];
        Arrays.fill(visited, Integer.MAX_VALUE);

        boolean[] pairsVisited = new boolean[times.length];
        while (!queue.isEmpty()) {
            Pair currentElement = queue.pop();
            Integer element = (Integer) currentElement.getKey();

            for (int i = 0; i < times.length; i++) {

                if (times[i][0] == element && !pairsVisited[i]) {

                    if (visited[element] < (Integer) currentElement.getValue()) {
                        queue.add(new Pair(times[i][1], times[i][2] + visited[element]));
                    } else {
                        queue.add(new Pair(times[i][1], times[i][2] + (Integer) currentElement.getValue()));
                    }
                    pairsVisited[i] = true;
                }
            }
        }

        int result = allVisted(visited);
        if (result != -1) {
            return result;
        }
        return -1;
    }

    public int allVisted(int[] visited) {

        int maxCost = Integer.MIN_VALUE;
        for (int i = 1; i < visited.length; i++) {
            if (visited[i] == Integer.MAX_VALUE) {
                return -1;
            } else {
                maxCost = Math.max(maxCost, visited[i]);
            }
        }
        return maxCost;
    }
}
