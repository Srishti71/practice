package Feb_10;

import javafx.util.Pair;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Award {

    public static void main(String s[]) {
        Award award = new Award();
        int[][] times = {{4, 2, 76}, {1, 3, 79}, {3, 1, 81}, {4, 3, 30}, {2, 1, 47}, {1, 5, 61}, {1, 4, 99}, {3, 4, 68}, {3, 5, 46}, {4, 1, 6}, {5, 4, 7}, {5, 3, 44}, {4, 5, 19}
                , {2, 3, 13}, {3, 2, 18}, {1, 2, 0}, {5, 1, 25}, {2, 5, 58}, {2, 4, 77}, {5, 2, 74}};
    }

    public boolean checkRecord(String s) {
        int countA = 0, countL = 0;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'A') {
                countA++;
                countL = 0;
            } else if (s.charAt(i) == 'L') {
                countL++;
            } else {
                countL = 0;
            }

            if (countA > 1 || countL > 2) {
                return false;
            }
        }
        return true;
    }
}
