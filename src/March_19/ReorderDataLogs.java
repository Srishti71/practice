package March_19;

import java.util.Arrays;

// 33 questions
//
public class ReorderDataLogs {

    public String[] reorderLogFiles(String[] logs, Integer[] data) {

       /* Arrays.sort(data, (a, b) -> {
            System.out.println(a + " " + b + " " + (a - b));
            return -1;
        });

        for (int l : data) {
            System.out.println(l);
        } */
       Arrays.sort(logs, (a, b) -> {
            String a1[] = a.split(" ", 2);
            String a2[] = b.split(" ", 2);

            boolean a1Digits = Character.isDigit(a1[1].charAt(0));
            boolean a2Digits = Character.isDigit(a2[1].charAt(0));

            if (!a1Digits && !a2Digits) {
                String a1First = a1[1];
                String a2First = a2[1];

                int comp = a1First.compareTo(a2First);

                if (comp != 0) return comp;

                return a1[0].compareTo(a2[0]);
            }


            if(a1Digits) {
                if(a2Digits) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                return -1;
            }
        });

        for (String l : logs) {
            System.out.println(l);
        }

        return logs;
    }

    public static void main(String s[]) {
        String logs[] = {"dig1 8 1 5 1", "let1 art can", "dig2 3 6", "let2 own kit dig", "let3 art zero"};

        Integer[] data = {3, 2, 4};

        ReorderDataLogs reorderDataLogs = new ReorderDataLogs();
        reorderDataLogs.reorderLogFiles(logs, data);
    }

}
