package March_19;

import java.util.ArrayList;
import java.util.List;

public class PartitionLabel {

    public List<Integer> partitionLabels(String S) {

        int[] list = new int[26];
        List<Integer> result = new ArrayList<>();

        for (int i = 0; i < S.length(); i++) {
            list[S.charAt(i) - 'a'] = i;
        }

        int j = 0, anchor = 0;
        for (int i = 0; i < S.length(); i++) {
            j = Math.max(j, list[S.charAt(i) - 'a']);
            if (j == i) {
                result.add(j - anchor + 1);
                anchor = i + 1;
            }
        }
        return result;
    }

}
