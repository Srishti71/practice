package March_27;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WorkBreak {

    int count=0;
    public boolean wordBreak(String s, List<String> wordDict) {
        return word_Break(s, new HashSet(wordDict), 0);
    }
    public boolean word_Break(String s, Set<String> wordDict, int start) {

        if (start == s.length()) {
            return true;
        }
        count++;
        for (int end = start + 1; end <= s.length(); end++) {
            if (wordDict.contains(s.substring(start, end)) && word_Break(s, wordDict, end)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String s[]) {
        WorkBreak w = new WorkBreak();
        String str = "aaaaa";
        List<String> list = Arrays.asList("a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa",  "aaaaaaaa",  "aaaaaaaaa", "aaaaaaaaaa");
        System.out.println(w.wordBreak(str, list));
        System.out.println(w.count);
    }
}
