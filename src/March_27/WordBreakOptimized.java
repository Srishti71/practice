package March_27;

import java.util.*;

public class WordBreakOptimized {
    Set<String> set;
    Map<String, Boolean > map = new HashMap();

    public boolean wordBreak(String s, List<String> dict) {
        set = new HashSet(dict);
        return wordBreakUtil(s, set);
    }
    public boolean wordBreakUtil(String s, Set<String> set) {

        if(map.containsKey(s)) {
            return map.get(s);
        }

        if (s.isEmpty() || set.contains(s)) {
            return true;
        }

        for (int i = 1; i < s.length(); i++) {
            String left = s.substring(0,i);
            String right = s.substring(i);
            boolean result = wordBreakUtil(right, set);
            map.put(right, result);
            if (set.contains(left) && result) {
                return true;
            }
        }

        return false;

    }
}
