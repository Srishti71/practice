package Feb_5;

public class TargetSum {
    int value = 0;
    public int findTargetSumWays(int[] nums, int S) {
         findTargetSumWaysHelper(nums, S, 0, 0);
         return value;
    }

    public void findTargetSumWaysHelper(int[] nums, int target, int currSum, int index) {
        if(index >= nums.length) {
            if(currSum == target) {
                value++;
            }
            return;
        }

        findTargetSumWaysHelper(nums, target, currSum + nums[index], index + 1);
        findTargetSumWaysHelper(nums, target, currSum - nums[index], index + 1);
    }

    public static void main(String s[]) {
        TargetSum targetSum = new TargetSum();
        int[] nums = {1, 1, 1, 1, 1};
        int target = 3;
        System.out.println(targetSum.findTargetSumWays(nums, target));
    }
}
