package Feb_5;

import java.util.Arrays;

public class TargetSumUsingMemorization {
    int memo[][];

    public int findTargetSumWays(int[] nums, int S) {
        memo = new int[nums.length][2001];

        for (int i[] : memo) {
            Arrays.fill(i, Integer.MIN_VALUE);
        }
        return findTargetSumWaysHelper(nums, S, 0, 0);
    }

    private int findTargetSumWaysHelper(int[] nums, int s, int index, int currSum) {

        if (nums.length == index) {
            if (currSum == s) {
                return 1;
            } else {
                return 0;
            }
        }
        if (memo[index][currSum + 1000] != Integer.MIN_VALUE) {
            return memo[index][currSum + 1000];
        }

        int add = findTargetSumWaysHelper(nums, s, index + 1, currSum + nums[index]);

        int sub = findTargetSumWaysHelper(nums, s, index + 1, currSum - nums[index]);

        memo[index][currSum + 1000] = add + sub;

        return memo[index][currSum + 1000];
    }

    public static void main(String s[]) {
        TargetSumUsingMemorization targetSumUsingMemorization = new TargetSumUsingMemorization();
        int[] nums = {1, 1, 1, 1, 1};
        int target = 3;
        System.out.println(targetSumUsingMemorization.findTargetSumWays(nums, target));
    }

}
