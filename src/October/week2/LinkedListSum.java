package October.week2;

import common.ListNode;

import java.util.LinkedList;

public class LinkedListSum {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        if(l1 == null && l2 == null)
            return null;

        ListNode result = null;
        ListNode prev = new ListNode(-1);
        int carry = 0;

        while (l1 != null && l2 != null) {
            int sum = carry + l1.val + l2.val;
            carry = sum / 10;
            sum = sum % 10;

            prev.next = new ListNode(sum);
            prev = prev.next;
            if (result == null) {
                result = prev;
            }
            l1 = l1.next;
            l2 = l2.next;
        }

        while (l1 != null) {
            int sum = carry + l1.val;
            carry = sum / 10;
            sum = sum % 10;

            prev.next = new ListNode(sum);
            prev = prev.next;

            if(result == null) {
                result = prev;
            }

            l1 = l1.next;
        }

        while (l2 != null) {
            int sum = carry + l2.val;
            carry = sum / 10;
            sum = sum % 10;

            prev.next = new ListNode(sum);
            prev = prev.next;
            if(result == null) {
                result = prev;
            }
            l2 = l2.next;
        }

        if (carry != 0) {
            prev.next = new ListNode(carry);
        }

        return result;
    }


    public static void main(String[] args) {
        ListNode head1 = new ListNode(5);
        //head1.next = new ListNode(4);
        //head1.next.next = new ListNode(6);

        ListNode head2 = new ListNode(5);
        //head2.next = new ListNode(6);
        //head2.next.next = new ListNode(4);

        LinkedListSum llSum = new LinkedListSum();

        ListNode res = llSum.addTwoNumbers(null, head2);
        while (res != null) {
            System.out.println(res.val);
            res = res.next;
        }

    }

}
