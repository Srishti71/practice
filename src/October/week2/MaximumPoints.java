package October.week2;

public class MaximumPoints {

    public int maxScore(int[] cardPoints, int k) {

        if (cardPoints.length == k) {

            int sum = 0;

            for (int i : cardPoints) {
                sum += i;
            }

            return sum;
        }
        int res = dfs(cardPoints, 0, cardPoints.length - 1, k);

        System.out.println("Result: " + res);
        return res;

    }

    private int dfs(int[] cardPoints, int start, int end, int k) {

        if (k == 0) {
            return 0;
        }

        int left = cardPoints[start] + dfs(cardPoints, start + 1, end, k - 1);
        int right = cardPoints[end] + dfs(cardPoints, start, end - 1, k - 1);

        return Math.max(left, right);
    }

    public static void main(String[] args) {

        /*
        int cardPoints[] =
                {53, 14, 91, 35, 51, 9, 80, 27, 6, 15, 77, 86, 34, 62, 55,
                45, 91, 45, 23, 75, 66, 42, 62, 13, 34, 18, 89, 67, 93, 83,
                100, 14, 92, 73, 48, 2, 47, 93, 99, 100, 88, 84, 48};

         */

        int cardPoints[] = {61, 5, 22, 64, 14, 16, 93, 28, 7, 99, 8, 17,
                2, 83, 9, 88, 16, 97, 33, 50, 78, 5,
                78, 100, 100, 44, 43, 73, 14, 31, 1,
                72, 93, 42, 48, 49, 3, 26, 59, 8, 20,
                39, 40, 67, 27};

        int k = 34;



        MaximumPoints maxPoint = new MaximumPoints();
        maxPoint.maxScore(cardPoints, k);

    }
}

