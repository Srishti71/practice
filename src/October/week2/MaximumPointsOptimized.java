package October.week2;

public class MaximumPointsOptimized {

    public int maxScore(int[] cardPoints, int k) {

        int len = cardPoints.length;

        if (len == 0 || k == 0) {
            return 0;
        }

        if (k == len) {
            int sum = 0;
            for (int i : cardPoints) {
                sum += i;
            }

            return sum;
        }

        int[] left = new int[len];
        int[] right = new int[len];

        left[0] = cardPoints[0];
        right[len - 1] = cardPoints[len - 1];


        for (int i = 1; i < len; i++) {
            left[i] = left[i - 1] + cardPoints[i];
            right[len - i - 1] = right[len - i] + cardPoints[len - i - 1];
        }


        int total = 0;
        for (int i = 0; i <= k; i++) {
            int sum ;
            if (i == 0)
                sum = right[len - k+i];
            else if(i == k) sum = left[i-1];

            else sum = left[i-1] + right[len-k + i];

            total = Math.max(total, sum);
        }

        return total;
    }
}
