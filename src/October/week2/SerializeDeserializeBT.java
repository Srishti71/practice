package October.week2;

import April_18.SerializeDeserializeBST;
import common.TreeNode;

import java.util.Arrays;
import java.util.List;

public class SerializeDeserializeBT {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String s = "";
        if (root == null) {
            s += "null,";
        } else {
            s = root.val + ",";

            s += serialize(root.left);
            s += serialize(root.right);
        }

        System.out.println(s);

        return s;
    }


    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String array[] = data.split(",");
        List<String> l = Arrays.asList(array);


        return deserilaizeRecur(l);
    }

    private TreeNode deserilaizeRecur(List<String> l) {

        if (l.get(0).equals("null")) {
            l.remove(0);
            return null;
        } else {
            TreeNode root = new TreeNode(Integer.parseInt(l.get(0)));
            l.remove(0);

            root.left = deserilaizeRecur(l);
            root.right = deserilaizeRecur(l);
            return root;
        }
    }


    public static void main(String[] args) {
        SerializeDeserializeBT serialDes = new SerializeDeserializeBT();

        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(3);
       /* TreeNode node3 = new TreeNode(4);
        TreeNode node4 = new TreeNode(5);

        TreeNode node5 = new TreeNode(6);
        TreeNode node6 = new TreeNode(7);
        TreeNode node7 = new TreeNode(8);

        root.left = node1;
        root.right = node2;

        node1.left = node3;
        node1.right = node4;


        node3.left = node5;
        node3.right = node6;

        node4.left = node7; */

        serialDes.serialize(root);
    }
}
