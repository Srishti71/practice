package October.week2;

import java.util.Map;
import java.util.TreeMap;

public class TwoSum {

    TreeMap<Integer, Integer> treeMap;

    /**
     * Initialize your data structure here.
     */
    public TwoSum() {
        treeMap = new TreeMap();
    }

    /**
     * Add the number to an internal data structure..
     */
    public void add(int number) {
        int count = treeMap.getOrDefault(number, 0);
        treeMap.put(number, count + 1);
    }

    /**
     * Find if there exists any pair of numbers which sum is equal to the value.
     */
    public boolean find(int value) {
        for (Map.Entry<Integer, Integer> entry : treeMap.entrySet()) {
            int key = entry.getKey();

            int diff = value - key;

            if (diff == key) {
                if (treeMap.get(key) > 1) {
                    return true;
                }
            } else {
                if (treeMap.containsKey(diff)) {
                    return true;
                }
            }

        }
        return false;
    }

    public static void main(String[] args) {
        //[[],[0],[-1],[-1],[0],[-2],[0],[-1],[1]]
        TwoSum twoSum = new TwoSum();
        twoSum.add(0);
        twoSum.add(-1);
        twoSum.add(-1);
        twoSum.add(0);
        System.out.println(twoSum.find(-2));
        System.out.println(twoSum.find(0));
        System.out.println(twoSum.find(-1));
        System.out.println(twoSum.find(1));
        //twoSum.add(5);
        System.out.println(twoSum.find(8));
        //System.out.println(twoSum.find(7));
    }
}