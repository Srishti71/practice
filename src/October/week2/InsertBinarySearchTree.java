package October.week2;

import common.TreeNode;

public class InsertBinarySearchTree {
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if(root == null) {
            return new TreeNode(val);
        }
        TreeNode node = root;
        while(node != null) {
            if(node.val < val) {
                if(node.right == null) {
                    node.right = new TreeNode(val);
                    break;
                } else {
                    node = node.right;
                }
            } else {
                if(node.left == null) {
                    node.left = new TreeNode(val);
                    break;
                } else {
                    node = node.left;
                }
            }
        }


        return root;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(7);
        TreeNode node3 = new TreeNode(1);
        TreeNode node4 = new TreeNode(3);


        root.left = node1;
        root.right = node2;

        node1.left = node3;
        node1.right = node4;

        InsertBinarySearchTree insertBST = new InsertBinarySearchTree();
        TreeNode res = insertBST.insertIntoBST(root, 5);
        insertBST.printElement(res);

    }

    public void printElement(TreeNode res) {
        if(res != null) {
            printElement(res.left);
            System.out.println(res.val);
            printElement(res.right);
        }
    }
}
