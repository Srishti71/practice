package October.week2;

import java.util.Arrays;
import java.util.Comparator;

public class CoveredIntervals {

    public int removeCoveredIntervals(int[][] intervals) {
        Arrays.sort(intervals, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                return o1[0] == o2[0] ? o2[1] - o1[1] : o1[0] - o2[0];
            }
        });

        int end, prev_end = 0;
        int count = 0;
        for (int[] cur : intervals) {
            end = cur[1];

            if (prev_end < end) {
                count++;
                prev_end = end;
            }
        }
        System.out.println(count);
        return count;
    }


    public static void main(String[] args) {
        int intervals[][] = {{1, 4}, {3, 6}, {2, 8}};
        CoveredIntervals coveredIntervals = new CoveredIntervals();
        coveredIntervals.removeCoveredIntervals(intervals);
    }
}
