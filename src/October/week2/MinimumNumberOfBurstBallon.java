package October.week2;

import java.util.Arrays;
import java.util.Comparator;

public class MinimumNumberOfBurstBallon {

    public int findMinArrowShots(int[][] points) {

        if (points.length == 0) {
            return 0;
        } else if (points.length == 1) {
            return 1;
        }

        Arrays.sort(points, new Comparator<int[]>() {

            @Override
            public int compare(int[] o1, int[] o2) {
                if (o1[0] == o2[0]) {
                    return Integer.compare(o1[1], o2[1]);
                } else {
                    return Integer.compare(o1[0], o2[0]);
                }
            }
        });

        for (int[] p : points) {
            System.out.println(p[0] + " " + p[1]);
        }
        int start = points[0][0], end = points[0][1];
        int count = 1;
        for (int i = 1; i < points.length; i++) {

            if (end < points[i][0]) {
                count++;
                start = points[i][0];
                end = points[i][1];
            } else {
                start = Math.max(start, points[i][0]);
                end = Math.min(end, points[i][1]);
            }
        }

        System.out.println(count);
        return count;
    }

    public static void main(String[] args) {

        MinimumNumberOfBurstBallon minNum = new MinimumNumberOfBurstBallon();

        int points[][] = {{10, 16}, {2, 8}, {1, 6}, {7, 12}};
        minNum.findMinArrowShots(points);

        int[][] points1 = {{1, 2}, {3, 4}, {5, 6}, {7, 8}};
        minNum.findMinArrowShots(points1);

        int[][] points2 = {{1, 2}, {2, 3}, {3, 4}, {4, 5}};
        minNum.findMinArrowShots(points2);

        int[][] points3 = {{1, 2}};
        minNum.findMinArrowShots(points3);

        int[][] points4 = {{2, 3}, {2, 3}};
        minNum.findMinArrowShots(points4);

        int[][] points5 = {};
        minNum.findMinArrowShots(points5);

        System.out.println("Face-----");
        int[][] points6 = {{-2147483646, -2147483645}, {2147483646, 2147483647}};
        minNum.findMinArrowShots(points6);

    }
}
