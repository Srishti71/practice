package October.week2;

public class Complements {

    public int bitwiseComplement(int N) {

        int result = N, bit = 1;
        while (result != 0) {

            N = N ^ bit; // 100 // 110 // 010
            bit = bit << 1; // 010 // 100 // 00

            result = result >> 1; // 1 // 0

        }

        System.out.println(result);
        return result;
    }

    public static void main(String[] args) {
        Complements comp = new Complements();
        comp.bitwiseComplement(5);
    }
}
