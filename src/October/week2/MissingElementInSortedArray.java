package October.week2;

public class MissingElementInSortedArray {

    public int missingElement(int[] nums, int k) {
        if (nums.length == 1) {
            return nums[0] + k;
        }

        int low = 0, high = nums.length - 1;

        while (low < high) {
            int mid = low + (high - low) / 2;

            if (mid == low) {
                break;
            }

            int missing = nums[mid] - nums[0] - (mid);
            if (k <= missing) {
                high = mid;
            } else {
                low = mid;
            }
        }
        int count = 0;
        for (int i = nums[low] + 1; i <= nums[high]-1; i++) {
            System.out.println(i);
            count++;
            if (count == k) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        int A[] = {4, 7, 9, 10}, K = 3;
        MissingElementInSortedArray miss = new MissingElementInSortedArray();
        System.out.println("Values: " + miss.missingElement(A, K));
    }
}
