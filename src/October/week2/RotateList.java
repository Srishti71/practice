package October.week2;

import common.ListNode;

public class RotateList {
    public ListNode rotateRight(ListNode head, int k) {
        int len = 0;

        ListNode node = head;

        while (node != null) {
            len++;
            node = node.next;
        }

        if (len == 1 || k == len || k%len == 0) {
            return head;
        }

        k = k%len;
        int tail = len - k;


        node = head;
        for (int i = 0; i < tail-1; i++) {
            node = node.next;
        }

        ListNode temp = node.next;
        node.next = null;
        ListNode newTemp = temp;
        while (newTemp.next != null) {
            newTemp = newTemp.next;
        }
        newTemp.next = head;
        return temp;
    }

    public static void main(String[] args) {
        RotateList rotateList = new RotateList();
        ListNode head = new ListNode(0);
        ListNode list1 = new ListNode(1);
        ListNode list2 = new ListNode(2);
        ListNode list3 = new ListNode(3);
        ListNode list4 = new ListNode(4);
        ListNode list5 = new ListNode(5);

        head.next = list1;
        /*list1.next = list2;
        list2.next = list3;
        list3.next = list4;
        list4.next = list5;*/

        ListNode l = rotateList.rotateRight(head, 1);
        while(l != null) {
            System.out.println(l.val);
            l= l.next;
        }
    }
}
