package October.week2;

import java.util.Arrays;

public class DivideChocolate {

    public static void main(String[] args) {

    }

    // 1, 2, 3, 4, 5, 6, 7, 8, 9

    public int maximizeSweetness(int[] sweetness, int K) {
        int low = 1, high = Arrays.stream(sweetness).sum() / (K + 1);

        while (low < high) {
            int mid = (low + high + 1) / 2;
            if (canSplit(sweetness, K, mid)) {
                low = mid;
            } else {
                high = mid - 1;
            }
        }
        return low;
    }

    public boolean canSplit(int[] sweetness, int k, int mid) {

        int chunks = 0, sum = 0;

        for (int i : sweetness) {
            sum += i;
            if (sum >= mid) {
                sum = 0;
                chunks++;
            }
        }

        return chunks >= k + 1;
    }
}
