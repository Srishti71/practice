package October.week2;

import java.util.Arrays;

public class NonOverlappingSubArray {
    public int minSumOfLengths(int[] arr, int target) {

        int len = arr.length;
        int num[] = new int[len];

        Arrays.fill(num, Integer.MAX_VALUE);

        int currSum = 0, left = 0, res = Integer.MAX_VALUE;


        for (int right = 0; right < len; right++) {

            currSum += arr[right];

            while (currSum > target) {
                currSum -= arr[left++];
            }

            if (right > 0) {
                num[right] = num[right - 1];
            }

            if (currSum == target) {
                if (left > 0 && num[left] != Integer.MAX_VALUE) {
                    res = Math.min(right - left + num[left - 1], res);
                }

                num[right] = Math.min(num[right], right - left + num[left - 1]);
            }


        }

        return res == Integer.MAX_VALUE ? -1 : res;

    }

    public static void main(String[] args) {
        NonOverlappingSubArray nonOverlappingSubArray = new NonOverlappingSubArray();
        nonOverlappingSubArray.minSumOfLengths(new int[]{4, 3, 2, 6, 6, 3, 4}, 6);
    }
}
