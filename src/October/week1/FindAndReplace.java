package October.week1;

public class FindAndReplace {

    public String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        StringBuilder result = new StringBuilder();


        int index = 0, i = 0;

        while (index < S.length() && i < indexes.length) {
            while (index < indexes[i]) {
                result.append(S.charAt(index));
                index++;
            }

            int j = 0;
            boolean flag = true;
            for (j = 0; j < sources[i].length() && index < S.length(); j++) {
                if (sources[i].charAt(j) == S.charAt(index)) {
                    index++;
                } else {
                    index = indexes[i];
                    flag = false;
                    break;
                }
            }

            if (flag) {
                result.append(targets[i]);
            }

            i++;
        }

        while(index < S.length()) {
            result.append(S.charAt(index));
            index++;
        }

        System.out.println(result);
        return result.toString();
    }

    public static void main(String[] args) {

        String S = "abcd";
        int[] indexes = {0, 2};
        String[] sources = {"a", "cd"};
        String[] targets = {"eee", "ffff"};

        FindAndReplace findReplace = new FindAndReplace();
        findReplace.findReplaceString(S, indexes, sources, targets);
    }
}


/*
S = "abcd", indexes = [0,2], sources = ["a","cd"], targets = ["eee","ffff"]
Output: "eeebffff"

index = 0

while (index < indexs[i])
result.append(index);
index++

prev =
for: j : source[i].length()
 source[i][j]==  S.charAt(index)
 j++
 index++;

index = index[i];






 */