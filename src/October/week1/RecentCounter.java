package October.week1;

import java.util.LinkedList;

public class RecentCounter {
    LinkedList<Integer> list;
    public RecentCounter() {
        list = new LinkedList<Integer>();
    }

    public int ping(int t) {
        list.addLast(t);
        while(!list.isEmpty()) {
            if(t - list.getFirst()  > 3000) {
                list.removeFirst();
            } else {
                break;
            }
        }
        return list.size();
    }

    public static void main(String[] args) {
        RecentCounter recentCounter = new RecentCounter();
        System.out.println(recentCounter.ping(1));
        System.out.println(recentCounter.ping(100));
        System.out.println(recentCounter.ping(3001));
        System.out.println(recentCounter.ping(3002));

    }
}
