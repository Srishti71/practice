package October.week1;

import javafx.util.Pair;

import java.util.*;

public class MaximumDistance {

    public int maxDistance(List<List<Integer>> arrays) {

        PriorityQueue<Pair<Integer, Integer>> minHeap = new PriorityQueue<Pair<Integer, Integer>>((x, y) -> Integer.compare((Integer) x.getKey(), (Integer) y.getKey()));
        PriorityQueue<Pair<Integer, Integer>> maxHeap = new PriorityQueue<Pair<Integer, Integer>>((x, y) -> Integer.compare((Integer) y.getKey(), (Integer) x.getKey()));


        for (int i = 0; i < arrays.size(); i++) {
            System.out.println(i);
            minHeap.add(new Pair<Integer, Integer>(arrays.get(i).get(0), i));
            int size = arrays.get(i).size();
            maxHeap.add(new Pair<Integer, Integer>(arrays.get(i).get(size - 1), i));
        }


        if (!minHeap.peek().getValue().equals(maxHeap.peek().getValue())) {
            return Math.abs(minHeap.peek().getKey() - maxHeap.peek().getKey());
        } else {
            int firstMin = minHeap.poll().getKey();
            int secondMin = minHeap.poll().getKey();

            int firstMax = maxHeap.poll().getKey();
            int secondMax = maxHeap.poll().getKey();
            return Math.max(Math.abs(firstMax - secondMin), Math.abs(firstMin - secondMax));


        }
    }

    public static void main(String[] args) {
        MaximumDistance maxDist = new MaximumDistance();
        List<List<Integer>> arrays = new ArrayList<>();
        List<Integer> l1 = Arrays.asList(1, 5); // 1 2
        List<Integer> l2 = Arrays.asList(3, 4); //
        //List<Integer> l3 = Arrays.asList(1, 2, 3);

        arrays.add(l1);
        arrays.add(l2);
       // arrays.add(l3);

        System.out.println("Result: " + maxDist.maxDistance(arrays));

    }
}
