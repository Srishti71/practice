package October.week1;

import common.TreeNode;

public class CountCompleteBinaryTree {

    public int countNodes(TreeNode root) {

        TreeNode node = root;
        int depth = getDepth(root);
        System.out.println(depth);
        int low = 0, high = (int) Math.pow(2, depth - 1) - 1;

        while (low <= high) {
            int mid = low + (high - low) / 2;

            if (exist(mid, depth, node)) low = mid + 1;
            else high = mid - 1;
        }
        System.out.println("low: " + low + " " + (Math.pow(2, depth - 1) - 1));
        int res = (int) Math.pow(2, depth - 1) - 1 + low;
        System.out.println(res);
        return res;
    }

    public int getDepth(TreeNode node) {
        if (node == null)
            return 0;

        int count = 0;

        while (node != null) {
            count++;
            node = node.left;
        }

        return count;
    }

    public boolean exist(int value, int depth, TreeNode node) {

        int low = 0, high = (int) Math.pow(2, depth - 1) - 1;

        for (int i = 0; i < depth - 1; i++) {
            int mid = low + (high - low) / 2;

            if (value <= mid) {
                node = node.left;
                high = mid;
            } else {
                node = node.right;
                low = mid + 1;
            }
        }

        return node != null;
    }


    public static void main(String[] args) {

        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(3);

        root.left = node1;
        root.right = node2;

        TreeNode node3 = new TreeNode(4);
        TreeNode node4 = new TreeNode(5);
        TreeNode node5 = new TreeNode(6);

        node1.left = node3;
        node1.right = node4;
        node2.left = node5;

        CountCompleteBinaryTree completeBinaryTree = new CountCompleteBinaryTree();
        completeBinaryTree.countNodes(root);
    }

}

/*


        2  -- 2 ^ 0
       / \
     3     4    -- 1 to  2 ^ 2
   /\       /\
   5 6   5 6

      3-- 7

      22-1 + 4

      2 - 3

       + 2
 */
