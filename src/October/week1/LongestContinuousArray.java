package October.week1;

import java.util.PriorityQueue;

public class LongestContinuousArray {

    public int longestSubarray(int[] nums, int limit) {

        if (nums.length == 0)
            return 0;

        PriorityQueue<Integer> maxHeap = new PriorityQueue<Integer>((a, b) -> (b - a));
        PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>();

        int i = 0, j = 0;
        int result = 0;
        maxHeap.offer(nums[0]);
        minHeap.offer(nums[0]);

        while (j < nums.length ) {

            int low = minHeap.peek();
            int high = maxHeap.peek();

            int diff = high - low;

            if (diff <= limit) {
                if (j - i + 1 > result) {
                    result = j - i + 1;
                }
                j++;
                if(j <  nums.length) {

                    minHeap.add(nums[j]);
                    maxHeap.add(nums[j]);
                }
            } else {
                minHeap.remove(nums[i]);
                maxHeap.remove(nums[i]);
                i++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        LongestContinuousArray longCont = new LongestContinuousArray();
        int[] nums = {8,2,4,7};
        System.out.println(longCont.longestSubarray(nums, 4));
    }

}
