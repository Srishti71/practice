package October.week1;

public class LongestConsecutiveLine {

    public int longestLine(int[][] M) {
        // H, V, D,A
        int[][] dir = {{0, 1}, {1, 0}, {1, 1}, {1, -1}};

        int result = 0;
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
                if (M[i][j] == 1) {
                    int horizontal = dfs(M, i, j, 0, 1, 0);
                    int vertical = dfs(M, i, j, 1, 0, 0);
                    int diagnol = dfs(M, i, j, 1, 1, 0);
                    int anitDiagnol = dfs(M, i, j, 1, -1, 0);
                    result = Math.max(horizontal, result);
                    result = Math.max(vertical, result);
                    result = Math.max(diagnol, result);
                    result = Math.max(anitDiagnol, result);
                }
            }
        }

        System.out.println(result);
        return result;
    }

    private int dfs(int[][] m, int i, int j, int dirRow, int dirCol, int count) {

        if (!isValid(i, j, m)) {
            return count;
        } else {
            return dfs(m, i + dirRow, j + dirCol, dirRow, dirCol, count + 1);
        }
    }

    private boolean isValid(int i, int j, int[][] m) {

        if (i < 0 || j < 0 || i >= m.length || j >= m[0].length || m[i][j] == 0)
            return false;
        return true;
    }

    public static void main(String[] args) {
        int[][] matrix = {{0, 1, 1, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 1}};

        LongestConsecutiveLine consecutiveLine = new LongestConsecutiveLine();
        consecutiveLine.longestLine(matrix);

    }
}
