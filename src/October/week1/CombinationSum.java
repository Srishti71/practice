package October.week1;

import java.util.*;

public class CombinationSum {

    // 2, 2, 3
    // 2,3,2

    List<List<Integer>> res = new ArrayList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {

        /*Set<Map<Integer, Integer>> set = new HashSet<Map<Integer, Integer>>();

        dfs(candidates, target, set, new ArrayList<Integer>(), 0);


        List<List<Integer>> l = traverseMap(set);
        System.out.println(l);
        return l; */

        cal(candidates, target, 0, new ArrayList<>());
        return res;
    }

    private List<List<Integer>> traverseMap(Set<Map<Integer, Integer>> set) {

        List<List<Integer>> list = new ArrayList<>();
        for (Map<Integer, Integer> m : set) {
            List<Integer> l = new ArrayList();

            for (int value : m.keySet()) {
                for (int i = 0; i < m.get(value); i++) {
                    l.add(value);
                }
            }
            list.add(l);
        }
        return list;
    }

    public void dfs(int[] candidate, int target, Set<Map<Integer, Integer>> set, List<Integer> list, int sum) {

        if (sum == target) {
            Map<Integer, Integer> map = new HashMap();
            for (int value : list) {
                int count = map.getOrDefault(value, 0);
                map.put(value, count + 1);
            }
            set.add(map);
            return;
        }

        if (sum > target) {
            return;
        }

        for (int i = 0; i < candidate.length; i++) {

            list.add(candidate[i]);

            sum += candidate[i];
            dfs(candidate, target, set, list, sum);
            if (!list.isEmpty()) {
                list.remove(list.size() - 1);
                sum -= candidate[i];
            }
        }
    }

    public void cal(int arr[], int target, int index, List<Integer> tmp) {
        System.out.println(index + " " + tmp+" "+target);
        if (target < 0 || index == arr.length) {
            return;
        }

        if (target == 0) {
            res.add(new ArrayList<>(tmp));
            return;
        }

        cal(arr, target, index + 1, tmp);

        tmp.add(arr[index]);
        cal(arr, target - arr[index], index, tmp);
        tmp.remove(tmp.size() - 1);
    }

    public static void main(String[] args) {
        CombinationSum combiSum = new CombinationSum();
        int candidates[] = {2,3,6,7}, target = 7;
        combiSum.combinationSum(candidates, target);
    }
}

/*

 [2,3,6,7], target = 7
        //call index = 0
            //call index = 1

               call index = 2
                    call index = 3
                        call index 4
                            return
                        tmp [7]
                            call rc2 target = 0 index = 3
                            res[7]
                            return
                            tmp []
                    tmp[6] target = 1 index = 2
                        rc1: index = 3
                            tmp [6, 7] target = -6 index = 3
                            tmp [6]


        if(target < 0 || index == arr.length)
        {
            return;
        }

        if(target == 0)
        {
            res.add(new ArrayList<>(tmp));
            return;
        }

        cal(arr,target,index+1,tmp); // index = 2

        tmp.add(arr[inde x]); // tmp [6]
        cal(arr,target-arr[index],index,tmp); // 1
        tmp.remove(tmp.size()-1); // []


 */
