package October.week1;

import java.util.ArrayList;
import java.util.List;

public class LongestContinueousArray {

    public int longestSubarray(int[] nums, int limit) {

        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            List<Integer> list = new ArrayList<>();

            list.add(nums[i]);
            max = Math.max(max, dfs(nums, list, i + 1, limit));
        }
        return max;
    }

    private int dfs(int[] nums, List<Integer> list, int i, int limit) {

        if (i == nums.length) {
            return list.size();
        }

        for (int element : list) {
            if (Math.abs(nums[i] - element) > limit) {
                return list.size();
            }
        }

        list.add(nums[i]);
        return dfs(nums, list, i + 1, limit);
    }

    public static void main(String[] args) {
        LongestContinueousArray longCont = new LongestContinueousArray();
        int[] nums = {8,2,4,7};
        System.out.println(longCont.longestSubarray(nums, 4));
    }
}
