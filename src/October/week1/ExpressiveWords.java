package October.week1;

import java.util.ArrayList;
import java.util.List;

public class ExpressiveWords {
    public int expressiveWords(String S, String[] words) {
        if(S.length()==0) {
            return 0;
        }
        Encoding sEncoding = new Encoding(S);

        int result = 0;
        label: for (String word : words) {
            Encoding wordEncoding = new Encoding(word);
            if (!wordEncoding.key.equals(sEncoding.key)) {
                continue;
            }
            for (int i = 0; i < wordEncoding.count.size(); i++) {

                int c1 = sEncoding.count.get(i);
                int c2 = wordEncoding.count.get(i);

                if (c1 < 3 && c1 != c2 || c2 > c1) {
                    continue label;
                }
            }
            result++;
        }
        System.out.println(result);
        return result;
    }

    public static void main(String[] args) {
        ExpressiveWords exp = new ExpressiveWords();
        String s = "";
        String[] word = {"hello", "hi", "helo"};
        exp.expressiveWords(s, word);
    }
}


class Encoding {

    String key= "";
    List<Integer> count = new ArrayList();

    Encoding(String word) {

        int prev = 0;
        // heeell
        key += word.charAt(0);
        for (int i = 1; i < word.length(); i++) {
            if (word.charAt(i - 1) != word.charAt(i)) {
                key += word.charAt(i);
                count.add(i - prev);
                prev = i;
            }
        }
        count.add(word.length() - prev);
        System.out.println(key+" "+ count);
    }
}

