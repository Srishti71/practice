package October.week1;

import java.util.*;

public class K_Diff_Pairs {

    public int findPairs(int[] nums, int k) {

       /* TreeSet<Integer> set = new TreeSet<>();

        for (int i : nums) {
            set.add(i);
        }

        List<Integer> list = new ArrayList<Integer>(set);
        */

        Arrays.sort(nums);
        Map<Integer, Integer> map = new HashMap();

        int low = 0, high = nums.length - 1;
        int count = 0;
        while (low < nums.length - 1) {
            int sum = nums[high] - nums[low];
            if (sum > k) {
                high--;
                if (low == high) {
                    low++;
                    high = nums.length - 1;
                }
            } else {
                if (sum == k) {
                    if (!map.containsKey(nums[low])) {
                        System.out.println(nums[low] + " " + nums[high]);
                        count++;
                        map.put(nums[low], nums[high]);
                    }
                }
                low++;
                high = nums.length - 1;
            }
        }
        System.out.println(map);
        System.out.println(count);
        return count;
    }

    public static void main(String[] args) {
        int nums[] = {1, 3, 1, 5, 4}, k = 0;
        K_Diff_Pairs kDiff = new K_Diff_Pairs();
        kDiff.findPairs(nums, k);
    }
}
