package september.week4;

import java.util.Stack;

public class ValidateStackSequence {

    public boolean validateStackSequences(int[] pushed, int[] popped) {
        if (pushed.length != popped.length) {
            return false;
        }

        if (pushed.length == 0 && popped.length == 0) {
            return true;
        }

        Stack<Integer> stack = new Stack<>();
        int i = 0, j = 0;
        while (i < pushed.length) {

            if (stack.isEmpty() || stack.peek() != popped[j]) {
                stack.push(pushed[i]);
                i++;
            } else {
                stack.pop();
                j++;
            }
        }
        while (!stack.isEmpty() && j < popped.length) {
            if (stack.pop() != popped[j]) {
                return false;
            }
            j++;
        }

        return stack.isEmpty();
    }

    public static void main(String[] args) {
        ValidateStackSequence validSeq = new ValidateStackSequence();
        System.out.println(validSeq.validateStackSequences(new int[]{1, 2, 3, 4, 5}, new int[]{4, 3, 5, 1, 2}));
    }

}

/*
pushed [1,2,3,4,5], popped = [4,3,5,1,2]
      i=0,                  j=0

stack -> [1] i =1
stack -> [1, 2] i =2
stack -> [1, 2, 3] i =3
stack -> [1, 2, 3, 4] i= 4 top = pop(j)
pop()
i =4,  j = 1
stack -> [1, 2, 3] top = pop(j)
pop() j = 2
stack -> [1, 2]
stack -> [1, 2, 5]
 */