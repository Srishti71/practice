package september.week4;


import java.util.TreeMap;

public class DivideArrayConsecutive {
    public boolean isPossibleDivide(int[] nums, int k) {
        TreeMap<Integer, Integer> map = new TreeMap();

        for (int i : nums) {
            int count = map.getOrDefault(i, 0);
            map.put(i, count + 1);
        }
        int len = nums.length;
        while (len > 0) {

            int smallestValue = map.firstKey();
            for (int i = 0; i < k; i++) {
                if (map.get(smallestValue) == null) {
                    return false;
                }
                map.put(smallestValue, map.get(smallestValue) - 1);
                if (map.get(smallestValue) == 0) {
                    map.remove(smallestValue);
                }
                smallestValue++;
            }
            len -= k;
        }

        return len == 0;
    }

    public static void main(String s[]) {
        int nums[] = {3, 2, 1, 2, 3, 4, 3, 4, 5, 9, 10, 11};
        DivideArrayConsecutive cons = new DivideArrayConsecutive();
        System.out.println(cons.isPossibleDivide(nums, 3));
    }
}
