package september.week4;

import java.util.ArrayList;
import java.util.List;

public class RemoveComments {
/*
Not the best solution
 */
    public List<String> removeComments(String[] source) {
        int i = 0, n = source.length;
        List<String> result = new ArrayList<>();

        while (i < n) {
            char charLine[] = source[i].toCharArray();
            StringBuilder temp = new StringBuilder();
            lable:
            for (int j = 0; j < charLine.length; j++) {
                char current = charLine[j];
                if (current == '/') {
                    if (j + 1 < charLine.length) {
                        char next = charLine[j + 1];
                        if (next == '/') {
                            if (!temp.toString().isEmpty()) {
                                result.add(temp.toString());
                            }
                            break;
                        } else if (next == '*') {
                            int startIndex = j + 2;
                            while (i < n) {

                                int index = findEndingBlock(i, startIndex, source);
                                if (index == source[i].length()) {
                                    i++;
                                    startIndex = 0;
                                } else {
                                    String str = source[i].substring(index + 1);
                                    if (!str.isEmpty())
                                        temp.append(str);
                                    break lable;
                                }
                            }
                        } else {
                            String str = current + "" + next;
                            if (!str.isEmpty())
                                temp.append(str);
                        }
                        j = j + 1;
                    } else {
                        String str = current + "";
                        if (!str.isEmpty())
                            temp.append(str);
                    }
                } else {
                    String str = current + "";
                    if (!str.isEmpty())
                        temp.append(str);
                }
            }
            i++;
            if (!temp.toString().isEmpty()) {
                result.add(temp.toString());
            }
        }
        System.out.println(result);
        return result;
    }

    private int findEndingBlock(int i, int startindex, String[] source) {
        char[] charArray = source[i].toCharArray();
        int n = charArray.length;
        for (int j = startindex; j < n; j++) {
            if (charArray[j] == '*' && (j + 1) < n && charArray[j + 1] == '/') {
                return j + 1;
            }
        }
        return n;
    }


    public static void main(String s[]) {
        RemoveComments remove = new RemoveComments();
        String[] source = new String[6];

        // ["main() { ", "  int a = 1; /* Its comments here ", "", "  ", "  */ return 0;", "} "]

        source[0] = "main() {";
        source[1] = " int a = 1; /* Its comments here ";
        source[2] = "";
        source[3] = " ";
        source[4] = "  */ return 0;";
        source[5] = "}";
        /*source[6] = "   multiline  ";
        source[7] = "comment for";
        source[8] = "  testing ";
        source[9] = "a = b + c;";
        source[10] = "}";*/


        remove.removeComments(source);
    }

    /*

    read line by line
        String temp:
        read char by char look first for '/'
            if next *
                look for * in the rest of the same line which is not followed by //

                next line which is not followed by //
            if next is //
                add temp to array if temp is not empty.
     */
}
