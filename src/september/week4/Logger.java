package september.week4;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class Logger {

    LinkedList<logs> queue = new LinkedList<logs>();
    HashSet<String> set = new HashSet();


    int start = 0;

    /**
     * Initialize your data structure here.
     */
    public Logger() {

    }

    /**
     * Returns true if the message should be printed in the given timestamp, otherwise returns false.
     * If this method returns false, the message will not be printed.
     * The timestamp is in seconds granularity.
     */
    public boolean shouldPrintMessage(int timestamp, String message) {

        while (!queue.isEmpty()) {
            logs log = queue.getFirst();
            if (timestamp - log.timestamp > 10) {
                queue.removeFirst();
                set.remove(log.message);
            }
        }

        if (set.contains(message)) {
            return false;
        } else {
            set.add(message);
            queue.addLast(new logs(timestamp, message));
            return true;
        }

    }

    static class logs {
        int timestamp;
        String message;

        logs(int timestamp, String message) {
            this.timestamp = timestamp;
            this.message = message;
        }
    }

    public static void main(String[] args) {
        Logger logger = new Logger();

// logging string "foo" at timestamp 1
        System.out.println(logger.shouldPrintMessage(1, "foo"));

        System.out.println("Should return true");

// logging string "bar" at timestamp 2
        System.out.println(logger.shouldPrintMessage(2, "bar"));
        System.out.println("Should return true");

// logging string "foo" at timestamp 3
        System.out.println(logger.shouldPrintMessage(3, "foo"));

        System.out.println("Should return false");

/* logging string "bar" at timestamp 8
        logger.shouldPrintMessage(8,"bar"); returns false;

// logging string "foo" at timestamp 10
        logger.shouldPrintMessage(10,"foo"); returns false;

// logging string "foo" at timestamp 11
        logger.shouldPrintMessage(11,"foo"); returns true; */
    }
}
