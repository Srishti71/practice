package september.week4;

import java.util.*;

public class WordSquares {
    public List<List<String>> wordSquares(String[] words) {
        if(words.length == 0) {
            return new ArrayList();
        }
        Map<String, List<String>> map = new HashMap();
        buildHashmapForPrefix(words, map);
        List<List<String>> result = new ArrayList<>();
        int n = words[0].length();
        for (int i = 0; i < words.length; i++) {
            LinkedList<String> wordSquare = new LinkedList();
            wordSquare.add(words[i]);
            backtrack(1, wordSquare, map, result, n);
        }
        System.out.println(result);
        return result;
    }

    private void backtrack(int step, LinkedList<String> wordSquare, Map<String, List<String>> map, List<List<String>> result, int n) {

        if (step == n) {
            result.add(new ArrayList(wordSquare));
            return;
        }
        String prefix = "";
        for (String word : wordSquare) {
            prefix += word.charAt(step);
        }

        List<String> next = map.getOrDefault(prefix, new ArrayList<>());

        for (String nextWord : next) {
            wordSquare.addLast(nextWord);
            backtrack(step + 1, wordSquare, map, result, n);
            wordSquare.removeLast();
        }
    }

    public void buildHashmapForPrefix(String[] words, Map<String, List<String>> map) {


        for (int j = 0; j < words.length; j++) {
            for (int i = 0; i < words[j].length(); i++) {
                String prefix = words[j].substring(0, i);
                List<String> word = map.getOrDefault(prefix, new ArrayList());
                word.add(words[j]);
                map.put(prefix, word);
            }
        }
    }

    public static void main(String s[]) {
        WordSquares ws = new WordSquares();
        String[] words = {"area","lead","wall","lady","ball"};
        ws.wordSquares(words);
    }
}
