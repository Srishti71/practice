package september.week4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordBreak {
    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> wordDictSet = new HashSet(wordDict);
        if(wordDictSet.contains(s)) {
            return true;
        }
        return  dfs(s, 0, wordDictSet, new Boolean[s.length()]);
    }

    private boolean dfs(String s, int start, Set<String> wordDictSet, Boolean[] memo) {

        if(s.length() == start) {
            return true;
        }

        System.out.println(s+" "+ start+" "+memo[start]);
        if(memo[start] != null) {
            return memo[start];
        }
        // og [cat, sand, dog, cats]
        // sandog [cat, sand, dog, cats]  // andog [cat, sand, dog, cats]
        // catsandog [cat, sand, an, dog, cats]
        for(int i =start + 1; i<= s.length(); i++) {
            String curr = s.substring(start, i); // an

            if(wordDictSet.contains(curr) && dfs(s, i, wordDictSet, memo)) {
                return memo[start] = true;
            }
        }
        return memo[start] = false;
    }

    public static void main(String[] args) {
        WordBreak wb = new WordBreak();
        String s= "aaaaaaaaaaaaaaaab";
        List<String> wordDict = Arrays.asList("a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa");
        System.out.println(wb.wordBreak(s, wordDict));
    }

}
