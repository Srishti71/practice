package september.week3;

import java.util.Arrays;
import java.util.PriorityQueue;

public class MinimumCostToMakeAtleastOnePath {

    // go right =
    public int minCost(int[][] grid) {
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        int n = grid.length;
        int m = grid[0].length;

        int[][] cost = new int[n][m];
        for (int[] a : cost)
            Arrays.fill(a, Integer.MAX_VALUE);
        cost[0][0] = 0;
        PriorityQueue<int[]> queue = new PriorityQueue<int[]>((int[] a, int[] b) ->
                cost[a[0]][a[1]] - cost[b[0]][b[1]]);
        queue.add(new int[]{0, 0});

        while (queue.size() > 0) {
            int[] current = queue.poll();
            int freeDir = grid[current[0]][current[1]] - 1;
            for (int i = 0; i < 4; i++) {
                int targetX = current[0] + dirs[i][0];
                int targetY = current[1] + dirs[i][1];
                if (targetX < 0 || targetY < 0 || targetX >= n || targetY >= m) {
                    continue;
                }
                if (i == freeDir && cost[targetX][targetY] > cost[current[0]][current[1]]) {
                    cost[targetX][targetY] = cost[current[0]][current[1]];
                    queue.add(new int[]{targetX, targetY});
                } else if (cost[targetX][targetY] > cost[current[0]][current[1]] + 1) {
                    cost[targetX][targetY] = cost[current[0]][current[1]] + 1;
                    queue.add(new int[]{targetX, targetY});
                }
            }
        }
        System.out.println("Cost: " + cost[n - 1][m - 1]);
        return cost[n - 1][m - 1];
    }

    public static void main(String[] args) {
        MinimumCostToMakeAtleastOnePath minCost = new MinimumCostToMakeAtleastOnePath();
        int[][] grid = {{1, 1, 1, 1},
                {2, 2, 2, 2},
                {1, 1, 1, 1},
                {2, 2, 2, 2}};

        minCost.minCost(grid);
    }
}
