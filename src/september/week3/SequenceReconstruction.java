package september.week3;


import java.util.*;

public class SequenceReconstruction {
    public boolean sequenceReconstruction(int[] org, List<List<Integer>> seqs) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        Map<Integer, Integer> indegree = new HashMap<>();

        buildGraphAndCountIndegree(map, indegree, seqs);
        System.out.println(map + " " + indegree);

        Queue<Integer> queue = new LinkedList();

        for (int node: map.keySet()) {
            if (indegree.get(node) == 0) {
                queue.offer(node);
            }
        }
        int index = 0;

        while (!queue.isEmpty()) {

            if (queue.size() > 1) {
                return false;
            }
            int current = queue.remove();

            if (org[index] != current) {
                return false;
            }
            index++;

            for (int to: map.get(current)) {
                indegree.put(to, indegree.get(to) - 1);
                if (indegree.get(to) == 0) {
                    queue.offer(to);
                }
            }
        }

        return index == org.length;
    }

    public static void main(String[] args) {
        SequenceReconstruction seq = new SequenceReconstruction();
        int[] org = {1, 4, 2, 3};
        List<List<Integer>> seqs = new ArrayList();
        seqs.add(Arrays.asList(1, 2));
        seqs.add(Arrays.asList(1, 3));
        seqs.add(Arrays.asList(2, 3));
        seqs.add(Arrays.asList(4, 2));
        seqs.add(Arrays.asList(1, 4));
        System.out.println("Result: " + seq.sequenceReconstruction(org, seqs));
    }

    private void buildGraphAndCountIndegree(Map<Integer, List<Integer>> graph, Map<Integer, Integer> indegree, List<List<Integer>> seqs) {
        for (List<Integer> seq : seqs) {
            for (int i = 0; i < seq.size(); i++) {
                graph.putIfAbsent(seq.get(i), new ArrayList<>());
                indegree.put(seq.get(i), indegree.getOrDefault(seq.get(i), 0));
                if (i < seq.size() - 1) {
                    // Exists an edge from seq.get(i) to seq.get(i + 1)
                    graph.putIfAbsent(seq.get(i + 1), new ArrayList<>());
                    graph.get(seq.get(i)).add(seq.get(i + 1));
                    indegree.put(seq.get(i + 1), indegree.getOrDefault(seq.get(i + 1), 0) + 1);
                }
            }
        }

        // System.out.println("graph: " + graph);
        // System.out.println("indegree: " + indegree);
    }
}