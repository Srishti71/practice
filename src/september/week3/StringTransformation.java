package september.week3;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StringTransformation {
    Map<Character, Character> map = new HashMap<>();
    Set<Character> charSet = new HashSet<>();

    public boolean canConvert(String str1, String str2) {

        if (str1.length() != str2.length()) return false;

        for (int i = 0; i < str1.length(); i++) {
            char c1 = str1.charAt(i), c2 = str2.charAt(i);
            charSet.add(c2);
            if (map.containsKey(c1)) {
                if (map.get(c1) != c2) return false;
            }
            map.put(c1, c2);
        }
        return charSet.size() < 26 || str1.equals(str2);
    }

    public static void main(String[] args) {
        StringTransformation strTransform = new StringTransformation();
        System.out.println(strTransform.canConvert("abcdefghijklmnopqrstuvwxyz", "bcdefghijklmnopqrstuvwxyza"));
        System.out.println(strTransform.map);
        System.out.println(strTransform.charSet);
    }
}
