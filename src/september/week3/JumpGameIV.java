package september.week3;

import java.util.*;

public class JumpGameIV {
    public int minJumps(int[] arr) {

        if (arr.length < 2) {
            return 0;
        }

        Map<Integer, ArrayList<Integer>> map = new HashMap();
        for (int i = 0; i < arr.length; i++) {
            ArrayList<Integer> list = map.getOrDefault(arr[i], new ArrayList<>());
            list.add(i);
            map.put(arr[i], list);
        }
        // queue holds the value, index, jumps
        Queue<int[]> queue = new LinkedList();
        queue.add(new int[]{arr[0], 0, 0});
        int last = arr.length - 1;
        boolean[] visited = new boolean[arr.length];
        visited[0] = true;

        while (!queue.isEmpty()) {
            int[] current = queue.remove();
            int currValue = current[0];
            int currIndex = current[1];
            int currJump = current[2];

            if (currIndex == last) {
                System.out.println("Result: " + currJump+""+ queue);
                return currJump;
            }

            for (int index : map.get(currValue)) {
                if (!visited[index]) {
                    queue.add(new int[]{currValue, index, currJump + 1});
                    visited[index] = true;
                }
            }

            if (currIndex > 0 && !visited[currIndex - 1]) {
                queue.add(new int[]{arr[currIndex - 1], currIndex - 1, currJump + 1});
                visited[currIndex - 1] = true;
            }
            if (!visited[currIndex + 1]) {
                queue.add(new int[]{arr[currIndex + 1], currIndex + 1, currJump + 1});
                visited[currIndex + 1] = true;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        JumpGameIV jump = new JumpGameIV();
        int[] arr = {-76, 3, 66, -32, 64, 2, -19, -8, -5, -93, 80, -5, -76, -78, 64, 2, 16};
        jump.minJumps(arr);
    }
}