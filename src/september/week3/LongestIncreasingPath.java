package september.week3;

import java.util.Arrays;

public class LongestIncreasingPath {
    int[][] dirs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    public static void main(String[] args) {
        LongestIncreasingPath longestIncreasingPath = new LongestIncreasingPath();
        int[][] matrix = {{3, 4, 5},
                {3, 2, 6},
                {2, 2, 1}};
        longestIncreasingPath.longestIncreasingPath(matrix);
    }

    public int longestIncreasingPath(int[][] matrix) {
        int row = matrix.length;
        if (row == 0) {
            return 0;
        }

        int col = matrix[0].length;
        int maxTotal = 0;
        int[][] cache = new int[row][col];


        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                boolean visited[][] = new boolean[row][col];
                maxTotal = Math.max(maxTotal, longestIncreasingPathUtil(matrix, visited, i, j, cache) + 1);
            }
        }

        System.out.println("Max value: " + maxTotal);
        return maxTotal;
    }

    private int longestIncreasingPathUtil(int[][] matrix, boolean[][] visited, int i, int j, int[][] cache) {
        if (cache[i][j] != 0) {
            return cache[i][j];
        }

        for (int dir[] : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];

            if (isValid(x, y, i, j, matrix, visited)) {

                cache[i][j] = Math.max(cache[i][j], 1 + longestIncreasingPathUtil(matrix, visited, x, y, cache));

            }
        }
        //cache[i][j] = maxMoves + 1;
        return cache[i][j];
    }

    private boolean isValid(int x, int y, int i, int j, int[][] matrix, boolean[][] visited) {
        return x >= 0 && y >= 0 && x < matrix.length && y < matrix[0].length && matrix[x][y] > matrix[i][j];
    }
}

/*
[9,9,4],
[6,6,8],
[2,1,1]

9 -> 9 = 2

4 -> 8
    4 -> 9 -> 9 = 3

6 -> 9 -> 9 = 3
6 -> 6

2 -> 6 -> 9
 */