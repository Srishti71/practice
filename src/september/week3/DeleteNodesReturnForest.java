package september.week3;

import common.TreeNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DeleteNodesReturnForest {

    public static void main(String[] args) {
        DeleteNodesReturnForest deleteNodesReturnForest = new DeleteNodesReturnForest();
        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(3);
        TreeNode node3 = new TreeNode(4);
        TreeNode node4 = new TreeNode(5);
        TreeNode node5 = new TreeNode(6);
        TreeNode node6 = new TreeNode(7);

        root.left = node1;
        root.right = node2;

        node1.left = node3;
        node1.right = node4;


        node2.left = node5;
        node2.right = node6;

        int[] to_delete = {3, 5};
        deleteNodesReturnForest.delNodes(root, to_delete);
    }

    List<TreeNode> result = new ArrayList<>();

    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        Set<Integer> toDeleteSet = new HashSet<>();
        for (int node : to_delete) {
            toDeleteSet.add(node);
        }

        deleteUtils(result, toDeleteSet, root, false);
        System.out.println(result);
        return result;
    }

    private void deleteUtils(List<TreeNode> result, Set<Integer> toDeleteSet, TreeNode root, boolean added) {
        if (root == null) return;
        if (!toDeleteSet.contains(root.val) && !added) {
            result.add(root);
            added = true;

        }

        if (root.left != null && toDeleteSet.contains(root.left.val)) {
            deleteUtils(result, toDeleteSet, root.left, false);
            root.left = null;
        } else {
            deleteUtils(result, toDeleteSet, root.left, added);
        }

        if (root.right != null && toDeleteSet.contains(root.right.val)) {
            deleteUtils(result, toDeleteSet, root.right, false);
            root.right = null;
        } else {
            deleteUtils(result, toDeleteSet, root.right, added);
        }


    }
}

/*
root itself has to be deleted
call method with left as a root and right as a root

delNodes(parent, current, toDelete)
 */