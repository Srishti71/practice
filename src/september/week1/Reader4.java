package september.week1;

public class Reader4 {

    /**
     * @param buf Destination buffer
     * @param n   Number of characters to read
     * @return The number of actual characters read
     */
    public int read(char[] buf, int n) {
        char[] buf4 = new char[4];
        int currentRead = 4;
        int copiedChar = 0;
        while (copiedChar < n && currentRead == 4) {
            currentRead = read4(buf4);

            for (int i = 0; i < currentRead; i++) {
                if (n == copiedChar)
                    return copiedChar;
                buf[copiedChar] = buf4[i];
                copiedChar++;
            }
        }
        return copiedChar;
    }

    private int read4(char[] buf4) {
        return 4;
    }

}
