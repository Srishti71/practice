package september.week1;

import java.math.BigInteger;

/*
https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/554/week-1-september-1st-september-7th/3446/
 */
public class ContainsDuplicate3 {

    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {

        int n = nums.length;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {

                System.out.println("abs: " + Math.abs(nums[i] - nums[j]));
               /* System.out.println(nums[i] + " " + nums[j] + " " + absDiff);
                if (absDiff <= t && (j - i) <= k)
                    return true; */
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ContainsDuplicate3 containsDup = new ContainsDuplicate3();
        System.out.println(containsDup.containsNearbyAlmostDuplicate(new int[]{1, 2, 3, 1}, 3, 0));
        System.out.println(containsDup.containsNearbyAlmostDuplicate(new int[]{1, 0, 1, 1}, 1, 2));
        System.out.println(containsDup.containsNearbyAlmostDuplicate(new int[]{1, 5, 9, 1, 5, 9}, 2, 3));
        System.out.println(containsDup.containsNearbyAlmostDuplicate(new int[]{-2147483648, 2147483647}, 1, 1));

    }
}
