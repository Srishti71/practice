package september.week1;

// https://leetcode.com/problems/binary-tree-level-order-traversal-ii/


import javafx.util.Pair;

import java.util.*;

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class BinaryTreeTraversal2 {

    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList();
        if (root == null) {
            return new ArrayList();
        }
        bfs(root, result);
        return result;
    }

    private void bfs(TreeNode root, List<List<Integer>> result) {
        Queue<Pair<TreeNode, Integer>> queue = new LinkedList<>();

        queue.add(new Pair(root, 0));
        ArrayList<Integer> temp = new ArrayList();
        int tempDepth = -1;

        while (!queue.isEmpty()) {
            Pair<TreeNode, Integer> current = queue.remove();

            TreeNode currentNode = current.getKey();
            int currentDepth = current.getValue();

            if (tempDepth < current.getValue()) {
                if (!temp.isEmpty()) {
                    ArrayList<Integer> array = new ArrayList<>(temp);
                    result.add(0, array);
                }

                temp.clear();

                temp.add(currentNode.val);
                tempDepth = currentDepth;
            } else if (tempDepth == currentDepth) {
                temp.add(currentNode.val);
            }

            if (currentNode.left != null)
                queue.add(new Pair(currentNode.left, currentDepth + 1));
            if (currentNode.right != null) {
                queue.add(new Pair(currentNode.right, currentDepth + 1));

            }
        }
        result.add(0, temp);

        System.out.println(result);
    }

    public static void main(String[] args) {
        BinaryTreeTraversal2 binaryTreeTraversal2 = new BinaryTreeTraversal2();
        TreeNode root = new TreeNode(3);
        TreeNode node1 = new TreeNode(9);
        TreeNode node2 = new TreeNode(20);
        TreeNode node3 = new TreeNode(15);
        TreeNode node4 = new TreeNode(7);

        root.left = node1;
        root.right = node2;

        node2.left = node3;
        node2.right = node4;

        binaryTreeTraversal2.levelOrderBottom(root);

    }
}
