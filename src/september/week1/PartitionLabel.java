package september.week1;

import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/submissions/detail/391744847/

// Since I am computing last occurance of the string all the time-- I could just save it.

public class PartitionLabel {

    public List<Integer> partitionLabels(String S) {

        List<Integer> list = new ArrayList<>();

        int n = S.length();
        int prev = 0;
        for (int i = 0; i < n; i++) {
            int next = S.lastIndexOf(S.charAt(i));

            System.out.println("value: " + next);
            while (i < next && i < n) {
                int last = S.lastIndexOf(S.charAt(i));
                next = Math.max(last, next);
                i++;
            }
            list.add(next + 1 - prev);
            prev = next + 1;
        }
        System.out.println(list);
        return list;
    }

    public static void main(String[] args) {
        PartitionLabel part = new PartitionLabel();
        part.partitionLabels("ababcbacadefegdehijhklij");
    }
}



/*
Input: S = "ababcbacadefegdehijhklij"
Output: [9,7,8]

"ababcbaca", "defegde", "hijhklij".

next = 0
lastOccuranceOf  a  > next

for(int i =0; i< n ;i++) {
next = lastOccurance(str[i]) // 9

while(i <next && i < n) {

lastOccurance(str[i] > next) {

next = lastOccurance(str[i])
i++;
}

res.add(next);
 */