package september.week1;

import java.util.*;

public class Powerset {

    public static void main(String[] args) {
        List<Integer> array = Arrays.asList(1, 2, 3);
        powerset(array);
    }


    public static List<List<Integer>> powerset(List<Integer> array) {
        // Write your code here.

        List<List<Integer>> set = new ArrayList<>();
        powerSetHelper(array, new ArrayList<Integer>(), 0, set);
        System.out.println(set);
        return set;
    }

    private static void powerSetHelper(List<Integer> array, ArrayList<Integer> currentArray, int start, List<List<Integer>> set) {
        set.add(new ArrayList<Integer>(currentArray));

        for (int i = start; i < array.size(); i++) {
            currentArray.add(array.get(i));
            powerSetHelper(array, currentArray, i + 1, set);
            if (currentArray.size() != 0)
                currentArray.remove(currentArray.size() - 1);
        }
    }
}
