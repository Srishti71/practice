package september.week1;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// https://leetcode.com/problems/all-elements-in-two-binary-search-trees/
public class AllElementsOfBinarySearchTree {

    public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {

        Stack<TreeNode> stack1 = new Stack<>();
        Stack<TreeNode> stack2 = new Stack<>();
        List<Integer> result = new ArrayList<>();

        while (root1 != null || root2 != null || !stack1.isEmpty() || !stack2.isEmpty()) {
            while (root1 != null) {
                stack1.push(root1);
                root1 = root1.left;
            }

            while (root2 != null) {
                stack2.push(root2);
                root2 = root2.left;
            }

            if (stack2.isEmpty() || !stack1.isEmpty() && stack1.peek().val < stack2.peek().val) {
                root1 = stack1.pop();
                result.add(root1.val);
                root1 = root1.right;
            } else {
                root2 = stack2.pop();
                result.add(root2.val);
                root2 = root2.right;
            }
        }
        System.out.println(result);
        return result;
    }

    public static void main(String[] args) {
        AllElementsOfBinarySearchTree al = new AllElementsOfBinarySearchTree();
        TreeNode root1 = new TreeNode(2);
        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(3);
        root1.left = node1;
        root1.right = node2;

        TreeNode root2 = new TreeNode(1);
        TreeNode node4 = new TreeNode(0);
        TreeNode node5 = new TreeNode(3);

        root2.left = node4;
        root2.right = node5;

        al.getAllElements(root1, root2);
    }
}
