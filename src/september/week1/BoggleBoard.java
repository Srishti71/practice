package september.week1;

import java.util.*;

/*

Questions to ask - do we use only lower case characters,
Do we have special case characters

 */

class Trie {
    Map<Character, Trie> children = new HashMap<>();
    String word = null;
}

public class BoggleBoard {
    public static List<String> boggleBoard(char[][] board, String[] words) {

        Set<String> wordSet = new HashSet<>();

        for (String w : words) {
            wordSet.add(w);
        }
        System.out.println(wordSet);
        Trie root = formTrie(wordSet);

        Set<String> result = findAllWords(board, root);

        return new ArrayList<String>(result);
    }

    static int[][] direction = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

    private static Set<String> findAllWords(char[][] board, Trie root) {

        int n = board.length;
        int m = board[0].length;
        Trie currentNode = root;

        Set<String> set = new HashSet();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                boolean[][] visited = new boolean[n][m];
                ArrayList<String> words = findWordsStartingAt(board, i, j, currentNode, visited);
                if (words != null) {
                    set.addAll(words);
                }
            }
        }
        return set;

    }

    private static ArrayList<String> findWordsStartingAt(char[][] board, int i, int j, Trie currentNode, boolean[][] visited) {

        if (!isValid(i, j, board, visited)) {
            return null;
        }


        if (!currentNode.children.containsKey(board[i][j])) {
            return null;
        }
        System.out.println(i + " " + j);
        visited[i][j] = true;
        ArrayList<String> wordsFound = new ArrayList<>();
        if (currentNode.children.get(board[i][j]).word != null) {
            wordsFound.add(currentNode.children.get(board[i][j]).word);
        }

        for (int[] dir : direction) {
            ArrayList<String> temp = findWordsStartingAt(board, i + dir[0], j + dir[1], currentNode.children.get(board[i][j]), visited);
            if (temp != null) {
                wordsFound.addAll(temp);
            }
        }

        visited[i][j] = false;
        System.out.println("Words found: " + wordsFound);
        return wordsFound;
    }

    private static boolean isValid(int i, int j, char[][] board, boolean[][] visited) {

        return i >= 0 && i < board.length && j >= 0 && j < board[0].length && !visited[i][j];
    }

    private static Trie formTrie(Set<String> wordSet) {
        Trie root = new Trie();

        for (String word : wordSet) {
            Trie currentNode = root;
            for (int i = 0; i < word.length(); i++) {
                currentNode.children.put(word.charAt(i), new Trie());
                currentNode = currentNode.children.get(word.charAt(i));
            }
            currentNode.word = word;
        }
        return root;
    }

    public static void main(String[] args) {

        char[][] board = {{'t', 'h', 'i', 's', 'i', 's', 'a'},
                {'s', 'i', 'm', 'p', 'l', 'e', 'x'},
                {'b', 'x', 'x', 'x', 'x', 'e', 'b'},
                {'x', 'o', 'g', 'g', 'l', 'x', 'o'},
                {'x', 'x', 'x', 'D', 'T', 'r', 'a'},
                {'R', 'E', 'P', 'E', 'A', 'd', 'x'},
                {'x', 'x', 'x', 'x', 'x', 'x', 'x'},
                {'N', 'O', 'T', 'R', 'E', '-', 'P'},
                {'x', 'x', 'D', 'E', 'T', 'A', 'E'}};

        String[] word = {"this", "is", "not", "a", "simple", "boggle", "board", "test", "REPEATED", "NOTRE-PEATED"};
        boggleBoard(board, word);
    }
}
