package september.week1;

public class InterweavingStrings {

    public static boolean interweavingStrings(String one, String two, String three) {
        // Write your code here.

        boolean res = interweavingStringsHelper(one, two, three, 0, 0, 0, one.length(), two.length(), three.length());
        System.out.println(res);
        return false;
    }

    private static boolean interweavingStringsHelper(String one, String two, String three, int index1, int index2, int index3, int len1, int len2, int len3) {

        if (index3 == len3 && index1 == len1 && index2 == len2) {
            return true;

        }

        if (index1 < len1 && index3 < len3 &&
                one.charAt(index1) == three.charAt(index3)) {
             if(interweavingStringsHelper(one, two, three, index1 + 1, index2, index3 + 1, len1, len2, len3)) return true;

        }

        if (index2 < len2 && index3 < len3 && two.charAt(index2) == three.charAt(index3)) {
            if( interweavingStringsHelper(one, two, three, index1, index2 + 1, index3 + 1, len1, len2, len3)) return true;
        }

        return false;
    }

    public static void main(String[] args) {
        interweavingStrings("algoexpert", "your-dream-job", "your-algodream-expertjob");
    }
}




/*
1=algoexpert
        2=your-dream-job

        3=your-algodream-expertjob


        if(charAt(3)==charAt(1)&&charAt(3)==charAt(2))
        return interweavingStringsHelper(remA,B,remC)||interweavingStringsHelper(A,remB,remC)

        else if(charAt(3)==charAt(1))
        return interweavingStringsHelper(remA,B,remC)
        else if(charAt(3)==charAt(2)))
        interweavingStringsHelper(A,remB,remC)
        else
        return false */