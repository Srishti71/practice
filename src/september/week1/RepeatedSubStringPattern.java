package september.week1;

public class RepeatedSubStringPattern {

    public boolean repeatedSubstringPattern(String s) {

        int len = s.length();

        for (int i = 1; i <= len / 2; i++) {
            String sub = s.substring(0, i);
            String actualStr = s;

            while (!actualStr.isEmpty() && actualStr.startsWith(sub)) {
                actualStr = actualStr.substring(sub.length());
            }
            if (actualStr.isEmpty()) {
                System.out.println(sub);
                return true;
            }

        }
        System.out.println(false);
        return false;
    }

    public static void main(String[] args) {
        RepeatedSubStringPattern pattern = new RepeatedSubStringPattern();
        pattern.repeatedSubstringPattern("abcabcabcabc");
    }
}
