package september.week1;

import java.util.HashSet;
import java.util.Set;

public class LargestTimeforGivenDigits {

    String maxTime = "";

    public String largestTimeFromDigits(int[] A) {
        String str = "";
        for (int i = 0; i < A.length; i++) {
            str += A[i]; // n2
        }
        Set<String> set = new HashSet<>();
        generateAllCombinationOfStr(str, "", set); // n!

        if(maxTime.equals("")) {
            return maxTime;
        }

        String res = maxTime.substring(0, 2) + ":" + maxTime.substring(2);

        return res;
    }

    private void generateAllCombinationOfStr(String str, String currentS, Set<String> set) {
        if (currentS.length() == 4) {
            set.add(currentS);
            if (isValidTime(currentS)) {
                if (maxTime.compareTo(currentS) < 0) {
                    maxTime = currentS;
                }
            }
        }

        for (int i = 0; i < str.length(); i++) {
            String rem = str.substring(0, i) + str.substring(i + 1); // 1234
            generateAllCombinationOfStr(rem, currentS + str.charAt(i), set);
        }
    }

    private boolean isValidTime(String currentS) {
        String hour = currentS.substring(0, 2);
        String minutes = currentS.substring(2);

        return Integer.parseInt(hour) < 24 && Integer.parseInt(minutes) < 60;
    }

    public static void main(String[] args) {
        LargestTimeforGivenDigits largest = new LargestTimeforGivenDigits();
        int[] A = {5, 5, 5, 5};
        largest.largestTimeFromDigits(A);
    }
}
