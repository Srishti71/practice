package september.week2;

import java.util.ArrayList;
import java.util.List;

public class Bridges {

    int[] parent;
    boolean[] visited;
    int[] discovery;
    int[] low;
    int time;

    public void bridgeUtil(List<Integer>[] connection, int n) {
        parent = new int[n];
        visited = new boolean[n];
        discovery = new int[n];
        low = new int[n];

        dfs(0, connection);

    }

    private void dfs(int currentNode, List<Integer>[] connection) {

        visited[currentNode] = true;
        low[currentNode] = discovery[currentNode] = time++;

        for (int next : connection[currentNode]) {
            if (!visited[next]) {
                parent[next] = currentNode;
                dfs(next, connection);

                low[currentNode] = Math.min(low[next], low[currentNode]);

                if (low[next] > discovery[currentNode]) {
                    System.out.println(currentNode + " - " + next);
                }
            } else if (parent[currentNode] != next) {
                low[currentNode] = Math.min(low[currentNode], discovery[next]);
            }
        }
    }

    public static void main(String[] args) {
        int n = 6;

        List<Integer>[] connection = new ArrayList[n];

        ArrayList<Integer> list1 = new ArrayList();
        list1.add(1);
        list1.add(2);
        connection[0] = list1;

        ArrayList<Integer> list2 = new ArrayList();
        list2.add(0);
        list2.add(2);
        list2.add(3);
        connection[1] = list2;

        ArrayList<Integer> list3 = new ArrayList();
        list3.add(0);
        list3.add(1);
        connection[2] = list3;

        ArrayList<Integer> list4 = new ArrayList();
        list4.add(1);
        list4.add(4);
        list4.add(5);
        connection[3] = list4;

        ArrayList<Integer> list5 = new ArrayList();
        list5.add(3);
        list5.add(5);
        connection[4] = list5;

        ArrayList<Integer> list6 = new ArrayList();
        list6.add(3);
        list6.add(4);
        connection[5] = list6;

        Bridges critical = new Bridges();
        critical.bridgeUtil(connection, n);
    }
}
