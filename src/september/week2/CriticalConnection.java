package september.week2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CriticalConnection {

    int[] low;
    int[] parent;
    int discovery[];
    int time = 0;

    public void findCriticalConnection(int n, List<Integer>[] connection) {

        low = new int[n];
        Arrays.fill(low, Integer.MAX_VALUE);
        parent = new int[n];
        Arrays.fill(parent, -1);

        discovery = new int[n];
        boolean visited[] = new boolean[n];
        dfs(0, visited, connection);
    }

    private void dfs(int currentNode, boolean[] visited, List<Integer>[] connection) {
        low[currentNode] = discovery[currentNode] = time++;
        int child = 0;
        visited[currentNode] = true;
        for (int next : connection[currentNode]) {
            if (!visited[next]) {
                child++;
                parent[next] = currentNode;
                dfs(next, visited, connection);
                low[currentNode] = Math.min(low[next], low[currentNode]);


                if (parent[currentNode] == -1 && child > 1)
                    System.out.println("Current Node: " + currentNode);

                if (parent[currentNode] != -1 && low[next] >= discovery[currentNode])
                    System.out.println("Current Node: " + currentNode);


            } else if (next != parent[currentNode]) {
                low[currentNode] = Math.min(low[currentNode], discovery[next]);
            }

        }
    }

    public static void main(String[] args) {
        int n = 6;

        List<Integer>[] connection = new ArrayList[n];

        ArrayList<Integer> list1 = new ArrayList();
        list1.add(1);
        list1.add(2);
        connection[0] = list1;

        ArrayList<Integer> list2 = new ArrayList();
        list2.add(0);
        list2.add(2);
        list2.add(3);
        connection[1] = list2;

        ArrayList<Integer> list3 = new ArrayList();
        list3.add(0);
        list3.add(1);
        connection[2] = list3;

        ArrayList<Integer> list4 = new ArrayList();
        list4.add(1);
        list4.add(4);
        list4.add(5);
        connection[3] = list4;

        ArrayList<Integer> list5 = new ArrayList();
        list5.add(3);
        list5.add(5);
        connection[4] = list5;

        ArrayList<Integer> list6 = new ArrayList();
        list6.add(3);
        list6.add(4);
        connection[5] = list6;

        CriticalConnection critical = new CriticalConnection();
        critical.findCriticalConnection(n, connection);
    }
}
