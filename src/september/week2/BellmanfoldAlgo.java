package september.week2;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BellmanfoldAlgo {

    public void findShortestPath(ArrayList<Pair<Integer, Integer>>[] connection) {

        int vertices = connection.length;
        int[] dist = new int[vertices];

        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[0] = 0;

        for (int i = 0; i < vertices; i++) {

            for (Pair<Integer, Integer> connect : connection[i]) {

                int e = connect.getKey();
                int w = connect.getValue();

                if (dist[i] != Integer.MAX_VALUE && dist[i] + w < dist[e]) {
                    dist[e] = dist[i] + w;
                }
            }
        }

        for(int value: dist) {
            System.out.println(value);
        }
    }

    public static void main(String[] args) {
        int n = 5;

        ArrayList<Pair<Integer, Integer>>[] connection = new ArrayList[n];

        ArrayList<Pair<Integer, Integer>> list1 = new ArrayList();
        list1.add(new Pair(1, 1));
        list1.add(new Pair(2, 4));
        connection[0] = list1;

        ArrayList<Pair<Integer, Integer>> list2 = new ArrayList();
        list2.add(new Pair(2, 3));
        list2.add(new Pair(4, 2));
        list2.add(new Pair(3, 2));
        connection[1] = list2;

        ArrayList<Pair<Integer, Integer>> list3 = new ArrayList();
        connection[2] = list3;

        ArrayList<Pair<Integer, Integer>> list4 = new ArrayList();
        list4.add(new Pair(1, 1));
        connection[3] = list4;

        ArrayList<Pair<Integer, Integer>> list5 = new ArrayList();
        list5.add(new Pair(3, 3));
        connection[4] = list5;

        BellmanfoldAlgo fold = new BellmanfoldAlgo();
        fold.findShortestPath(connection);
    }
}
