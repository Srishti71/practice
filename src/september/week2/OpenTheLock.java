package september.week2;

import java.util.HashSet;

import java.util.Set;

public class OpenTheLock {
    public int openLock(String[] deadends, String target) {
        Set<String> deadendsSet = new HashSet<>();

        for (String word : deadends) {
            deadendsSet.add(word);
        }
        Set<String> visited = new HashSet<>();
        int result = dfs(deadendsSet, target, "0000", 0, visited);

        return result;
    }

    private int dfs(Set<String> deadends, String target, String currentString, int step, Set<String> visited) {

        if (deadends.contains(currentString) || visited.contains(currentString)) {
            return Integer.MAX_VALUE;
        }

        if (target.equals(currentString)) {
            System.out.println("Result: " + step);
            return step;
        }
        System.out.println(currentString);
        visited.add(currentString);

        int result = Integer.MAX_VALUE;
        for (int i = 0; i < currentString.length(); i++) {
            int elementInt = Character.getNumericValue(currentString.charAt(i));
            int left = 0, right = 0;
            if (elementInt == 0) {
                right = 1;
                left = 9;
            } else if (elementInt == 9) {
                right = 0;
                left = 8;
            } else {
                left = elementInt - 1;
                right = elementInt + 1;
            }

            String str1 = currentString.substring(0, i) + left + currentString.substring(i + 1);

            int result0 = dfs(deadends, target, currentString, step, visited);
            int result1 = dfs(deadends, target, str1, step + 1, visited);

            int result2 = dfs(deadends, target, currentString.substring(0, i) + right + currentString.substring(i + 1), step + 1, visited);

            int res = Math.min(result1, result2);
            result = Math.min(res, result);
        }

        // System.out.println("Result: " + result);

        //visited.remove(currentString);
        return result;
    }

    public static void main(String[] args) {
        OpenTheLock open = new OpenTheLock();
        String deadends[] = {"0201", "0101", "0102", "1212", "2002"};
        open.openLock(deadends, "0202");
    }
}
