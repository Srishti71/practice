package september.week2;

public class FloydWarshall {

    public void findShortestPathToAll(int[][] connection) {
        int v = connection.length;

        int[][] dist = new int[v][v];

        for (int i = 0; i < v; i++) {
            for (int j = 0; j < v; j++) {
                dist[i][j] = connection[i][j];
            }
        }

        for (int k = 0; k < v; k++) {
            for (int i = 0; i < v; i++) {
                for (int j = 0; j < v; j++) {
                    if (dist[i][k] + dist[k][j] < dist[i][j])
                        dist[i][j] = dist[i][k] + dist[k][j];
                }
            }
        }

        for (int i = 0; i < v; i++) {
            for (int j = 0; j < v; j++) {
                System.out.println(dist[i][j]);
            }
        }
    }

    public static void main(String[] args) {
        FloydWarshall warshall = new FloydWarshall();
        int INF = 99999;
        int[][] connection = {{0, 5, INF, 10},
                {INF, 0, 3, INF},
                {INF, INF, 0, 1},
                {INF, INF, INF, 0}};
        warshall.findShortestPathToAll(connection);
    }
}
