package september.week2;

import java.util.*;

public class EmployInform {

    public int numOfMinutes(int n, int headID, int[] manager, int[] informTime) {
        Map<Integer, ArrayList<Integer>> managerMap = new HashMap<>();

        buildTree(manager, managerMap);

        System.out.println(managerMap);

        System.out.println("Inform time: " + bfs(managerMap, informTime, headID));
        return 0;
    }

    private int bfs(Map<Integer, ArrayList<Integer>> managerMap, int[] informTime, int headID) {

        int total = 0;

        Queue<int[]> queue = new LinkedList<>();
        // 1st = employeeId,. time to inform
        queue.add(new int[]{managerMap.get(-1).get(0), 0});

        while (!queue.isEmpty()) {
            int[] currentElement = queue.remove();

            if (managerMap.get(currentElement[0]) == null) {
                total = Math.max(total, currentElement[1]);
                continue;
            }
            for (int employee : managerMap.get(currentElement[0])) {
                queue.add(new int[]{employee, currentElement[1] + informTime[currentElement[0]]});
            }

        }

        return total;
    }

    private void buildTree(int[] manager, Map<Integer, ArrayList<Integer>> map) {

        int index = 0;
        for (int employee : manager) {
            ArrayList<Integer> list = map.getOrDefault(employee, new ArrayList());
            list.add(index++);
            map.put(employee, list);
        }
    }

    public static void main(String[] args) {
        EmployInform employeeInfo = new EmployInform();
        employeeInfo.numOfMinutes(6, 2, new int[]{2, 2, -1, 2, 2, 2},
                new int[]{0, 0, 1, 0, 0, 0});

    }
}
