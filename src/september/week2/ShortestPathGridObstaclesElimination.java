package september.week2;


import java.util.LinkedList;
import java.util.Queue;

public class ShortestPathGridObstaclesElimination {

    public int shortestPath(int[][] grid, int k) {
        // 4 entires = i, j, count, remObstacles
        Queue<int[]> queue = new LinkedList<>();

        queue.add(new int[]{0, 0, 0, k});
        int[][] directions = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
        int row = grid.length, col = grid[0].length;
        boolean visited[][][] = new boolean[row][col][k + 1];

        int target[] = new int[]{row - 1, col - 1};
        visited[0][0][k] = true;
        int result = Integer.MAX_VALUE;

        while (!queue.isEmpty()) {
            int[] currentEntry = queue.remove();
            if (currentEntry[0] == target[0] && currentEntry[1] == target[1]) {
                result = Math.min(result, currentEntry[2]);
                System.out.println(result);
                continue;
            }

            for (int[] dir : directions) {
                int newRow = currentEntry[0] + dir[0];
                int newCol = currentEntry[1] + dir[1];

                if (isValid(newRow, newCol, row, col)) {
                    int isObstacle = grid[newRow][newCol];
                    if (isObstacle == 1 && currentEntry[3] > 0 && !visited[newRow][newCol][currentEntry[3] - 1]) {
                        System.out.println("Inside: " + newRow + " " + newCol);
                        queue.add(new int[]{newRow, newCol, currentEntry[2] + 1, currentEntry[3] - 1});
                        visited[newRow][newCol][currentEntry[3] - 1] = true;
                    } else if (isObstacle == 0 && !visited[newRow][newCol][currentEntry[3]]) {
                        queue.add(new int[]{newRow, newCol, currentEntry[2] + 1, currentEntry[3]});
                        visited[newRow][newCol][currentEntry[3]] = true;
                    }
                }
            }
        }

        return result == Integer.MAX_VALUE ? -1 : result;
    }

    private boolean isValid(int newRow, int newCol, int row, int col) {

        if (newCol >= 0 && newRow >= 0 && newRow < row && newCol < col) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        ShortestPathGridObstaclesElimination path = new ShortestPathGridObstaclesElimination();
        int[][] grid = {{0, 1, 1},
                {1, 1, 1},
                {1, 0, 0}};
        path.shortestPath(grid, 1);
    }
}
