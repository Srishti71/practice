package september.week2;

import java.util.Stack;

public class StringDecode {

    public String decodeString(String s) {
        if (s == null) {
            return null;
        }

        Stack<Integer> numStack = new Stack();
        Stack<String> charStack = new Stack();

        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);

            if (isNumeric(currentChar)) {
                int num = 0; // 10
                while (i < s.length() && Character.isDigit(s.charAt(i))) {
                    num = num * 10 + (s.charAt(i)-'0');
                    i++;
                }
                i--;
                numStack.add(num);
            } else if (currentChar != ']') {
                charStack.add(currentChar + "");
            } else {
                String temp = "";
                int tempNum = numStack.pop();
                while (!charStack.isEmpty() && !charStack.peek().equals("[")) {
                    temp = charStack.pop() + temp;
                }
                charStack.pop();
                StringBuilder actual = new StringBuilder();
                for (int j = 0; j < tempNum; j++) {
                    actual.append(temp);
                }
                charStack.add(new String(actual));
            }
        }

        StringBuilder res = new StringBuilder();
        while (!charStack.isEmpty()) {
            res.insert(0, charStack.pop());
        }
        String result = new String(res);
        System.out.println("Result" + res);
        return result;
    }

    public boolean isNumeric(char ch) {
        return Character.isDigit(ch);
    }

    public static void main(String[] args) {
        StringDecode decode = new StringDecode();
        decode.decodeString("100[leetcode]");
    }
}
