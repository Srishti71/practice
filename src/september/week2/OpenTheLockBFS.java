package september.week2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

class Locks {
    String locksPosition;
    int turns;

    Locks(String locksPosition, int turns) {
        this.locksPosition = locksPosition;
        this.turns = turns;
    }
}

public class OpenTheLockBFS {
    public int openLock(String[] deadends, String target) {
        Queue<Locks> queue = new LinkedList<>();

        queue.add(new Locks("0000", 0));
        Set<String> visited = new HashSet<>();
        for (String str : deadends) {
            visited.add(str);
        }
        visited.add("0000");

        while (!queue.isEmpty()) {
            Locks currentPosition = queue.remove();
            String currentLock = currentPosition.locksPosition;
            if (currentLock.equals(target)) {
                System.out.println("Result: " + currentPosition.turns);
                return currentPosition.turns;
            }

            for (int i = 0; i < currentLock.length(); i++) {
                int currentNumber = Character.getNumericValue(currentLock.charAt(i));
                int up, down;
                if (currentNumber == 9) {
                    up = 0;
                    down = 8;
                } else if (currentNumber == 0) {
                    up = 1;
                    down = 9;
                } else {
                    up = currentNumber + 1;
                    down = currentNumber - 1;
                }

                String newStrUp = currentLock.substring(0, i) + up + currentLock.substring(i + 1);
                String newStrDown = currentLock.substring(0, i) + down + currentLock.substring(i + 1);
                if (!visited.contains(newStrUp)) {
                    queue.add(new Locks(newStrUp, currentPosition.turns + 1));
                    visited.add(newStrUp);
                }

                if (!visited.contains(newStrDown)) {
                    queue.add(new Locks(newStrDown, currentPosition.turns + 1));
                    visited.add(newStrDown);
                }

            }
        }
        return -1;
    }

    public static void main(String[] args) {
        OpenTheLockBFS openTheLockBFS = new OpenTheLockBFS();
        // "8887","8889","8878","8898","8788","8988","7888","9888"], target = "8888"
        String deadends[] = {"8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888"};
        openTheLockBFS.openLock(deadends, "8888");
    }
}
