public class SnakeLadder {

    int minMoves = Integer.MAX_VALUE;

    public int snakesAndLadders(int[][] board) {

        int row = board.length;
        int col = board[0].length;

        int dest = row * col;

        dfs(board, 1, 0, dest);
        return minMoves;
    }

    private void dfs(int[][] board, int currentLocation, int moves, int dest) {

        int totalCol = board[0].length;
        int totalRow = board.length;
        int row = (currentLocation - 1) / totalCol;
        int col = (currentLocation - 1) % totalCol;
        col = row % 2 == 0 ? col : totalCol - 1 - col;
        int actualRow = totalRow - 1 - row;

        if (actualRow < totalRow && actualRow >= 0 && col < totalCol && col >= 0 && currentLocation <= totalCol * totalRow) {
            if (board[actualRow][col] != -1) {
                currentLocation = board[actualRow][col];
            }
            if (currentLocation == dest) {
                minMoves = Math.min(minMoves, moves);
                System.out.println("Moves: " + moves);
            }
            dfs(board, currentLocation + 1, moves + 1, dest);
            dfs(board, currentLocation + 2, moves + 1, dest);
            dfs(board, currentLocation + 3, moves + 1, dest);
            dfs(board, currentLocation + 4, moves + 1, dest);
            dfs(board, currentLocation + 5, moves + 1, dest);
            dfs(board, currentLocation + 6, moves + 1, dest);
        }


    }

    public static void main(String s[]) {
        SnakeLadder snakeLadder = new SnakeLadder();
        int[][] board = {
                {-1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, 35, -1, -1, 13, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, 15, -1, -1, -1, -1}
        };

        snakeLadder.snakesAndLadders(board);
    }
}
