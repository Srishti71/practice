package March_7;

public class MaximalSquare {

    public int maximalSquare(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;

        if (m == 0 || n == 0)
            return 0;
        if (m == 1) {
            for (int i = 0; i < n; i++) {
                if (matrix[0][i] == 1) {
                    return 1;
                }
            }
            return 0;
        }

        if (n == 1) {
            for (int i = 0; i < m; i++) {
                if (matrix[i][0] == 1) {
                    return 1;
                }
            }
            return 0;
        }
        int[][] newMat = new int[m][n];
        int ans = Integer.MIN_VALUE;
        for (int i = 0; i < m; i++) {
            newMat[i][0] = matrix[i][0];
        }

        for (int i = 0; i < n; i++) {
            newMat[0][i] = matrix[0][i];
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if(matrix[i][j] == 0){
                    newMat[i][j] = 0;
                } else {

                    newMat[i][j] = Math.min(Math.min(newMat[i - 1][j], newMat[i][j - 1]), newMat[i - 1][j - 1]) + (matrix[i][j]);
                }
                ans = Math.max(ans, newMat[i][j]);
            }
        }
        if (ans == Integer.MIN_VALUE)
            return 0;
        return ans * ans;
    }

    public static void main(String s[]) {
        MaximalSquare maximalSquare = new MaximalSquare();
        int[][] matrix = {{1, 0, 1, 1, 0, 1},
                          {1, 1, 1, 1, 1, 1},
                          {0, 1, 1, 0, 1, 1},
                          {1, 1, 1, 0, 1, 0},
                          {0, 1, 1, 1, 1, 1},
                          {1, 1, 0, 1, 1, 1}};
        System.out.println(maximalSquare.maximalSquare(matrix));
    }
}
