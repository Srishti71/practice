package March_7;

import java.util.ArrayList;
import java.util.List;

public class TextJustification {
    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> mainList = new ArrayList<>();

        int currLenWord = 0;
        int extra = 0;
        List<String> templist = new ArrayList<>();
        int i = 0;
        while (i < words.length) {
            if (currLenWord + templist.size() + words[i].length() <= maxWidth) {
                templist.add(words[i]);
                currLenWord += words[i].length();
                i++;
            } else {
                extra = maxWidth - currLenWord - (templist.size() - 1);
                if (templist.size() == 1) {
                    String last = createSentenceWithAWord(templist, maxWidth);

                    if (last != null) {
                        mainList.add(last);
                    }
                } else {
                    mainList.add(createSentenceWithSpace(extra, templist));
                }

                currLenWord = 0;
                templist.clear();
            }
        }


        String last = createSentenceWithAWord(templist, maxWidth);

        if (last != null)
            mainList.add(last);

        System.out.println(mainList);
        return mainList;
    }

    private String createSentenceWithAWord(List<String> templist, int maxWidth) {
        if (!templist.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (String e : templist) {
                sb.append(e);
                sb.append(" ");
            }
            for (int j = sb.length(); j < maxWidth; j++) {
                sb.append(" ");
            }
            return sb.toString();
        }
        return null;
    }

    private String createSentenceWithSpace(int extra, List<String> templist) {

        int listLen = templist.size();
        int rem = extra % (listLen - 1);
        int com = extra / (listLen - 1);

        System.out.println(templist + " " + rem + " " + com);

        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < listLen - 1; j++) {
            sb.append(templist.get(j));
            sb.append(" ");
            for (int i = 0; i < com; i++)
                sb.append(" ");
            if (rem > 0) {
                sb.append(" ");
                rem--;
            }
        }
        sb.append(templist.get(listLen - 1));
        return sb.toString();
    }

    public static void main(String s[]) {
        String words[] = {"ask","not","what","your","country","can","do","for","you","ask","what","you","can","do","for","your","country"};
        TextJustification justification = new TextJustification();
        int maxWidth = 16;
        justification.fullJustify(words, maxWidth);
    }
}
