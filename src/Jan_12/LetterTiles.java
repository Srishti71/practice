package Jan_12;

import java.util.HashSet;
import java.util.Set;

public class LetterTiles {

    static int count = 0;
    static Set<String> set = new HashSet();

    public int numTilePossibilities(String tiles, String curr) {
        int res = 0;
        for (int i = 0; i < tiles.length(); i++) {

            String result = curr + tiles.charAt(i);
            if (set.contains(result)) continue;
            set.add(result);
            res += numTilePossibilities(tiles.substring(0, i) + tiles.substring(i + 1), curr + tiles.charAt(i)) + 1;

        }
        return res;
    }

    public static void main(String s[]) {

        LetterTiles letterTiles = new LetterTiles();
        System.out.print(letterTiles.numTilePossibilities("AAABBC", ""));

    }

}
