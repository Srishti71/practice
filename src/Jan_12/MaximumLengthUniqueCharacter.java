package Jan_12;

/*
    https://leetcode.com/problems/maximum-length-of-a-concatenated-string-with-unique-characters/
 */

import java.util.ArrayList;
import java.util.List;

public class MaximumLengthUniqueCharacter {

    //  arr = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p"]
    public static void main(String s[]) {
        MaximumLengthUniqueCharacter maximumLengthUniqueCharacter = new MaximumLengthUniqueCharacter();
        List<String> list = new ArrayList<>();

        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        System.out.print("Max length: " + maximumLengthUniqueCharacter.maxLength(list));
    }

    int maxLength = Integer.MIN_VALUE;

    public int maxLength(List<String> arr) {
        if (arr.size() == 1) {
            return arr.get(0).length();
        }
        return maxLengthHelper(arr, "", 0, 0);
    }

    public int maxLengthHelper(List<String> arr, String formedWord, int index, int length) {
        if (index == arr.size()) {
            return length;
        }
        String current = arr.get(index);
        int take = 0;
        if (isUnique(formedWord, current))
            take = maxLengthHelper(arr, formedWord + current, index + 1, length + current.length());

        maxLength = Math.max(maxLength, Math.max(take, maxLengthHelper(arr, formedWord, index + 1, length)));
        return maxLength;
    }

    private boolean isUnique(String s, String s1) {

        int[] uniqueness = new int[26];

        for (int i = 0; i < s.length(); i++) {
            int curr = s.charAt(i) - 'a';
            if (uniqueness[curr] != 0)
                return false;
            uniqueness[curr] = 1;
        }

        for (int i = 0; i < s1.length(); i++) {
            int curr = s1.charAt(i) - 'a';
            if (uniqueness[curr] != 0)
                return false;
            uniqueness[curr] = 1;
        }
        return true;
    }
}
