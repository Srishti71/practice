package Jan_12;

public class MaxGoldPath {

    int maxAmount = Integer.MIN_VALUE;

    public int getMaximumGold(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                maxAmount = Math.max(maxAmount, getMaximumGoldAmountStartfrom(i, j, grid, new boolean[m][n]));
            }
        }
        return maxAmount;
    }

    private int getMaximumGoldAmountStartfrom(int i, int j, int[][] grid, boolean[][] visited) {

        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || grid[i][j] == 0 || visited[i][j]) {
            return 0;
        }
        visited[i][j] = true;
        int up = getMaximumGoldAmountStartfrom(i - 1, j, grid, visited);

        int down = getMaximumGoldAmountStartfrom(i + 1, j, grid, visited);
        int left = getMaximumGoldAmountStartfrom(i, j - 1, grid, visited);
        int right = getMaximumGoldAmountStartfrom(i, j + 1, grid, visited);

        int value = Math.max(Math.max(Math.max(up, left), right), down) + grid[i][j];
        visited[i][j] = false;
        return value;
    }

    public static void main(String s[]) {

        MaxGoldPath maxGoldPath = new MaxGoldPath();
        int[][] grid = {{1, 0, 7, 0, 0, 0}, {2, 0, 6, 0, 1, 0}, {3, 5, 6, 7, 4, 2}, {4, 3, 1, 0, 2, 0}, {3, 0, 5, 0, 20, 0}};
        maxGoldPath.getMaximumGold(grid);
        System.out.print("Max Amount: " + maxGoldPath.maxAmount);
    }

}
