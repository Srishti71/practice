package March_12;

import java.util.ArrayList;

public class SplitArrayLargestSum {

    int sum = Integer.MAX_VALUE;

    public int splitArray(int[] nums, int m) {
        ArrayList<Integer> index = new ArrayList<>();
        index.add(0);
        splitArrays(nums, index, 1, m, 1);
        return sum;
    }

    private void splitArrays(int[] nums, ArrayList<Integer> indexes, int index, int target, int ind) {
        if (target == indexes.size()) {
            int currentSum = calculateSum(indexes, nums);
            sum = Math.min(currentSum, sum);
            return;
        }

        if (index >= nums.length)
            return;

        for (int i = index; i < nums.length; i++) {
            indexes.add(i);
            splitArrays(nums, indexes, i + 1, target, ind + 1);
            indexes.remove(indexes.size() - 1);
        }
    }

    private int calculateSum(ArrayList<Integer> indexes, int[] nums) {
        int localMax = Integer.MIN_VALUE;
        for (int i = 0; i < indexes.size() - 1; i++) {
            int sum = 0;

            int start = indexes.get(i);
            int end = indexes.get(i + 1);
            for (int j = start; j < end; j++) {
                sum += nums[j];
            }
            localMax = Math.max(sum, localMax);
        }
        int sum = 0;
        for (int i = indexes.get(indexes.size() - 1); i < nums.length; i++) {
            sum += nums[i];
        }
        localMax = Math.max(sum, localMax);

        if (localMax == Integer.MIN_VALUE)
            return 0;

        return localMax;
    }

    public static void main(String s[]) {
        SplitArrayLargestSum splitArrayLargestSum = new SplitArrayLargestSum();
        int nums[] = {7, 2, 5, 10, 8};
        splitArrayLargestSum.splitArray(nums, 3);
    }
}