package March_8;

import java.util.ArrayList;
import java.util.List;

// https://leetcode.com/problems/encode-and-decode-strings/
public class EncodeAndDecodeString {

    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {

        StringBuilder sb = new StringBuilder();

        for (String element : strs) {
            sb.append(intToString(element.length()));
            sb.append(element);
        }
        return sb.toString();
    }

    private String intToString(int length) {
        char[] bytes = new char[4];
        for (int i = 3; i > -1; i--) {
            bytes[3 - i] = (char) (length >> (i * 8) & 0xff);
        }
        return new String(bytes);
    }

    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {

        int i = 0;
        List<String> list = new ArrayList();

        while (i < s.length()) {
            int length = getIntFromString(s.substring(i, i + 4));
            i = i + 4;
            list.add(s.substring(i, i + length));
            i += length;
        }
        return list;
    }

    private int getIntFromString(String substring) {
        int num = 0;
        for (int i = 0; i < substring.length(); i++) {
            num = (num << 8) + (int) substring.charAt(i);
        }
        return num;
    }

    public static void main(String s[]) {
        EncodeAndDecodeString encodeAndDecodeString = new EncodeAndDecodeString();
        List<String> list = new ArrayList<>();
        encodeAndDecodeString.encode(list);
    }
}
