package March_8;

import common.TreeNode;

public class CompleteTree {

    int count = 0;

    public int countNodes(TreeNode root) {
        performCount(root);
        return count;
    }

    private void performCount(TreeNode root) {
        if (root == null) {
            return;
        }

        count++;
        performCount(root.left);
        performCount(root.right);
    }

    public static void main(String s[]) {

        CompleteTree completeTree = new CompleteTree();
        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(3);
        TreeNode node3 = new TreeNode(4);
        TreeNode node4 = new TreeNode(5);
        TreeNode node5 = new TreeNode(6);

        root.left = node1;
        root.right = node2;
        node1.left = node3;
        node1.right = node4;

        node2.left = node5;
        System.out.println(completeTree.countNodes(root));

    }
}
