package Jan_30;

import java.lang.reflect.Array;
import java.util.*;

public class CriticalConnectionNetwork {
    // take one connection source = 0, destination = 1
    // find out if there is a way to reach from 0 to 1 or not, if not then add it to critical list
    // dfs that

    // Question: can there be cycle?

    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        HashSet<List<Integer>> visitedSet = new HashSet<>();

        Map<Integer, List<Integer>> map = generateData(n, connections);
        List<List<Integer>> criticalPath = new ArrayList<>();

        for (int i = 0; i < n; i++) {

            int source = connections.get(i).get(0);
            int destination = connections.get(i).get(1);
            visitedSet.clear();

            visitedSet.add(Arrays.asList(source, destination));
            visitedSet.add(Arrays.asList(destination, source));

            if(!findPathFromSourceToDestination(source, destination, map, visitedSet)) {
                criticalPath.add(Arrays.asList(source, destination));
            }
        }

        return criticalPath;
    }

    private boolean findPathFromSourceToDestination(int source, int destination, Map<Integer, List<Integer>> map, HashSet<List<Integer>> set) {

        Stack<Integer> stack = new Stack<>();
        List<Integer> list = map.getOrDefault(source, new ArrayList<>());
        set.add(Arrays.asList(source, destination));
        for( int i = 0; i< list.size(); i++) {
            if(list.get(i) != destination) {
                stack.add(list.get(i));
            }

        }

        while(!stack.isEmpty()) {

            int key = stack.pop();
            if(key == destination) {

            }

        }
        return false;

    }

    public Map<Integer, List<Integer>> generateData(int n, List<List<Integer>> connections) {
        Map<Integer, List<Integer>> map = new HashMap();

        for (int i = 0; i < n; i++) {
            List<Integer> list = map.getOrDefault(connections.get(i).get(0), new ArrayList<Integer>());
            list.add(connections.get(i).get(1));
            map.put(connections.get(i).get(0), list);

            List<Integer> reverseList = map.getOrDefault(connections.get(i).get(1), new ArrayList<Integer>());
            reverseList.add(connections.get(i).get(0));
            map.put(connections.get(i).get(1), list);
        }
        return map;

    }
}
