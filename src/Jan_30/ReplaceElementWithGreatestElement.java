package Jan_30;

public class ReplaceElementWithGreatestElement {

    public int[] replaceElements(int[] arr) {

        if (arr.length <= 1) {
            return arr;
        }

        int len = arr.length;
        int maxSoFar = Math.max(arr[len - 1], arr[len - 2]);

        arr[len - 2] = arr[len - 1];
        arr[len - 1] = -1;

        for (int i = arr.length - 3; i >= 0; i--) {
            int temp = maxSoFar;
            maxSoFar = Math.max(arr[i], maxSoFar);
            arr[i] = temp;
        }
        return arr;
    }

    public static void main(String s[]) {
        ReplaceElementWithGreatestElement replaceElementWithGreatestElement = new ReplaceElementWithGreatestElement();
        int arr[] = {17, 18, 5, 4, 6, 1};
        int newArr[] = replaceElementWithGreatestElement.replaceElements(arr);
        replaceElementWithGreatestElement.printElement(newArr);
    }

    void printElement(int[] ele) {
        for (int i = 0; i < ele.length; i++) {
            System.out.println(ele[i]);
        }
    }
}
