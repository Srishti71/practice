package April_26;

public class TreasureIsland {
    int min = Integer.MAX_VALUE;

    public void findMinPathToIsland(int[][] grid) {
        dfs(grid, 0, 0, 0);
        System.out.println(min);

    }

    private void dfs(int[][] grid, int row, int col, int count) {

        if (row >= 0 && col >= 0 && row < grid.length && col < grid[0].length && grid[row][col] != 1) {

            if (grid[row][col] == 2) {
                min = Math.min(min, count);
                return;
            }

            int curr = grid[row][col];
            grid[row][col] = 1;

            dfs(grid, row + 1, col, count + 1);
            dfs(grid, row - 1, col, count + 1);
            dfs(grid, row, col + 1, count + 1);
            dfs(grid, row, col - 1, count + 1);
            grid[row][col] = curr;
        }
    }

    public static void main(String s[]) {
        TreasureIsland treasure = new TreasureIsland();
        int[][] grid = {{0, 0, 0, 0},
                {1, 0, 0, 0},
                {0, 0, 0, 0},
                {2, 0, 1, 0}};
        //treasure.findMinPathToIsland(grid);
        treasure.stringValue("baba");
    }


    public void stringValue(String s) {
        int n = s.length();
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                System.out.println(s.substring(i, j));
            }
        }
    }
}
