package common;

import java.util.ArrayList;

public class Trie {

    public ArrayList<String> words;
    public Trie[] children;
    public Trie() {
        words = new ArrayList<>();
        children = new Trie[26] ;
    }
}
