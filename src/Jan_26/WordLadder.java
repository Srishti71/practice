package Jan_26;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Pairs {
    String word;
    int count;

    Pairs(String word, int count) {
        this.count = count;
        this.word = word;
    }
}

public class WordLadder {

    public int ladderLength(String beginWord, String endWord, List<String> wordList) {

        Queue<Pairs> queue = new LinkedList<>();
        boolean[] visited = new boolean[wordList.size()];

        queue.add(new Pairs(beginWord, 1));

        while (!queue.isEmpty()) {
            Pairs input = queue.remove();
            if(input.word.equals(endWord)) {
                return input.count;
            }
            for (int i = 0; i < wordList.size(); i++) {
                if (!visited[i] && isDistanceBetweenWordsISOne(wordList.get(i), input.word)) {
                    queue.add(new Pairs(wordList.get(i), input.count+1));
                    visited[i] = true;
                }

            }

        }

        return 0;

    }

    private boolean isDistanceBetweenWordsISOne(String s, String word) {

        if (s.length() != word.length()) {
            return false;
        }
        int diff = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != word.charAt(i)) {
                diff++;
                if (diff > 1)
                    return false;
            }
        }
        return true;
    }

    public static void main(String s[]) {
        WordLadder wordLadder = new WordLadder();
        List<String> list = new ArrayList<>();
        // "hot","dot","dog","lot","log","cog"
        list.add("hot");
        list.add("dot");

        list.add("dog");
        list.add("lot");

        list.add("log");
        list.add("cog");

        int value = wordLadder.ladderLength("hit", "cog", list);
        System.out.println(value);
    }
}
