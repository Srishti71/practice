package Jan_26;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LRUCache {

    /*
    Implementation using Doubly Linked List
    */

    class DLinkedList {
        int key, value;
        DLinkedList next, prev;
    }

    private void addNEwNode(DLinkedList node) {
        node.prev = head;
        node.next = head.next;

        head.next.prev = node;
        head.next = node;
    }

    private void removeNode(DLinkedList node) {
        DLinkedList prev = node.prev;
        DLinkedList next = node.next;

        node.prev.next = next;
        next.prev = prev;
    }

    private void moveToHead(DLinkedList node) {
        removeNode(node);
        addNEwNode(node);
    }

    private DLinkedList popTail() {
        DLinkedList dLinkedList = tail.prev;
        removeNode(dLinkedList);
        return dLinkedList;
    }

    Map<Integer, DLinkedList> map = new HashMap<>();
    DLinkedList head, tail;

    int capacity;
    int size;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new DLinkedList();
        tail = new DLinkedList();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {

        DLinkedList node = map.get(key);
        if(node == null) return  -1;

        moveToHead(node);
        return node.value;
    }
    public void put(int key, int value) {
        DLinkedList node = map.get(key);

        if( node == null) {
            node.key = key;
            node.value = value;

            map.put(key, node);
            addNEwNode(node);

            size++;

            if(size > capacity) {
                DLinkedList pop = popTail();
                map.remove(pop.key);
                size--;
            }


        } else {
            node.value = value;
            moveToHead(node);
        }
    }
}
