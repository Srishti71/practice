package Jan_25;

/*
Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8

either of them could be null return not null element

when any one of them has reached null then return rest if the nigger list as it is



 */


import common.ListNode;

import java.util.List;

public class AddTwoNumbersLL {

    ListNode result = null, head = null;

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        if (l1 == null && l2 == null)
            return null;

        if (l1 == null)
            return l2;

        if (l2 == null)
            return l1;

        int carry = 0;
        while (l1 != null && l2 != null) {
            int sum = l1.val + l2.val + carry;
            carry = 0;
            if (sum > 9) {
                carry = sum / 10;
                sum = sum % 10;
            }
            if (result == null) {
                result = new ListNode(sum);
                head = result;
            } else {
                result.next = new ListNode(sum);
                result = result.next;
            }
            l1 = l1.next;
            l2 = l2.next;
        }

        while (l1 != null) {
            int sum = l1.val + carry;
            carry = 0;
            if (sum > 9) {
                carry = sum / 10;
                sum = sum % 10;
            }
            result.next = new ListNode(sum);
            result = result.next;

            l1 = l1.next;
        }

        while (l2 != null) {
            int sum = l2.val + carry;
            carry = 0;
            if (sum > 9) {
                carry = sum / 10;
                sum = sum % 10;
            }
            result.next = new ListNode(sum);
            result = result.next;

            l2 = l2.next;
        }

        if (carry != 0) {
            result.next = new ListNode(carry);
        }

        return head;
    }

    public static void main(String s[]) {
        AddTwoNumbersLL addTwoNumbersLL = new AddTwoNumbersLL();

        ListNode l1 = new ListNode(5);

        ListNode l2 = new ListNode(5);

        ListNode head = addTwoNumbersLL.addTwoNumbers(l1, l2);
        addTwoNumbersLL.printElement(head);
    }

    public void printElement(ListNode node) {
        ListNode head = node;

        while (head != null) {
            System.out.println(head.val);
            head = head.next;
        }
    }
}
