package Jan_25;

import java.util.ArrayList;
import java.util.List;

/*
    https://leetcode.com/problems/shortest-way-to-form-string/

    Algorithm:

    generate all the substrings of B

    sort them

    go through A to using greedy algo to find out the minimum ways. / try recuirsively when the possible subSequence is found and obtain the result.

 */
public class ShortestFormString {

    int minValue = Integer.MAX_VALUE;


    private static List<String> generateAllSubSequence(String source) {
        List<String> subsequence = new ArrayList<String>();
        int length = source.length();
        for(int i = 0; i < length; i++) {
            for( int j = i; j< length; j++) {
                subsequence.add(source.substring(i, j));
            }
        }
        return subsequence;
    }

    private int formDifferentCombination(List<String> subSequenceList, String sub, int value) {

        if (sub.equals("")) {
            System.out.println(value);
            minValue = Math.min(minValue, value);
            return value;
        }

        int n = sub.length();
        if (n == 1) {
            System.out.println(sub);
            if (subSequenceList.contains(sub)) {
                minValue = Math.min(minValue, value + 1);
                return value + 1;
            }
        }
        for (int j = 1; j <= n; j++) {
            String substring = sub.substring(0, j);
            System.out.println(substring);
            if (subSequenceList.contains(substring)) {
                formDifferentCombination(subSequenceList, sub.substring(j), value + 1);
            }
        }

        return Integer.MAX_VALUE;
    }

    public static void main(String s[]) {
        // source = "abc", target = "abcbc"

        ShortestFormString shortestFormString = new ShortestFormString();
        shortestFormString.formDifferentCombination(generateAllSubSequence("abc"), "abcb", 0);

        System.out.println("Minimum ways: " + shortestFormString.minValue);

    }
}


