package Feb_20;

import java.util.Stack;

public class EvaluateReverseNotation {

    public static void main(String s[]) {

        EvaluateReverseNotation evaluate = new EvaluateReverseNotation();
        String[] token = {"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"};

        // ["3","11","5","+","-"]
        evaluate.evalRPN(token);
    }

    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();

        for(String s: tokens) {
            if(s.equals("+")) {
                stack.push(stack.pop() + stack.pop());
            } else if(s.equals("-")) {
                int secondNum = stack.pop();
                stack.push(stack.pop() - secondNum);

            } else if(s.equals("*")) {
                stack.push(stack.pop() * stack.pop());
            } else if(s.equals("/")) {
                int secondNum = stack.pop();
                stack.push(stack.pop() / secondNum);
            } else{
                stack.push(Integer.parseInt(s));
            }
        }

        int result =  stack.pop();
        System.out.println(result);
        return result;
    }
}

/*
Input: ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
Output: 22
Explanation:
  ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
= ((10 * (6 / (12 * -11))) + 17) + 5
= ((10 * (6 / -132)) + 17) + 5
= ((10 * 0) + 17) + 5
= (0 + 17) + 5
= 17 +


["3","11","5","+","-"] = -13

["3","11","+","5","-"]


["4","-2","/","2","-3","-","-"]
 */
