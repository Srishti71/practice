package Feb_20;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BracesExpansion {

    public String[] expand(String s) {
        List<String> res = new ArrayList();
        backtrack(new StringBuilder(), s, res);
        Collections.sort(res);
        System.out.println(res);
        String[] strs = new String[res.size()];
        for (int i = 0; i < strs.length; i++) {
            strs[i] = res.get(i);
        }
        return strs;
    }

    private void backtrack(StringBuilder cur, String s, List<String> res) {

        if (s.length() == 0) {
            res.add(cur.toString());
            return;
        }
        if (s.charAt(0) != '{') {//no brace expansion
            char c = s.charAt(0);
            cur.append(c);
            s = s.substring(1);
            backtrack(cur, s, res);
            s = c + s;
            cur.setLength(cur.length() - 1);
        } else {//process brace expansion
            int end = s.indexOf("}");
            String brace = s.substring(0, end + 1);
            s = s.substring(end + 1);
            for (int i = 0; i < brace.length(); i++) {
                char c = brace.charAt(i);
                if (!Character.isLetter(c)) continue;
                cur.append(c);
                backtrack(cur, s, res);
                cur.setLength(cur.length() - 1);
            }
            s = brace + s;
        }
    }

    public static void main(String s[]) {
        BracesExpansion bracesExpansion = new BracesExpansion();
        bracesExpansion.expand("ab{c,{d, g} p}ef");

    }
}
