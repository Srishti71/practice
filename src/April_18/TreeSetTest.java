package April_18;

import java.util.Collections;
import java.util.TreeSet;

public class TreeSetTest {
    public static void main(String s[]) {

        TreeSetTest treeSetTest = new TreeSetTest();

        TreeSet<Integer> treeSet = new TreeSet<>();
        Integer[] number = {6, 1, 7, 2, 3, 4, 9, 0};
        Collections.addAll(treeSet, number);
        System.out.println(treeSet);
        TreeSet<String> set = new TreeSet<>();
        String[] words = {"abc", "aa", "ab", "bcde", "a", "aba", "e", "efgh"};
        Collections.addAll(set, words);
        System.out.println(set);


    }
}
