package April_18;

public class MinimumNumberOfJumps {

    public static void main(String str[]) {
        MinimumNumberOfJumps jump = new MinimumNumberOfJumps();
        jump.minNumberOfJumps(new int[]{1, 3, 6, 1, 0, 9});
    }

    public void minNumberOfJumps(int[] nums) {

        int n = nums.length;
        int[] jumps = new int[n];

        jumps[0] = 0;
        for (int i = 1; i < n; i++) {
            jumps[i] = Integer.MAX_VALUE;
            for (int j = 0; j < i; j++) {
                if (i <= j + nums[j] && jumps[j] != Integer.MAX_VALUE) {
                    jumps[i] = Math.min(jumps[i], jumps[j] + 1);
                }
            }
        }

        for (int i : jumps) {
            System.out.println(i);
        }
        System.out.println("Result: " + jumps[n - 1]);
    }
}
