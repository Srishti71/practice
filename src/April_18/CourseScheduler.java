package April_18;

import java.util.*;

public class CourseScheduler {

    public static void main(String s[]) {
        CourseScheduler schedule = new CourseScheduler();
        // 4, [[1,0],[2,0],[3,1],[3,2]]
        int[][] courses = {{0, 1}, {0, 2}, {1, 2}};
        schedule.findOrder(3, courses);
    }

    public int[] findOrder(int numCourses, int[][] prerequisites) {

        Map<Integer, List<Integer>> map = new HashMap();

        for (int i = 0; i < prerequisites.length; i++) {
            List<Integer> l = map.getOrDefault(prerequisites[i][1], new ArrayList());
            l.add(prerequisites[i][0]); // [ 0 --> 1, 2] [ 1--> 3] [ 2 --> 3]
            map.put(prerequisites[i][1], l);
        }

        System.out.println(map);
        int[] countDependency = new int[numCourses];
        Arrays.fill(countDependency, 0);

        // {1, 0}, {2, 1}
        for (int i = 0; i < prerequisites.length; i++) {
            countDependency[prerequisites[i][0]] += 1; // countDependency[1] = 1 countDependency[2] // = 1;  countDependency[3] = 2
        }

        Queue<Integer> nextCourses = new LinkedList(); // [0, 1, 2, 3]
        for (int i = 0; i < numCourses; i++) {
            if (countDependency[i] == 0) {
                nextCourses.add(i);
            }
        }
        int[] result = new int[nextCourses.size()];
        int k = 0;
        while(!nextCourses.isEmpty()) {

        }
        System.out.println(nextCourses);

        return result;
    }
}
