package April_18;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SerializeDeserializeBST {

    public static void main(String s[]) {
        SerializeDeserializeBST serializeDeserializeBST = new SerializeDeserializeBST();
        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(5);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);

        root.left = node1;
        root.right = node2;

        node1.left = node3;
        node1.right = node4;

        String serialized = serializeDeserializeBST.serialize(root);
        System.out.println(serialized);
    }


    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        return serialization(root, new StringBuffer());
    }

    private String serialization(TreeNode root, StringBuffer sb) {

        if (root == null) {
            sb.append("null,");
        } else {
            sb.append(root.val + ",");
            serialization(root.left, sb);
            serialization(root.right, sb);
        }
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] dataArray = data.split(",");
        System.out.println(data);

        List<String> list;
        list = new ArrayList(Arrays.asList(dataArray));

        return deserialization(list);

    }

    private TreeNode deserialization(List<String> list) {
        TreeNode node = null;
        if (list.get(0).equals("null")) {
            list.remove(0);
            return null;
        }

        node = new TreeNode(Integer.parseInt(list.get(0)));
        list.remove(0);
        node.left = deserialization(list);
        node.right = deserialization(list);

        return node;
    }
}
