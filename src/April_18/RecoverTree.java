package April_18;

import com.sun.source.tree.Tree;
import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class RecoverTree {
    public void recoverTree(TreeNode root) {
        if (root == null)
            return;
        List<Integer> orderedList = new ArrayList<>();

        inorderTraversal(root, orderedList);
        System.out.println(orderedList);
        int[] e = findDuplicate(orderedList);

        recover(root, 2, e[0], e[1]);
        inorderTraversal(root, orderedList);
        System.out.println(orderedList);
    }

    public void recover(TreeNode root, int count, int small, int large) {

        if (root != null) {
            if (root.val == large || root.val == small) {
                root.val = root.val == small ? large: small;
                count--;
                if(count == 0) return;
                //large = -1;
            }

            recover(root.left, count, small, large);
            recover(root.right, count, small, large);
        }
    }

    public void inorderTraversal(TreeNode root, List<Integer> orderedList) {
        if (root == null)
            return;

        inorderTraversal(root.left, orderedList);
        orderedList.add(root.val);
        inorderTraversal(root.right, orderedList);
    }

    public int[] findDuplicate( List<Integer> orderedList) {
        int n = orderedList.size();
        int x = -1, y = -1;
        for (int i = 0; i < n - 1; ++i) {
            if (orderedList.get(i + 1) < orderedList.get(i)) {
                y = orderedList.get(i + 1);
                // first swap occurence
                if (x == -1) x = orderedList.get(i);
                    // second swap occurence
                else break;
            }
        }
        return new int[]{x, y};
    }

    public static void main(String s[]) {
        RecoverTree reco = new RecoverTree();
        TreeNode root = new TreeNode(2);
        TreeNode node1 = new TreeNode(1);
        root.left = node1;

        reco.recoverTree(root);

    }
}
