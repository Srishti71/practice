package April_14;

import java.util.ArrayList;
import java.util.List;

class Node {
    int data;
    Node left, right;

    Node(int value) {
        this.data = value;
    }
}

public class BoundaryOfBinaryTree {
    Node root;
    List<Integer> list = new ArrayList<>();

    // A simple function to print leaf nodes of a binary tree
    void printLeaves(Node node) {
        if (node == null)
            return;

        printLeaves(node.left);
        // Print it if it is a leaf node
        if (node.left == null && node.right == null)
            list.add(node.data);
        printLeaves(node.right);
    }

    // A function to print all left boundary nodes, except a leaf node.
    // Print the nodes in TOP DOWN manner
    void printBoundaryLeft(Node node) {
        if (node == null)
            return;

        if (node.left != null) {
            // to ensure top down order, print the node
            // before calling itself for left subtree
            list.add(node.data);
            printBoundaryLeft(node.left);
        } else if (node.right != null) {
            list.add(node.data);
            printBoundaryLeft(node.right);
        }

        // do nothing if it is a leaf node, this way we avoid
        // duplicates in output
    }

    // A function to print all right boundary nodes, except a leaf node
    // Print the nodes in BOTTOM UP manner
    void printBoundaryRight(Node node) {
        if (node == null)
            return;

        if (node.right != null) {
            // to ensure bottom up order, first call for right
            // subtree, then print this node
            printBoundaryRight(node.right);
            list.add(node.data);
        } else if (node.left != null) {
            printBoundaryRight(node.left);
            list.add(node.data);
        }
        // do nothing if it is a leaf node, this way we avoid
        // duplicates in output
    }

    // A function to do boundary traversal of a given binary tree
    void printBoundary(Node node) {
        if (node == null)
            return;

        list.add(node.data);

        // Print the left boundary in top-down manner.
        printBoundaryLeft(node.left);

        // Print all leaf nodes
        printLeaves(node.left);
        printLeaves(node.right);

        // Print the right boundary in bottom-up manner
        printBoundaryRight(node.right);
    }

    // Driver program to test above functions
    public static void main(String args[]) {
        BoundaryOfBinaryTree tree = new BoundaryOfBinaryTree();
        tree.root = new Node(1);
        tree.root.right = new Node(2);
        tree.root.right.left = new Node(3);
        tree.root.right.right = new Node(4);
        tree.printBoundary(tree.root);
        System.out.println(tree.list);
    }
}
