package April_14;

import java.util.*;

public class DesignMemoryFileSystem {

    /*
    Map<Key, DoublyLinkedList>
    /a
    "/" -> TreeSet [a]
    /a/b
    "a" -> TreeSet[b]
    [a, b]
     */

    Map<String, TreeSet<String>> map = new HashMap();
    Map<String, String> files = new HashMap<>();

    public DesignMemoryFileSystem() {

    }

    public List<String> ls(String path) {
        if (path.equals("/")) {
            return new ArrayList(map.get("/"));
        } else {
            String p[] = path.split("/");
            String last = p[p.length - 1];
            if (files.containsKey(path)) {
                return Arrays.asList(last);
            } else {
                if (map.get(last) == null) {
                    return new ArrayList<>();
                }
                return new ArrayList(map.get(last));
            }
        }
    }

    public void mkdir(String path) {
        if (path.equals("/")) {
            map.put("/", new TreeSet<>());
        } else {
            String s[] = path.split("/");
            String key = "/";

            for (int i = 1; i < s.length; i++) {
                if (!s[i].equals("")) {
                    if (map.get(key) != null && !map.get(key).contains(s[i])) {
                        map.get(key).add(s[i]);
                    } else if (map.get(key) == null) {
                        map.put(key, new TreeSet());
                        map.get(key).add(s[i]);
                    }
                    key = s[i];
                }
            }
        }
    }

    public void addContentToFile(String filePath, String content) {
        files.put(filePath, content);
        mkdir(filePath);
    }

    public String readContentFromFile(String filePath) {
        return files.get(filePath);
    }

    public static void main(String s[]) {
        DesignMemoryFileSystem designMemoryFileSystem = new DesignMemoryFileSystem();
        designMemoryFileSystem.mkdir("/a/b/d");
        designMemoryFileSystem.mkdir("/a/b/c");
        designMemoryFileSystem.mkdir("/a/b/e");
        designMemoryFileSystem.addContentToFile("/a/b/c/d", "Hello");
        designMemoryFileSystem.addContentToFile("/a/b/c/d", "Hello Word");
        System.out.println(designMemoryFileSystem.readContentFromFile("/a/b/c/d"));
        System.out.println(designMemoryFileSystem.ls("/a/b/f"));
    }

}
