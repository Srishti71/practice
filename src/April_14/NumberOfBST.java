package April_14;

import common.TreeNode;

import java.util.*;

public class NumberOfBST {

    public static void main(String s[]) {
        NumberOfBST num = new NumberOfBST();
        List<TreeNode> l = num.differentBst(Arrays.asList(2, 3, 1, 4));
        System.out.println(l.size());
    }

    Set<Integer> set = new HashSet<>();

    public List<TreeNode> differentBst(List<Integer> array) {

        List<TreeNode> resultNode = new ArrayList<>(); // rn: []

        for (int i = 0; i < array.size(); i++) { // I1:[1,2,3]  I2:[2,3] I3:[3]

            int curr = array.get(i); // I1: 1  I2: 2, I3: 3
            TreeNode root = new TreeNode(curr); // I1:1; I2:2, I3: 3
            set.add(curr);

            List<Integer> root_left = getLeftNode(curr, array, true); // I1:[], I2:[] I3:[]
            List<Integer> root_right = getLeftNode(curr, array, false); // I1: [2, 3], I2:[3],  I3: []

            List<TreeNode> leftNodes = differentBst(root_left); // I1: [], I2:[], I3:[]
            List<TreeNode> rightNodes = differentBst(root_right); // I1: root_right = [2,3] , I2: root_right = [3], I3:[] rightNode = []

            if (!leftNodes.isEmpty() && !rightNodes.isEmpty()) {

                for (TreeNode left : leftNodes) {
                    for (TreeNode right : rightNodes) {
                        TreeNode newRoot = new TreeNode(root.val);
                        newRoot.left = left;
                        newRoot.right = right;
                        resultNode.add(newRoot);
                    }
                }
            } else if (!leftNodes.isEmpty()) {
                for (TreeNode left : leftNodes) {

                    TreeNode newRoot = new TreeNode(root.val);
                    newRoot.left = left;
                    resultNode.add(newRoot);

                }
            } else if (!rightNodes.isEmpty()) {
                for (TreeNode right : rightNodes) {
                    TreeNode newRoot = new TreeNode(root.val);
                    newRoot.right = right;
                    resultNode.add(newRoot);
                }

            } else {
                TreeNode newRoot = new TreeNode(root.val);
                resultNode.add(newRoot);
            }
            set.remove(curr);
        }
        return resultNode;
    }

    private List<Integer> getLeftNode(int curr, List<Integer> array, boolean left) {

        List<Integer> newArray = new ArrayList<>();

        if (left) {
            for (int ele : array) {
                if (ele < curr && !set.contains(ele)) {
                    newArray.add(ele);
                }
            }
        } else {
            for (int ele : array) {
                if (ele > curr && !set.contains(ele)) {
                    newArray.add(ele);
                }
            }
        }
        return newArray;
    }
}