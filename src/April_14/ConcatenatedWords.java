package April_14;

import java.util.*;

public class ConcatenatedWords {

    public static void main(String s[]) {
        ConcatenatedWords conc = new ConcatenatedWords();
        conc.findAllConcatenatedWordsInADict(new String[]{"cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"});
    }

    public List<String> findAllConcatenatedWordsInADict(String[] words) {

        List<String> result = new ArrayList<>();
        Set<String> wordSet = new HashSet<>();

        for(String w: words) {
            wordSet.add(w);
        }
        for(String word: words) {
            if(checkSubString(word, wordSet))
                result.add(word);
        }
        System.out.println(result);
        return result;
    }

    private boolean checkSubString(String word, Set<String> set) {

        for(int i = 1; i< word.length(); i++) {
            String newWord1 = word.substring(0, i);
            if(set.contains(newWord1)) {
                if(set.contains(word.substring(i)) || checkSubString(word.substring(i), set)) {
                    return true;
                }
            }
        }
        return false;
    }
}
