package April_14;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NumberOfDistinctIsland {

    public static void main(String s[]) {
        NumberOfDistinctIsland num = new NumberOfDistinctIsland();
        int[][] grid = {{1,1,0,0,0},
                {1,1,0,0,0},
                {0,0,0,1,1},
                {0,0,0,1,1}};

        num.numDistinctIslands(grid);
    }

    List<Integer> shape;
    boolean[][] seen;
    int row;
    int col;

    public int numDistinctIslands(int[][] grid) {

        row = grid.length;
        col = grid[0].length;

        seen = new boolean[row][col];
        Set shapes = new HashSet<ArrayList<Integer>>();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                shape = new ArrayList();
                explore(grid, i, j, 0);
                System.out.println(shape);

                if (!shapes.contains(shape)) {
                    shapes.add(shape);
                }
            }
        }
        System.out.println("size: "+shapes.size());
        return shapes.size();
    }

    private void explore(int[][] grid, int i, int j, int direction) {

        if (i >= 0 && j > 0 && i < row && j < col && grid[i][j] == 1 && !seen[i][j]) {
            seen[i][j] = true;
            shape.add(direction);
            explore(grid, i - 1, j, 1);
            explore(grid, i + 1, j, 2);
            explore(grid, i, j - 1, 3);
            explore(grid, i, j + 1, 4);
            shape.add(0);

        }
    }

}
