package April_14;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

class Data {
    int value;
    int lastUsed;
    int frequency;

    Data(int value, int lastUsed, int frequency) {
        this.value = value;
        this.lastUsed = lastUsed;
        this.frequency = frequency;
    }

}

public class LFUCache {

    Map<Integer, Data> map = new HashMap<>();
    PriorityQueue<Data> pq = new PriorityQueue<Data>((a, b) -> a.frequency - b.frequency == 0 ? a.lastUsed - b.lastUsed : a.frequency - b.frequency);
    int time = 0;
    int capacity;

    public LFUCache(int capacity) {
        this.capacity = capacity;
    }

    //  Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
    public int get(int key) {
        if (!map.containsKey(key)) return -1;
        time++;
        Data curr = map.get(key);
        curr.lastUsed = time;
        curr.frequency = curr.frequency + 1;

        System.out.println(key);
        return key;
    }

    /*
    Set or insert the value if the key is not already present. When the cache reaches its capacity,
     it should invalidate the least frequently used item before inserting a new item. For the purpose of this problem,
     when there is a tie (i.e., two or more keys that have the same frequency), the least recently used key would be evicted.
     */
    public void put(int key, int value) {
        if(map.containsKey(key) ) {
            return;
        }

        if (map.size() >= capacity) {
            Data d = pq.poll();
            System.out.println("Pop: "+d.value);
            map.remove(d.value);
        }

        time++;
        Data d = new Data(value, time, 1);
        map.put(key, d);
        pq.add(d);
    }

    public static void main(String s[]) {
        LFUCache cache = new LFUCache( 2 /* capacity */ );

        cache.put(1, 1);
        cache.put(2, 2);
        cache.get(1);       // returns 1
        cache.put(3, 3);    // evicts key 2
        cache.get(2);       // returns -1 (not found)
        cache.get(3);       // returns 3.
        cache.put(4, 4);    // evicts key 1.
        cache.get(1);       // returns -1 (not found)
        cache.get(3);       // returns 3
        cache.get(4);       // returns 4
    }
}
