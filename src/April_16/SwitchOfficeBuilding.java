package April_16;

import javafx.util.Pair;

import java.util.*;

class Request {
    String name;
    int to;
    int from;

    Request(String name, int from, int to) {
        this.name = name;
        this.from = from;
        this.to = to;
    }
}

public class SwitchOfficeBuilding {

    List<List<String>> result = new ArrayList<>();

    Map<Integer, List<Pair<String, Integer>>> map = new HashMap();

    Set<String> set = new HashSet();

    public void buildGraph(List<Request> list) {
        for (Request req : list) {
            List<Pair<String, Integer>> pairs = map.getOrDefault(req.from, new ArrayList<>());
            pairs.add(new Pair(req.name, req.to));
            map.put(req.from, pairs);
        }
    }

    public void findBuildingSwitch() {

        for (Integer key : map.keySet()) {
            List<Pair<String, Integer>> list = map.get(key);

            for (Pair<String, Integer> pair : list) {

                String name = pair.getKey();
                // ["Alex", 2],
                if (!set.contains(name)) {
                    List<String> path = new ArrayList();
                    path.add(name); // Alex
                    dfs(pair, key, path);
                }
            }
        }
    }

    void dfs(Pair<String, Integer> currentReq, int beginningPath, List<String> path) { // I1: ["Alex", 2],,   1,,, [Alex] |||||||  I2:  ["Ben", 1], 1,  [Alex, Ben]
        int to = currentReq.getValue();
        String name = currentReq.getKey();
        if (to == beginningPath) {
            System.out.println(path);
            result.add(new ArrayList(path));
            return;
        }

        set.add(name); // set= ["Alex", 2]

        List<Pair<String, Integer>> potentialCanditate = map.get(to); // 2 -> ["Ben", 1], ["David", 3]

        if (potentialCanditate != null) {
            for (Pair<String, Integer> r : potentialCanditate) { //  ["Ben", 1]
                if (!set.contains(r.getKey())) {
                    path.add(r.getKey()); // [Alex, Ben]

                    dfs(r, beginningPath, path); //    ["Ben", 1], 1,  [Alex, Ben]

                    path.remove(path.size() - 1); // [Alex]
                }
            }
        }
        set.remove(name);
    }

    public static void main(String s[]) {
        SwitchOfficeBuilding building = new SwitchOfficeBuilding();
        List<Request> list = new ArrayList<>();
        list.add(new Request("Alex", 1, 2));
        list.add(new Request("Ben", 2, 1));
        list.add(new Request("Chris", 1, 2));
        list.add(new Request("David", 2, 3));
        list.add(new Request("Ellen", 3, 1));
        list.add(new Request("Frank", 4, 5));

        building.buildGraph(list);
        building.findBuildingSwitch();
    }
}
