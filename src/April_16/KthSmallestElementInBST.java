package April_16;

import common.TreeNode;

public class KthSmallestElementInBST {

    int smallest = 0;

    public static void main(String s[]) {
        KthSmallestElementInBST kth = new KthSmallestElementInBST();
        // 5,3,6,2,4,null,null,1
        TreeNode root = new TreeNode(5);
        TreeNode node1 = new TreeNode(3);
        TreeNode node2 = new TreeNode(6);
        TreeNode node3 = new TreeNode(2);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(1);

        root.left = node1;
        root.right = node2;
        node1.left = node3;
        node1.right = node4;
        node3.left = node5;
        int value = kth.findKthSmallestElement(root, 3);
        System.out.println(value);
    }

    public int findKthSmallestElement(TreeNode node, int k) {

        if (node == null)
            return 0;

        if(smallest < k) {
            findKthSmallestElement(node.left, k);
            smallest++;
            if (smallest == k) {
                System.out.println(node.val);
                return node.val;
            } else {
                return findKthSmallestElement(node.right, k);
            }
        } else {
            return 0;
        }

    }

}
