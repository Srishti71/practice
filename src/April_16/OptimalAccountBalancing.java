package April_16;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptimalAccountBalancing {

    public int minTransfers(int[][] transactions) {

        Map<Integer, Integer> map = new HashMap();
        for (int[] transaction : transactions) {
            map.put(transaction[0], map.getOrDefault(transaction[0], 0) + transaction[2]);
            map.put(transaction[1], map.getOrDefault(transaction[1], 0) - transaction[2]);
        }

        List<Integer> list = new ArrayList();
        for (Integer value : map.values()) {
            list.add(value); //[]
        }

        return optimizeAccount(list, 0);
    }

    private int optimizeAccount(List<Integer> list, int i) { // I1: [i :0 , list[5, -10, 5]] I2: [5, -5, 5]]

        if (i == list.size()) {
            return 0;
        }
        int curr = list.get(i); // curr: -5

        if (curr == 0)
            return optimizeAccount(list, i + 1);

        int min = Integer.MAX_VALUE;

        for (int j = i + 1; j < list.size(); j++) {
            int next = list.get(j); // -10
            if (curr * next < 0) { // true
                list.set(j, curr + next); // [5, -5, 5]]
                min = Math.min(min, 1 + optimizeAccount(list, i + 1));
                list.set(j, next);
            }

        }
        return min;
    }

    public static void main(String s[]) {
        OptimalAccountBalancing optimal = new OptimalAccountBalancing();
        int[][] balance = {{0, 1, 1}, {0, 2, 2}, {0, 3, 3}, {0, 4, 3}, {0, 5, 3}};
        System.out.println(optimal.minTransfers(balance));
    }
}
