package April_16;

import java.util.*;

public class WordLadder2 {

    Map<String, List<String>> map = new HashMap<>();
    List<List<String>> result = new ArrayList();

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {

        Set<String> wordSet = new HashSet(wordList);
        if (!wordSet.contains(endWord)) {
            return new ArrayList();
        }
        Set<String> startSet = new HashSet();
        startSet.add(beginWord);

        buildGraph(startSet, wordSet, endWord);

        List<String> list = new ArrayList();
        list.add(beginWord);

        dfs(beginWord, endWord, list);
        return result;
    }

    private void dfs(String beginWord, String endWord, List<String> list) {

        if (beginWord.equals(endWord)) {

            result.add(new ArrayList(list));
            return;
        }
        if (map.get(beginWord) == null) {
            return;
        }

        List<String> possibleCandidate = map.get(beginWord);
        for (String word : possibleCandidate) {
            list.add(word);
            dfs(word, endWord, list);
            list.remove(list.size() - 1);

        }

    }

    public void buildGraph(Set<String> startSet, Set<String> words, String endWord) {
        if (startSet.size() == 0) return;

        boolean finish = false;
        Set<String> tmp = new HashSet<>();

        words.removeAll(startSet);

        for (String s : startSet) {
            char[] beginArray = s.toCharArray();
            for (int i = 0; i < beginArray.length; i++) {
                char old = beginArray[i];
                for (char c = 'a'; c <= 'z'; c++) {
                    beginArray[i] = c;
                    String word = new String(beginArray);
                    if (words.contains(word)) {
                        if (word.equals(endWord)) {
                            finish = true;
                        } else {

                            tmp.add(word);
                        }
                        if (map.get(s) == null) {
                            map.put(s, new ArrayList());
                        }
                        map.get(s).add(word);
                    }
                }
                beginArray[i] = old;
            }
        }
        if (!finish) {
            buildGraph(tmp, words, endWord);
        }
    }

    public static void main(String s[]) {
        WordLadder2 wl = new WordLadder2();
        wl.findLadders("qa", "sq", Arrays.asList(
                "si", "go", "se", "cm", "so", "ph", "mt", "db", "mb", "sb", "kr", "ln", "tm", "le", "av", "sm", "ar", "ci",
                "ca", "br", "ti", "ba", "to", "ra", "fa", "yo", "ow", "sn", "ya", "cr", "po", "fe", "ho", "ma", "re", "or", "rn", "au", "ur", "rh", "sr", "tc", "lt", "lo", "as", "fr", "nb", "yb", "if", "pb", "ge", "th", "pm", "rb", "sh", "co", "ga", "li", "ha", "hz", "no", "bi", "di", "hi", "qa", "pi", "os", "uh", "wm", "an", "me", "mo", "na", "la",
                "st", "er", "sc", "ne", "mn", "mi", "am", "ex", "pt", "io", "be", "fm", "ta", "tb", "ni", "mr", "pa", "he", "lr", "sq", "ye"));
        System.out.println(wl.result);
    }
}
