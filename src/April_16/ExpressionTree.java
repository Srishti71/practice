package April_16;

import common.TreeNode;

import java.util.Stack;

class Node {
    String val;
    Node left;
    Node right;

    Node(String val) {
        this.val = val;
    }
}

public class ExpressionTree {

    public boolean isOperator(char op) {
        if (op == '+' || op == '-' || op == '*' || op == '/' || op == '(' || op == ')') {
            return true;
        }
        return false;
    }

    public Node constructET(String expression) {
        Stack<Node> stack = new Stack();

        for (int i = 0; i < expression.length(); i++) {
            if (isOperator(expression.charAt(i))) {
                Node left = null, right = null;
                if (!stack.isEmpty()) {
                    left = stack.pop();
                }
                if (!stack.isEmpty()) {
                    right = stack.pop();
                }
                Node curr = new Node(expression.charAt(i) + "");
                curr.left = left;
                curr.right = right;
                stack.push(curr);
            } else {
                Node curr = new Node(expression.charAt(i) + "");
                stack.push(curr);
            }
        }
        System.out.println("Stack size" + stack.size());
        return stack.pop();
    }

    public static void main(String s[]) {
        ExpressionTree expTree = new ExpressionTree();
        Node root = expTree.constructET("ab+ef*g*-");

        System.out.println(expTree.constructStringExp(root));
    }

    public String constructStringExp(Node node) {
        if (node == null)
            return "";
        return constructStringExp(node.left) + node.val + constructStringExp(node.right);
    }

    private static void print(Node root) {

        if (root != null) {
            print(root.left);
            System.out.println(root.val);
            print(root.right);
        }
    }
}
