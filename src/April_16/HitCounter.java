package April_16;

public class HitCounter {

    int hits[], count[];

    HitCounter() {
        hits = new int[300];
        count = new int[300];
    }

    public void hit(int timestamp) {

        int time = timestamp % 300;
        if (time < timestamp) {
            count[time] = 0;
            hits[time] = timestamp;
        }
        count[time] = count[time] + 1;
    }

    public int getHits(int timestamp) {
        int sum = 0;
        for (int i = 0; i < 300; i++) {
            if (hits[i] > timestamp - 300) {
                sum += count[i];
            }
        }
        return sum;
    }

    public static void main(String s[]) {
        HitCounter hit = new HitCounter();
    }
}
