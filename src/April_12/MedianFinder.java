package April_12;

import java.util.PriorityQueue;

public class MedianFinder {

    public static void main(String s[]) {
        MedianFinder med = new MedianFinder();
        System.out.println(med.findMedian());
        med.addNum(1);

        System.out.println(med.findMedian());
        med.addNum(2);
        System.out.println(med.findMedian());
        med.addNum(3);
        System.out.println(med.findMedian());

        med.addNum(6);
        System.out.println(med.findMedian());

        med.addNum(5);
        System.out.println(med.findMedian());
        med.addNum(0);
        System.out.println(med.findMedian());
        med.addNum(6);
        System.out.println(med.findMedian());

        med.addNum(3);
        System.out.println(med.findMedian());

        med.addNum(1);
        System.out.println(med.findMedian());

        med.addNum(0);
        System.out.println(med.findMedian());

        med.addNum(0);
        System.out.println(med.findMedian());
    }

    PriorityQueue<Integer> low = new PriorityQueue<>((a, b) -> b - a);
    PriorityQueue<Integer> high = new PriorityQueue<>((a, b) -> a - b);

    public MedianFinder() {
    }

    public void addNum(int num) {
        low.add(num);

        high.add(low.peek());                               // balancing step
        low.poll();

        if (low.size() < high.size()) {                     // maintain size property
            low.add(high.peek());
            high.poll();
        }
    }

    // ["MedianFinder","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian","addNum","findMedian"]
    //[[],[6],[],[10],[],[2],[],[6],[],[5],[],[0],[],[6],[],[3],[],[1],[],[0],[],[0],[]]
    public double findMedian() {

        if (low.isEmpty()) {
            return 0.0;
        }

        if (low.size() > high.size()) {
            return low.peek();
        } else if (low.size() == high.size() && !low.isEmpty()) {
            return ((double) low.peek() + high.peek()) * 0.5;
        } else {
            return 0.0;
        }

    }
}
