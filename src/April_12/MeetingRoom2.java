package April_12;

import java.util.Arrays;
import java.util.PriorityQueue;

class Room {

    int start;
    int end;

    Room(int start, int end) {
        this.start = start;
        this.end = end;
    }
}

public class MeetingRoom2 {

    public static void main(String s[]) {
        MeetingRoom2 met = new MeetingRoom2();
        int[][] intervals = {{2, 15}, {36, 45}, {9, 29}, {16, 23}, {4, 9}};
        met.minMeetingRooms(intervals);
    }

    public int minMeetingRooms(int[][] intervals) {
        PriorityQueue<int[]> queue = new PriorityQueue<int[]>((a, b) ->
                a[1] - b[1] == 0 ? a[0] - b[0] : a[1] - b[1]);

        Arrays.sort(intervals, (integers, t1) -> integers[0] - t1[0] == 0 ? integers[1] - t1[1] : integers[0] - t1[0]);

        // [2, 15] [4, 9] [9, 29] [16, 23] [36, 45]

        int count = 0;
        for (int[] element : intervals) {
            if (!queue.isEmpty()) {

                int[] potential = queue.poll(); //[2, 15]

                if (potential[1] <= element[0]) {
                    potential[0] = element[0];
                    potential[1] = element[1];
                    queue.add(potential);
                } else {
                    queue.add(potential);
                    queue.add(new int[]{element[0], element[1]});
                    count++;
                }
            } else {
                queue.add(new int[]{element[0], element[1]});
                count++;
            }
        }
        System.out.println(count);
        return count;
    }


}
