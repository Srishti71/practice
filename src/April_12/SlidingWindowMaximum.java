package April_12;

import java.util.ArrayDeque;

public class SlidingWindowMaximum {

    ArrayDeque<Integer> deque = new ArrayDeque<>();

    public int[] maxSlidingWindow(int[] nums, int k) {

        int maxIndex = 0;
        int[] result = new int[nums.length - k + 1];
        for (int i = 0; i < k; i++) {
            cleanDeque(nums, i, k);
            deque.addLast(i);
            maxIndex = (nums[i] > nums[maxIndex]) ? i : maxIndex;
        }
        result[0] = nums[maxIndex];
        int p = 1;
        for (int i = k; i < nums.length; i++) {
            cleanDeque(nums, i, k);
            deque.addLast(i);
            result[p] = deque.getFirst();
            p++;
        }
        return result;
    }

    private void cleanDeque(int[] nums, int i, int k) {
        if (!deque.isEmpty() && deque.getFirst() == i - k) {
            deque.removeFirst();
        }
        while (!deque.isEmpty() && nums[deque.getLast()] < nums[i]) {
            deque.removeLast();
        }
    }

    public static void main(String s[]) {
        SlidingWindowMaximum slidingWindowMaximum = new SlidingWindowMaximum();
        slidingWindowMaximum.maxSlidingWindow(new int[]{1,3,-1,-3,5,3,6,7}, 3);
    }
}
