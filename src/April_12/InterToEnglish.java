package April_12;

public class InterToEnglish {

    public static void main(String s[]) {
        InterToEnglish interToEnglish = new InterToEnglish();
        interToEnglish.numberToWords(12345);
    }

    public String numberToWords(int num) {

        int billion = num / 1_000_000_000;
        int million = (num - (billion * 1_000_000_000)) / 1_000_000;
        int thousand = (num - (billion * 1_000_000_000) - (million * 1_000_000)) / 1_000;
        int rest = num - (billion * 1_000_000_000) - (million * 1_000_000) - (thousand * 1000);


        String result = "";
        if (billion != 0) {
            result += formNumber(billion) + " Billion ";
        }
        if (million != 0) {
            result += formNumber(million) + " Million ";
        }
        if (thousand != 0) {
            result += formNumber(thousand) + " Thousand ";
        }
        result += formNumber(rest);
        System.out.println(result);
        return result.trim();
    }

    private String formNumber(int number) {
        int hundred = number / 1_00;
        int rest = number - hundred * 100;

        String s = "";
        if (hundred * rest != 0) {
            s += ones(hundred) + " Hundred " + two(rest);
        } else if (hundred == 0 && rest != 0) {
            s += two(rest);
        } else if (hundred != 0 && rest == 0) {
            s += ones(hundred) + " Hundred ";
        }
        return s;
    }

    private String two(int rest) {

        if (rest < 10) {
            return ones(rest);
        } else if (rest < 20) {
            return teens(rest);
        } else {
            return tens(rest);
        }
    }

    private String teens(int rest) {
        String result = "";
        switch (rest) {
            case 10:
                result = "Ten";
                break;
            case 11:
                result = "Eleven";
                break;
            case 12:
                result = "Twelve";
                break;
            case 13:
                result = "Thirteen";
                break;
            case 14:
                result = "Fourteen";
                break;
            case 15:
                result = "Fifteen";
                break;
            case 16:
                result = "Sixteen";
                break;
            case 17:
                result = "Seventeen";
                break;
            case 18:
                result = "Eighteen";
                break;
            case 19:
                result = "Nineteen";
                break;

        }
        return result;
    }

    private String tens(int rest) {

        int n = rest / 10; // 6
        int r = rest - n * 10; // 7

        if (n * r != 0) {
            return ten(n) + " " + ones(r);
        } else {
            return ten(n);
        }
    }

    private String ones(int rest) {
        String result = "";
        switch (rest) {
            case 1:
                result = "One";
                break;
            case 2:
                result = "Two";
                break;
            case 3:
                result = "Three";
                break;
            case 4:
                result = "Four";
                break;
            case 5:
                result = "Five";
                break;
            case 6:
                result = "Six";
                break;
            case 7:
                result = "Seven";
                break;
            case 8:
                result = "Eight";
                break;
            case 9:
                result = "Nine";
                break;
        }
        return result;
    }

    private String ten(int n) {
        String result = "";
        switch (n) {
            case 2:
                result = "Twenty";
                break;
            case 3:
                result = "Thirty";
                break;
            case 4:
                result = "Forty";
                break;
            case 5:
                result = "Fifty";
                break;
            case 6:
                result = "Sixty";
                break;
            case 7:
                result = "Seventy";
                break;
            case 8:
                result = "Eighty";
                break;
            case 9:
                result = "Ninety";
                break;
        }
        return result;
    }
}
