package April_12;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SerializeDeserializeBinaryTree {

    List<TreeNode> node = new ArrayList<>();

    public String serialize(TreeNode root, String s) {
        if (root == null) {
            s = s + "null,";
        } else {

            s += root.val + ",";
            s = serialize(root.left, s);
            s = serialize(root.right, s);
        }
        return s;
    }

    public String serialize(TreeNode root) {
        return serialize(root, "");
    }

    public TreeNode deserialize(String data) {
        String[] d = data.split(",");
        List<String> l = new ArrayList<String>(Arrays.asList(d));
        return rdeserialize(l);

    }

    private TreeNode rdeserialize(List<String> l) {

        if (l.get(0).equals("null")) {
            l.remove(0);
            return null;
        }

        TreeNode root = new TreeNode(Integer.parseInt(l.get(0)));
        l.remove(0);
        root.left = rdeserialize(l);
        root.right = rdeserialize(l);
        return root;
    }

    // Decodes your encoded data to tree.

    public static void main(String s[]) {
        SerializeDeserializeBinaryTree serializeDeserializeBinaryTree = new SerializeDeserializeBinaryTree();
        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(2);
        TreeNode node2 = new TreeNode(3);

        TreeNode node3 = new TreeNode(4);
        TreeNode node4 = new TreeNode(5);

        root.left = node1;
        root.right = node2;

        node2.left = node3;
        node2.right = node4;

        serializeDeserializeBinaryTree.serialize(root);
    }
}
