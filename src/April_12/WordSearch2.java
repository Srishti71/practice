package April_12;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

class TrieNode {

    TrieNode[] children = new TrieNode[26];
    String word;

}

public class WordSearch2 {

    TrieNode root = new TrieNode();
    List<String> result = new ArrayList<>();

    int[][] dirs = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};

    public static void main(String s[]) {
        WordSearch2 wordSearch2 = new WordSearch2();
        char[][] letters = {{'o', 'a', 'a', 'n'},
                {'e', 't', 'a', 'e'},
                {'i', 'h', 'k', 'r'},
                {'i', 'f', 'l', 'v'}};

        String[] words = {"oath", "pea", "eat", "rain"};
        wordSearch2.findWords(letters, words);
    }

    public void buildTrie(String[] words) {

        for (String s : words) {
            TrieNode node = root;
            for (int i = 0; i < s.length(); i++) {
                int index = s.charAt(i) - 'a';
                if(node.children[index] == null)
                    node.children[index] = new TrieNode();
                node = node.children[index];
            }
            node.word = s;
        }
    }

    public List<String> findWords(char[][] board, String[] words) {

        buildTrie(words);

        int row = board.length;
        int col = board[0].length;

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                char ch = board[i][j];
                int index = ch - 'a';
                if (root.children[index] != null) {
                    dfs(i, j, board, root);
                }
            }
        }
        System.out.println(result);
        return result;
    }

    private void dfs(int i, int j, char[][] board, TrieNode node) {
        char letter = board[i][j];
        int ch = letter - 'a';

        TrieNode currentNode = node.children[ch];

        if (currentNode.word != null) {
            result.add(currentNode.word);
            //currentNode.word = null;
        }
        board[i][j] = '#';

        for (int[] dir : dirs) {
            int first = i + dir[0];
            int second = j + dir[1];

            if (first < 0 || second < 0 || first >= board.length || second >= board[0].length || board[first][second] == '#'|| currentNode.children[board[first][second] - 'a'] == null)
                continue;
            dfs(first, second, board, currentNode);
        }
        board[i][j] = letter;
    }
}
