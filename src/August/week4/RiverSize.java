package August.week4;

import java.util.ArrayList;
import java.util.List;

public class RiverSize {


    public static List<Integer> riverSizes(int[][] matrix) {
        // Write your code here.

        List<Integer> list = new ArrayList<>();
        int n = matrix.length;

        if (n == 0)
            return new ArrayList<Integer>();

        int m = matrix[0].length;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (matrix[i][j] == 1) {
                    int res = dfs(matrix, i, j);
                    list.add(res);
                }
            }
        }
        return list;
    }

    private static int dfs(int[][] matrix, int i, int j) {

        if (i < 0 || i >= matrix.length || j < 0 || j >= matrix[0].length || matrix[i][j] == 0) {
            return 0;
        }

        matrix[i][j] = 0;

        int left = dfs(matrix, i, j - 1);
        int right = dfs(matrix, i, j + 1);
        int up = dfs(matrix, i - 1, j);
        int down = dfs(matrix, i + 1, j);

        int res = left + right + up + down + 1;
        return res;
    }

    public static void main(String[] args) {
        int matrix[][] = {{1, 0, 0, 1, 0},
                {1, 0, 1, 0, 0},
                {0, 0, 1, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 1, 1, 0}
        };

        riverSizes(matrix);
    }
}


/*
matrix[][] = {{1,0,0,1,1,0},
{1,0,1,0,0,0},
{0,0,1,0,1},
{1,0,1,0,1},
{1,0,1,1,0}
}


if(matrix[i][j] != 1)
return 0;


matrix[i][j] = 0
int left = dfs(array, i, j-1);
int right = dfs(array, i, j+1);
int up = dfs(array, i-1, j);
int down = dfs(array, i+1, j);

int result = left + right + upp + down + 1;

add.array;
 */