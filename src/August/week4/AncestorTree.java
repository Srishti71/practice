package August.week4;

public class AncestorTree {

    public static AncestralTree getYoungestCommonAncestor(
            AncestralTree topAncestor, AncestralTree descendantOne, AncestralTree descendantTwo) {
        // Write your code here.

        int depthOne = getDepth(topAncestor, descendantOne);
        int depthTwo = getDepth(topAncestor, descendantTwo);

        if (depthOne < depthTwo) {
            return backtrackAncestralTree(topAncestor, descendantOne, descendantTwo, depthTwo - depthOne);
        } else {
            return backtrackAncestralTree(topAncestor, descendantTwo, descendantOne, depthOne- depthTwo);
        }


    }

    private static AncestralTree backtrackAncestralTree(AncestralTree topAncestor, AncestralTree lowDescend, AncestralTree higherDescend, int i) {

        while (i > 0) {
            lowDescend = lowDescend.ancestor;
            i--;
        }

        while(lowDescend != higherDescend) {
            lowDescend = lowDescend.ancestor;
            higherDescend = higherDescend.ancestor;
        }
        return lowDescend;
    }


    private static int getDepth(AncestralTree topAncestor, AncestralTree descendantOne) {
        int depth = 0;
        AncestralTree currentNode = descendantOne;
        while (topAncestor != currentNode) {
            depth++;
            currentNode = currentNode.ancestor;
        }
        return depth;
    }

    static class AncestralTree {
        public char name;
        public AncestralTree ancestor;

        AncestralTree(char name) {
            this.name = name;
            this.ancestor = null;
        }

        // This method is for testing only.
        void addAsAncestor(AncestralTree[] descendants) {
            for (AncestralTree descendant : descendants) {
                descendant.ancestor = this;
            }
        }
    }


}
