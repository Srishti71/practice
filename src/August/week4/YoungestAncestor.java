package August.week4;

public class YoungestAncestor {

    public static AncestralTree getYoungestCommonAncestor(
            AncestralTree topAncestor, AncestralTree descendantOne, AncestralTree descendantTwo) {
        // Write your code here.

        if (topAncestor == null) {
            return null;
        }

        boolean isDescendantOneRightChild = isChild(topAncestor.right, descendantOne);
        boolean isDescendantTwoLeftChild = isChild(topAncestor.left, descendantTwo);

        if ((isDescendantOneRightChild && isDescendantTwoLeftChild) || (!isDescendantOneRightChild && !isDescendantTwoLeftChild)) {
            return topAncestor;
        }


        if (isDescendantOneRightChild) {
            return getYoungestCommonAncestor(topAncestor.right, descendantOne, descendantTwo);
        } else {
            return getYoungestCommonAncestor(topAncestor.left, descendantOne, descendantTwo);
        }
    }

    private static boolean isChild(AncestralTree right, AncestralTree descendantOne) {
        if (right == null || descendantOne == null)
            return false;

        if (right.value == descendantOne.value) {
            return true;
        }

        return isChild(right.left, descendantOne) || isChild(right.right, descendantOne);

    }

    public static void main(String[] args) {
        AncestralTree root = new AncestralTree('A');
        AncestralTree node1 = new AncestralTree('B');
        AncestralTree node2 = new AncestralTree('C');

        root.left = node1;
        root.right = node2;

        AncestralTree node3 = new AncestralTree('D');
        AncestralTree node4 = new AncestralTree('E');

        node1.left = node3;
        node1.right = node4;

        AncestralTree node5 = new AncestralTree('F');
        AncestralTree node6 = new AncestralTree('G');

        node2.left = node5;
        node2.right = node6;

        AncestralTree node7 = new AncestralTree('H');
        AncestralTree node8 = new AncestralTree('I');

        node3.left = node7;
        node3.right = node8;

        AncestralTree res = getYoungestCommonAncestor(root, node4, node8);
        if(res != null) {
            System.out.println(res.value);
        }

    }

}


/*

if one child is in left and other in right return node

if both in left - node.left




 */