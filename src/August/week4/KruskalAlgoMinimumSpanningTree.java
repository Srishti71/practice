package August.week4;

import java.util.Arrays;

public class KruskalAlgoMinimumSpanningTree {

    public static void main(String[] args) {
        int[][] connections = {{0, 1, 1},
                {0, 2, 7},
                {1, 3, 5},
                {1, 4, 6},
                {2, 3, 2},
                {4, 5, 9},
                {4, 6, 3},
                {6, 7, 8},
                {5, 7, 4}};




        KruskalAlgoMinimumSpanningTree algo = new KruskalAlgoMinimumSpanningTree();
        algo.buildKruskalAlgo(connections, 8);
    }

    int[] parent;
    int cost = 0;

    public void buildKruskalAlgo(int[][] array, int n) {

        parent = new int[n];
        Arrays.fill(parent, -1);
        Arrays.sort(array, (a, b) -> a[2] - b[2]);

        for (int[] connection : array) {

            union(connection[0], connection[1], connection[2]);
        }

        System.out.println("Cost is: " + cost);
    }

    public void union(int x, int y, int z) {

        int xRoot = find(x), yRoot = find(y);
        System.out.println(x + " " + xRoot + " " + y + " " + yRoot);
        if (xRoot == yRoot) {
            return;
        }

        if (parent[xRoot] < parent[yRoot]) {
            int temp = parent[yRoot];
            parent[yRoot] = xRoot;

            parent[xRoot] += temp;
            System.out.println(x + " - " + parent[xRoot] + " " + y + " - " + parent[yRoot] + " " + temp);
        } else {
            int temp = parent[xRoot];
            parent[xRoot] = yRoot;
            parent[yRoot] += temp;
            System.out.println(x + " - " + parent[xRoot] + " " + y + " - " + parent[xRoot] + " " + temp);
        }

        cost += z;
    }

    public int find(int x) {

        if (parent[x] < 0) {
            return x;
        }
        return find(parent[x]);

    }
}
