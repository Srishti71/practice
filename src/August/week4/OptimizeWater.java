package August.week4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OptimizeWater {

    int[] array;

    public static void main(String[] args) {
        OptimizeWater optimizeWater = new OptimizeWater();
        int[] wells = {1, 5, 3};
        int[][] pipes = {{1, 2, 6}, {2, 3, 2}, {1, 3, 1}};
        int n = 3;
        optimizeWater.minCostToSupplyWater(n, wells, pipes);
    }

    public int minCostToSupplyWater(int n, int[] wells, int[][] pipes) {
        array = new int[n + 1];
        List<int[]> edges = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            array[i + 1] = i + 1;
            edges.add(new int[]{0, i + 1, wells[i]});
        }
        for (int[] p : pipes) {
            edges.add(p);
        }
        Collections.sort(edges, (a, b) -> Integer.compare(a[2], b[2]));

        System.out.println();
        int res = 0;
        for (int[] e : edges) {
            int x = find(e[0]), y = find(e[1]);
            if (x != y) {
                res += e[2];
                System.out.println(x + " " + y + " " + e[2]);
                array[x] = y;
                --n;
            }
        }
        System.out.println("res" + res);
        return res;
    }

    private int find(int x) {
        if (x != array[x]) array[x] = find(array[x]);
        return array[x];
    }
}
