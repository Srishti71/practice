package August.week4;

import java.util.*;

/*

Q1: can the flight data undirected
Q2:

 */
public class CheapestFlight {

    Map<String, Integer> memo = new HashMap<>();

    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {

        Map<Integer, List<int[]>> map = new HashMap();
        for (int[] flight : flights) {
            List<int[]> list = map.getOrDefault(flight[0], new ArrayList<>());
            int[] destWithCost = new int[]{flight[1], flight[2]};
            list.add(destWithCost);
            map.put(flight[0], list);
        }

        System.out.println(map);

        int res = findCheapestPriceHelper(map, src, dst, 1, 0);
        return  res == Integer.MAX_VALUE ? -1 : res ;
    }

    private int findCheapestPriceHelper(Map<Integer, List<int[]>> map, int src, int dst, int k, int cost) {

        if (memo.containsKey(src + "-" + dst)) {
            return memo.get(src + "-" + dst);
        }

        if (src == dst) {
            return cost;
        }

        if (k < 0)
            return Integer.MAX_VALUE;

        int result = Integer.MAX_VALUE;

        List<int[]> potentialDestList = map.get(src);
        if (potentialDestList == null || potentialDestList.isEmpty()) {
            return Integer.MAX_VALUE;
        }

        for (int[] potentialDest : potentialDestList) {
            result = Math.min(result, findCheapestPriceHelper(map, potentialDest[0], dst, --k, cost + potentialDest[1]));
        }

        memo.put(src + "-" + dst, result == Integer.MAX_VALUE ? -1 : result);
        System.out.println(result);
        return result;
    }

    public static void main(String[] args) {
        CheapestFlight flight = new CheapestFlight();

        int[][] flights = {{4, 1, 1}, {1, 2, 3}, {0, 3, 2}, {0, 4, 10}, {3, 1, 1}, {1, 4, 3}};
        flight.findCheapestPrice(5, flights, 2, 1, 1);

        /*
10
[[3,4,4],[2,5,6],[4,7,10],[9,6,5],[7,4,4],[6,2,10],[6,8,6],[7,9,4],[1,5,4],[1,0,4],[9,7,3],[7,0,5],[6,5,8],
[1,7,6],[4,0,9],[5,9,1],[8,7,3],[1,2,6],[4,1,5],[5,2,4],[1,9,1],[7,8,10],[0,4,2],[7,2,8]]

6
0
7

         */
    }


}

/*
n=3,edges=[[0,1,100],[1,2,100],[0,2,500]]
        src=0,dst=2,k=1

        src,dest,k,cost
        0,2,1,0

        Option1:go to 1
        1,2,0,100=200

        Option 2:go to 2

        2,2,0,500

        cost=500;

        option1:go from 1to 2
        2,2,-1,100+100

        if(src==dest)
        return cost

        if(k< 0)
        returm Integer.MAX_VALUE;

*/