package August.week4;

import java.util.Arrays;

public class DisjointSet {

    int[] parent;

    public static void main(String[] args) {
        int n = 5;
        DisjointSet disjointSet = new DisjointSet();
        disjointSet.buildDisjointSet(n);
    }

    public void buildDisjointSet(int n) {

        parent = new int[n];

        Arrays.fill(parent, -1);

        union(0, 2);

        // 4 is a friend of 2
        union(4, 2);

        // 3 is a friend of 1
        union(3, 1);

        for (int i : parent) {
            System.out.print(i + " ");
        }

        int x = find(4);
        int y = find(0);
        if (parent[4] == parent[0])
            System.out.println("Yes");
        else
            System.out.println("No");

        // Check if 1 is a friend of 0
        x = find(1);
        y = find(0);
        if (parent[x] == parent[y])
            System.out.println("Yes");
        else
            System.out.println("No");
    }

    public int find(int x) {

        if (parent[x] < 0) {
            return x;
        }

        return find(parent[x]);

    }

    public void union(int x, int y) {

        int xRoot = find(x), yRoot = find(y);

        if (xRoot == yRoot) {
            return;
        }

        if (parent[xRoot] < parent[yRoot]) {
            int temp = parent[yRoot];
            parent[yRoot] = xRoot;
            parent[xRoot] += temp;
        } else {
            int temp = parent[xRoot];
            parent[xRoot] = yRoot;
            parent[yRoot] += temp;
        }
    }
}
