package August.week4;

import April_8.AnalyzeUserWebsite;

public class AncestralTree {
    char value;
    AncestralTree left;
    AncestralTree right;

    AncestralTree(char value) {
        this.value = value;
    }
}
