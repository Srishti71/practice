package August.week4;

import java.util.HashSet;
import java.util.Set;

public class SingleCycleCheck {

    public static boolean hasSingleCycle(int[] array) {
        // Write your code here.

        int n = array.length;
        for (int i = 0; i < n; i++) {
            Set<Integer> set = new HashSet<>();
            set.add(i);
            int newIndex = i;
            for (int j = 0; j < n; j++) {
                newIndex = newIndex + array[newIndex]; // 2
                if (newIndex < 0) {
                    newIndex += n;
                } else if (newIndex >= n) {
                    newIndex -= n;
                }

                if (set.contains(newIndex)) {
                    break;
                } else {
                    set.add(newIndex);
                }
            }
            if (set.size() == n) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int[] array = {10, 11, -6, -23, -2, 3, 88, 909, -26}; // -4 % 9 = 3
        System.out.println(hasSingleCycle(array));
    }
}
