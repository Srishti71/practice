package August.recursive;

public class MaxProfitKTransaction {

    public static void main(String[] args) {
        int[] prices = {5, 11, 3, 50, 60, 90};
        maxProfitWithKTransactions(prices, 1);
    }

    public static int maxProfitWithKTransactions(int[] prices, int k) {
        // Write your code here.
        if (k == 0 || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        if (n == 2) {
            return prices[1] - prices[0];
        }

        int res = helperMaxProfitWithKTransactions(prices, k, 0, true);
        System.out.println(globalMax+" "+res);
        return -1;
    }

    static int globalMax = 0;

    private static int helperMaxProfitWithKTransactions(int[] prices, int k, int index, boolean buy) {

        if (k == 0 || index == prices.length) {
            return 0;
        }

        int value = 0;
        if (buy) {// buy = true , buy = false
            value = helperMaxProfitWithKTransactions(prices, k--, index + 1, !buy) - prices[index];
        } else {// buy = false , buy =
            value = helperMaxProfitWithKTransactions(prices, k--, index + 1, !buy) + prices[index];
        }

        int value2 = helperMaxProfitWithKTransactions(prices, k, index + 1, buy);
        System.out.println(value+" "+value2);
        return Math.max(value, value2);
    }
}
