package August.backtracking;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class EightQueens {

    /*

    - Q X X
    X X X Q
    Q X X X
    X X Q X


    find first empty space and write 'Q'
    add all the invalid state
    diagonal in both direction
    row and col.

    Set<Map<Integer, Integer>> mainSet = new HashSet();

        if(row == actualRow)
        return true;


        for(int j = 0; j< col; j++) {

            if(array[i][j] =='-') {
                array[i][j] = 'Q';
                Set<Integer, Integer> set = markAllInvalidPlaces(i, j, array)
                placesEightQueens(i+ 1, array);
                array[i][j] = '-'
                unmarkAll(set)
            }
    }

    return false;


   public Set<Map<Integer, Integer>> markAllInvalidPlaces(int i, int j, int row, int col, char[][] array) {

       Set<Map<Integer, Integer>> currentInvalidSet =  new HashMap();
        // same row
        for( int p = j+1; p< col;p++) {
            if(!mainSet.contains(i,p)) {
                mainSet.put(i, p)
                currentInvalidSet.put(i, p);
            }

        // same col
        for( int p = i+1; p< row;p++) {
            if(!mainSet.contains(p, j)) {
                mainSet.put(p, j)
                currentInvalidSet.put(p, j);
            }
        }


        // diag left
        for( int p = i+1; p< row;p++) {
             for( int q = j-1; q>=0;q--) {
            if(!mainSet.contains(p, q)) {
                mainSet.put(p, q)
                currentInvalidSet.put(p, q);
            }
        }

          // diag right
        for( int p = i+1; p< row;p++) {
             for( int q = j+1; q<col;q++) {
            if(!mainSet.contains(p, q)) {
                mainSet.put(p, q)
                currentInvalidSet.put(p, q);
            }
        }
        return currentInvalidSet
   }

     */


    public boolean getEightQueensPosition(int i, int row, int col, Map<Integer, Integer> map, char[][] array) {

        if (i == array.length) {
            System.out.println("Map is: "+map);
            return true;
        }

        for (int j = 0; j < col; j++) {

            if (array[i][j] == '-') {
                array[i][j] = 'Q';
                Set<int[]> set = markAllInvalidPlaces(i, j, row, col, array);
                map.put(i, j);
                //System.out.println(map);
                boolean value = getEightQueensPosition(i + 1, row, col, map, array);
                if (value)
                    return true;
                map.remove(i);
                array[i][j] = '-';
                unmarkAll(set, array);
            }
        }


        /////
        return false;
    }

    private void unmarkAll(Set<int[]> set, char[][] array) {

        for (int[] cord : set) {
            array[cord[0]][cord[1]] = '-';
        }

    }


    public Set<int[]> markAllInvalidPlaces(int i, int j, int row, int col, char[][] array) {

        Set<int[]> currentInvalidSet = new HashSet();
        // same row
        for (int p = j + 1; p < col; p++) {
            if (array[i][p] != 'X') {
                int[] coordinates = new int[2];
                coordinates[0] = i;
                coordinates[1] = p;
                currentInvalidSet.add(coordinates);
                array[i][p] = 'X';
            }
        }
        // same col
        for (int p = i + 1; p < row; p++) {
            if (array[p][j] != 'X') {
                int[] coordinates = new int[2];
                coordinates[0] = p;
                coordinates[1] = j;
                currentInvalidSet.add(coordinates);
                array[p][j] = 'X';
            }
        }


        // diag left
        for (int p = i + 1, q= j-1; p < row && q>=0; p++,q--) {
                if (array[p][q] != 'X') {
                    int[] coordinates = new int[2];
                    coordinates[0] = p;
                    coordinates[1] = q;
                    currentInvalidSet.add(coordinates);
                    array[p][q] = 'X';
                }
        }

        // diag right
        for (int p = i + 1, q = j + 1 ; p < row && q < col; p++, q++) {

                if (array[p][q] != 'X') {
                    int[] coordinates = new int[2];
                    coordinates[0] = p;
                    coordinates[1] = q;
                    currentInvalidSet.add(coordinates);
                    array[p][q] = 'X';
                }

        }

        return currentInvalidSet;
    }

    public static void main(String s[]) {
        EightQueens eightQueens = new EightQueens();
        char[][] array = {{'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'},
                {'-', '-', '-', '-','-', '-', '-', '-'}
        };
        Map<Integer, Integer> map = new HashMap<>();
        System.out.println(eightQueens.getEightQueensPosition(0, array.length, array[0].length, map, array));

    }
}
