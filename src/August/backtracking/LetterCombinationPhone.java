package August.backtracking;

import java.util.*;

// https://leetcode.com/problems/letter-combinations-of-a-phone-number/
public class LetterCombinationPhone {

    public List<String> letterCombinations(String digits) {

        Map<String, List<String>> map = new HashMap<>();

        map.put("2", Arrays.asList("a","b","c"));
        map.put("3", Arrays.asList("d","e","f"));
        map.put("4", Arrays.asList("g","h","i"));
        map.put("5", Arrays.asList("j","k","l"));
        map.put("6", Arrays.asList("m","n","o"));
        map.put("7", Arrays.asList("p","q","r","s"));
        map.put("8", Arrays.asList("t","u","v"));
        map.put("9", Arrays.asList("w","x","y","z"));

        if(digits.isEmpty())
            return null;

        List<String> currentArray = new ArrayList();
        currentArray.add("");

        return findLetterCombination(map, currentArray, digits);
    }

    private List<String> findLetterCombination(Map<String, List<String>> map, List<String> currentArray, String digits) {

        if(digits.isEmpty()){
            System.out.println(currentArray);
            return currentArray;
        }

        String currElement = digits.substring(0,1);

        ArrayList<String> prevList = new ArrayList<>();
        prevList.addAll(currentArray);
        currentArray.clear();

        for(String letter: map.get(currElement)) {
            for(String element: prevList) {
                currentArray.add(element+letter);
            }
        }
        return findLetterCombination(map,currentArray,digits.substring(1));
    }

    public static void main(String s[]) {
        LetterCombinationPhone letterCombinationPhone = new LetterCombinationPhone();
        letterCombinationPhone.letterCombinations("23");
    }
}
