package August.backtracking;

// https://leetcode.com/problems/confusing-number-ii/

import java.io.CharConversionException;
import java.util.HashMap;
import java.util.Map;

public class ConfusingNumber2 {

    int res = 0;
    int[] array = {0, 1, 6, 8, 9};
    Map<Integer, Integer> map = new HashMap();

    ConfusingNumber2() {
        map.put(0, 0);
        map.put(1, 1);
        map.put(6, 9);
        map.put(8, 8);
        map.put(9, 6);
    }

    public int confusingNumberII(int N) {

        dfs(0, N);
        System.out.println(res);
        return res;
    }

    private void dfs(int currNum, int N) {
        if (currNum > N) {
            return;
        }

        if (currNum <= N && isConfusing(currNum)) {
            res++;
        }

        int start = currNum == 0 ? 1 : 0;
        for (int i = start; i < array.length; i++) {
            dfs(10 * currNum + array[i], N);
        }
    }

    private boolean isConfusing(int currNum) {

        String original = currNum+"";
        String newNumber = "";

        for(int i =0; i< original.length();i++){
            newNumber = map.get(Character.getNumericValue(original.charAt(i))) + newNumber;
        }

        return !original.equals(newNumber);

    }

    public static void main(String s[]) {
        ConfusingNumber2 confusingNumber2 = new ConfusingNumber2();
        confusingNumber2.confusingNumberII(100);
    }

}