package August.DynamicProgramming;

import java.util.HashMap;

public class NumberOfPI {

    public static void main(String[] args) {
        String[] number = {"31", "4", "45", "6", "1", "3"};
        numbersInPi("312", number);
    }

    public static int numbersInPi(String pi, String[] numbers) {
        // Write your code here.

        if (pi == null || numbers.length == 0) {
            return 0;
        }

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (String num : numbers) {
            map.put(num, 0);
        }


        int res = findNumberInPiRecursive(pi, 0, Integer.MAX_VALUE, map);
        return res;
    }
    // 313 == 31 1 3
    static int count = 0;

    private static int findNumberInPiRecursive(String pi, int index, int spaces, HashMap<String, Integer> map) {
        System.out.println(pi + " ------- " + (index)+"  "+(++count));
        if (map.containsKey(pi)) {
            return map.get(pi);
        }

        if (pi.isEmpty()) {
            return spaces;
        }

        if (index > pi.length()) {
            return Integer.MAX_VALUE;
        }

        String newStr = pi.substring(0, index);
        int value = Integer.MAX_VALUE;

        if (map.containsKey(newStr)) {
            int rem = findNumberInPiRecursive(pi.substring(index), 0, Integer.MAX_VALUE, map);
            if (rem != Integer.MAX_VALUE) {
                value = map.get(newStr) + 1 + rem;
            }
        }

        int value2 = findNumberInPiRecursive(pi, index + 1, spaces, map);

        int result = Math.min(value, value2);

        map.put(pi, Math.min(map.getOrDefault(pi, Integer.MAX_VALUE), result));
        return result;
    }

}
