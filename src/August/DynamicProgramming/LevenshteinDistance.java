package August.DynamicProgramming;

public class LevenshteinDistance {
    public static void main(String s[]) {
        LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
        System.out.println(levenshteinDistance.levenshteinDistance("cereal", "saturdzz"));
    }

    public static int levenshteinDistance(String str1, String str2) {
        // Write your code here.
        if(str1 == null ||str2 == null) {
            return 0;
        }
        int m = str1.length();
        int n = str2.length();// col
        if(m == 0 && n ==0) {
            return 0;
        }

        if(m == 0) {
            return n;
        }
        if( n == 0) {
            return m;
        }

        int dp[][] = new int[m+1][n+1];
        for(int i = 0; i <= n; i++) {
            dp[0][i] = i;
        }
        for(int i = 0; i <= m; i++) {
            dp[i][0] = i;
        }

        for(int i = 1; i <= m; i++) {
            for(int j = 1; j <= n; j++ ) {
                if(str1.charAt(i-1) == str2.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1];
                } else {
                    dp[i][j] = 1 + Math.min(dp[i-1][j-1], Math.min(dp[i][j-1], dp[i-1][j]));
                }
            }
        }

        for(int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                System.out.print(dp[i][j]+" ");

            }
            System.out.println();
        }
        return dp[m][n];
    }
}
