package August.DynamicProgramming;

import java.util.ArrayList;
import java.util.List;

public class LongestCommonSubsequence {

    static class Result {
        int value;
        List<Character> list = new ArrayList();

        Result(int value, List<Character> list) {
            this.value = value;
            this.list = list;
        }

    }

    public static void main(String s[]) {
        System.out.println(longestCommonSubsequence("ZXVVYZW", "XKYKZPW"));

    }

    public static List<Character> longestCommonSubsequence(String str1, String str2) {
        // Write your code here.

        if (str1 == null || str2 == null) {
            return new ArrayList<Character>();
        }

        int m = str1.length();
        int n = str2.length();

        if (m == 0 || n == 0) {
            return new ArrayList<Character>();
        }

        Result[][] dp = new Result[m + 1][n + 1];
        for (int i = 0; i <= n; i++) {
            dp[0][i] = new Result(0, new ArrayList<Character>());
        }

        for (int i = 0; i <= m; i++) {
            dp[i][0] = new Result(0, new ArrayList<Character>());
        }

        int maxValue = 0;
        List<Character> finalResult = new ArrayList<Character>();

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    List<Character> list = new ArrayList<Character>(dp[i - 1][j - 1].list);
                    list.add(str1.charAt(i - 1));
                    dp[i][j] = new Result(1 + dp[i - 1][j - 1].value, list);
                } else {
                    if (dp[i - 1][j].value >= dp[i][j - 1].value && dp[i - 1][j].value >= dp[i - 1][j - 1].value) {
                        dp[i][j] = new Result(dp[i - 1][j].value, dp[i - 1][j].list);
                    } else if (dp[i][j - 1].value >= dp[i - 1][j].value && dp[i][j - 1].value >= dp[i - 1][j - 1].value) {
                        dp[i][j] = new Result(dp[i][j - 1].value, dp[i][j - 1].list);
                    } else {
                        dp[i][j] = new Result(dp[i - 1][j - 1].value, dp[i - 1][j - 1].list);
                    }
                }

                if (dp[i][j].value > maxValue) {
                    maxValue = dp[i][j].value;
                    finalResult.clear();
                    finalResult.addAll(dp[i][j].list);
                }
            }
        }
        return finalResult;
    }


}
