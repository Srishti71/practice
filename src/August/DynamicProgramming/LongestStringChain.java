package August.DynamicProgramming;

// https://leetcode.com/problems/longest-string-chain/

/*
In:

["a","b","ba","bca","bda","bdca"]

Out: 4

single char- add it to map
[a, 1]
[b,1]
[ba,1]
[bca, 1]
[bda, 1]
[bdca, 1]

for all words:

a

"" -- break;

b

"" -- break

ba

global max = 2

a --> map.put(ba, Math.max(map.get(a)+1, map.get(ba)))

b --> map.put(ba, Math.max(map.get(b)+1, map.get(ba)))

bca

ca --> map.put(ca, Math.max(map.get(ca)+1, map.get(bca))) == 1
ba -->
 */


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LongestStringChain {

    Map<String, Integer> map = new HashMap();

    public int longestStrChain(String[] words) {
        int globalMin = Integer.MIN_VALUE;
        if (words.length > 0) {
            globalMin = 1;
        }
        for (String word : words) {
            map.put(word, 1);
        }

        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                String newWord = word.substring(0, i) + word.substring(i + 1);
                System.out.println("Word = " + word + " newWord= " + newWord);
                int newPredecessorCount = Math.max(map.get(word), map.getOrDefault(newWord, 0) + 1);
                map.put(word, newPredecessorCount);
                globalMin = Math.max(globalMin, newPredecessorCount);
            }
        }
        return globalMin;
    }

    public static void main(String s[]) {
        LongestStringChain longestStringChain = new LongestStringChain();
        String array[] = {"a", "bca","c", "b", "bda", "ba", "bdca"};
        Arrays.sort(array, (a,b) -> {
            if(a.length() < b.length()){
                return -1;
            } else if(a.length() > b.length()) {
                return 1;
            } else{
                return 0;
            }
        });

        System.out.println(longestStringChain.longestStrChain(array));

    }

}


