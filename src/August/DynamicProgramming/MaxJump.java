package August.DynamicProgramming;

public class MaxJump {

    public static int minNumberOfJumps(int[] array) {
        // Write your code here.

        int n = array.length;
        if (n == 0 || n == 1) {
            return 0;
        }

        int maxReach = array[0];
        int jump = 1;
        int step = array[0];
        for (int i = 1; i < n; i++) {


            maxReach = Math.max(i + array[i], maxReach);
            step -= 1;

            if (step == 0) {
                jump++;
                step = maxReach - i;
            }

        }
        return jump;
    }

}
