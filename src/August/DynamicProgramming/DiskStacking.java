package August.DynamicProgramming;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DiskStacking {

    public static void main(String args[]) {
        List<Integer[]> list = new ArrayList<>();


        /*

        [3, 3, 4],
    [2, 1, 2],
    [3, 2, 3],
    [2, 2, 8],
    [2, 3, 4],
    [5, 5, 6],
    [1, 2, 1],
    [4, 4, 5],
    [1, 1, 4],
    [2, 2, 3]

         */
        Integer[] array1 = {3, 3, 4};
        list.add(array1);

        Integer[] array2 = {2, 1, 2};
        list.add(array2);

        Integer[] array3 = {3, 2, 3};
        list.add(array3);

        Integer[] array4 = {2, 2, 8};
        list.add(array4);

        Integer[] array5 = {2, 3, 4};
        list.add(array5);

        Integer[] array6 = {5, 5, 6};
        list.add(array6);

        Integer[] array7 = {1, 2, 1};
        list.add(array7);

        Integer[] array8 = {4, 4, 5};
        list.add(array8);

        Integer[] array9 = {1, 1, 4};
        list.add(array9);

        Integer[] array10 = {2, 2, 3};
        list.add(array10);

        diskStacking(list);
    }

    static class DiskStack {
        int height;
        List<Integer[]> disks;

        DiskStack(int height, List<Integer[]> disks) {
            this.height = height;
            this.disks = disks;
        }
    }

    public static List<Integer[]> diskStacking(List<Integer[]> disks) {
        // Write your code here.

        if (disks == null)
            return new ArrayList<Integer[]>();

        int size = disks.size();

        if (size == 1) {
            return disks;
        }



        Collections.sort(disks, (a, b) -> {

            if (a[0] < b[0] && a[1] < b[1] && a[2] < b[2]) {
                return -1;
            } else if (a[0] > b[0] && a[1] > b[1] && a[2] > b[2]) {
                return 1;
            } else if (a[2] > b[2]) {
                return 1;
            } else if (a[2] < b[2]) {
                return -1;
            } else if (a[2] == b[2]) {
                if (a[0] <= b[0] && a[1] <= b[1]) {
                    return -1;
                } else if (a[0] >= b[0] && a[1] >= b[1]) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        });

        DiskStack[] diskStack = new DiskStack[size];
        List<Integer[]> initialDisk = new ArrayList<Integer[]>();
        initialDisk.add(disks.get(0));

        int maxHeight = disks.get(0)[2];
        List<Integer[]> maxDiskStack = initialDisk;

        diskStack[0] = new DiskStack(maxHeight, initialDisk);

        for(Integer[] values: disks ) {
            for(int value: values) {
                System.out.print(value + " ");
            }
            System.out.println();
        }



        for (int i = 1; i < size; i++) {
            for (int j = i - 1; j >= 0; j--) {

                Integer[] diskI = disks.get(i);
                Integer[] diskJ = disks.get(j);
                DiskStack prevDiskStack = diskStack[j];

                if (diskStack[i] == null) {
                    List<Integer[]> currList = new ArrayList<Integer[]>();
                    currList.add(diskI);
                    diskStack[i] = new DiskStack(diskI[2], currList);

                }

                if ((diskI[2] + prevDiskStack.height) > diskStack[i].height) {
                    if (diskI[0] > diskJ[0] && diskI[1] > diskJ[1] && diskI[2] > diskJ[2]) {
                        diskStack[i].height = diskI[2] + prevDiskStack.height;
                        diskStack[i].disks.clear();
                        diskStack[i].disks.addAll(diskStack[j].disks);
                        diskStack[i].disks.add(diskI);
                    }
                }

                if (maxHeight < diskStack[i].height) {
                    maxHeight = diskStack[i].height;
                    maxDiskStack = diskStack[i].disks;
                }
            }

        }

        // [[3, 3, 4], [3, 3, 4], [4, 4, 5], [5, 5, 6]]
        System.out.println("Result----------");
        for(DiskStack allDisk: diskStack ) {
            for(Integer[] value: allDisk.disks) {
                for(int v: value)
                    System.out.print(v + " ");

                System.out.println();
            }

            System.out.println("    ");

        }

        return maxDiskStack;
    }
}