package August.DynamicProgramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaxSumIncreasingSubSequence {

    static class Result {
        int value;
        List<Integer> list;

        Result(int value, List<Integer> list) {
            this.value = value;
            this.list = list;
        }
    }

    public static List<List<Integer>> maxSumIncreasingSubsequence(int[] array) {

        int length = array.length;

        if (length == 0) {
            return null;
        }

        if (length == 1) {
            return new ArrayList<List<Integer>>() {
                {
                    add(List.of(array[0])); // Example max sum
                    add(List.of(array[0])); // Example max sequence
                }
            };
        }

        Result[] result = new Result[length];
        result[0] = new Result(array[0], Arrays.asList(array[0]));

        int maxValue = array[0];
        List<Integer> resultList = new ArrayList<Integer>();
        resultList.add(maxValue);

        for (int i = 1; i < length; i++) {
            List<Integer> list1 = new ArrayList<Integer>();
            list1.add(array[i]);
            result[i] = new Result(array[i], list1);
            for (int j = i - 1; j >= 0; j--) {
                if (array[j] < array[i]) {
                    if (result[i].value < result[j].value + array[i]) {
                        result[i].value = result[j].value + array[i];
                        result[i].list.clear();
                        result[i].list.addAll(result[j].list);
                        result[i].list.add(array[i]);
                    }
                }
            }

            if (maxValue < result[i].value) {
                maxValue = result[i].value;
                resultList = result[i].list;
            }
        }

        final int value = maxValue;
        final List<Integer> res = new ArrayList<Integer>(resultList);

        return new ArrayList<List<Integer>>() {
            {
                add(List.of(value)); // Example max sum
                add(res); // Example max sequence
            }
        };
    }

    public static void main(String str[]) {
        System.out.println(maxSumIncreasingSubsequence(new int[]{-5, -4, -3, -2, -1}));
    }

}
