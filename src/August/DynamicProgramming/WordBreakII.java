package August.DynamicProgramming;

// https://leetcode.com/problems/word-break-ii/


import java.util.*;

public class WordBreakII {


    public List<String> wordBreak(String s, List<String> dict) {
        Set<String> set = new HashSet<>(dict);
        Set<String> result = wordBreakTwoUtil(s, set);
        if(result == null)
            return new ArrayList();
        return new ArrayList(result);
    }

    private Set<String> wordBreakTwoUtil(String s, Set<String> set) {
        int len = s.length();
        Set<String>[] dp = new HashSet[len+1];
        dp[0] = new HashSet();

        for( int i = 1; i<= len; i++) {
            for(int j = i-1; j>=0; j--) {
                if(dp[j] != null) {
                    String curr = s.substring(j, i);
                    if(!curr.isEmpty() && set.contains(curr)) {
                       if(dp[i] == null) {
                           dp[i] = addNewStringToSet(curr, dp[j]);
                       } else {
                           dp[i].addAll(addNewStringToSet(curr, dp[j]));
                       }
                    }
                }
            }
        }
        return dp[len];
    }

    private Set<String> addNewStringToSet(String curr, Set<String> strings) {
        Set<String> newSet = new HashSet<>();
        if(strings.isEmpty()) {
            newSet.add(curr);
            return newSet;
        }
        for(String s: strings) {
            if(!s.isEmpty()) {
                newSet.add(s+" "+curr);
            }
        }
        return newSet;
    }

    public static void main(String s[]) {
        WordBreakII word = new WordBreakII();
        System.out.println(word.wordBreak("catsanddog", Arrays.asList("cat", "cats", "and", "sand", "dog")));
    }
}
