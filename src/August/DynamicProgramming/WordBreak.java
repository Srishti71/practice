package August.DynamicProgramming;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class WordBreak {

    public boolean canBreak(int start, int end, String originalStr, List<String> words) {

        if (start == end) {
            return true;
        } else if (start <= end && end <= originalStr.length()) {
            String newStr = originalStr.substring(start, end);
            if (words.contains(newStr)) {
                String rem = originalStr.substring(end);
                if (rem.equals("") || words.contains(rem) || canBreak(0, 1, rem, words)) {
                    return true;
                }
            }
            return canBreak(start, end + 1, originalStr, words);

        } else {
            return false;
        }
    }

    public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet(wordDict);
        if (s == null)
            return false;
        int len = s.length();
        if (len == 1) {
            return set.contains(s);
        }
        boolean[] dp = new boolean[len + 1];
        dp[0] = true;
        for (int i = 1; i <= len; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (dp[j]) {
                    if (set.contains(s.substring(j, i))) {
                        dp[i] = true;
                        break;
                    }
                }
            }
        }
        return dp[len];
    }

    public static void main(String s[]) {
        WordBreak wordBreak = new WordBreak();
        List<String> words = new ArrayList();
        words.add("cats");
        words.add("and");
        words.add("god");
        System.out.println(wordBreak.wordBreak("catsandgo", words));
    }
}