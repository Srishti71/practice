package August.DynamicProgramming;

import java.util.*;

public class WordBreakII_DFS {

    public List<String> wordBreak(String s, Set<String> wordDict) {
        return DFS(s, wordDict, new HashMap<String, LinkedList<String>>());
    }

    private List<String> DFS(String s, Set<String> wordDict, HashMap<String, LinkedList<String>>
            map) {

        if (map.containsKey(s)) {
            return map.get(s);
        }

        LinkedList<String> res = new LinkedList<>();

        if (s.isEmpty()) {
            res.add("");
            return res;
        }

        for (String word : wordDict) {
            if (word.startsWith(s)) {
                List<String> sub = DFS(s.substring(s.length()), wordDict, map);
                for (String w : sub) {
                    res.add(word + (w.isEmpty() ? "" : " ") + w);
                }
            }
        }
        map.put(s, res);
        return res;

    }


    public static void main(String s[]) {
        WordBreakII_DFS word = new WordBreakII_DFS();
        List<String> list = Arrays.asList("cat", "cats", "and", "sand", "dog");
        System.out.println(word.wordBreak("catsanddog",new HashSet(list) ));
    }

}
