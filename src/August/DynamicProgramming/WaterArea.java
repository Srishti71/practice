package August.DynamicProgramming;

public class WaterArea {

    public static void main(String args[]) {
        WaterArea waterArea = new WaterArea();
        int[] heights = {0, 8, 0, 0, 5, 0, 0, 10, 0, 0, 1, 1, 0, 3};
        waterArea.waterArea(heights);
    }

    public static int waterArea(int[] heights) {

            if (heights.length == 0 || heights.length == 1) {
                return 0;
            }

            int len = heights.length;
            int left[] = new int[len];
            int right[] = new int[len];

            int maxLeft = heights[0];
            for (int i = 1; i < len; i++) {
                maxLeft = Math.max(maxLeft, heights[i-1]);
                left[i] = maxLeft;
            }

            int maxRight = heights[len-1];
            for (int i = len-2; i >= 0; i--) {
                maxRight = Math.max(maxRight, heights[i+1]);
                right[i] = maxRight;
            }

            int globalSum = 0;
            for(int i =0; i< len; i++) {
                int temp = Math.min(left[i], right[i]);
                globalSum +=  heights[i] > temp ? 0 : temp - heights[i];
            }

            return globalSum;
    }
}
