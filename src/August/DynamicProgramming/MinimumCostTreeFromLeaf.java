package August.DynamicProgramming;

import April_18.MinimumNumberOfJumps;

// https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/

public class MinimumCostTreeFromLeaf {

    class Result {
        int res;
        int maxMul;

        Result(int res, int maxMul) {
            this.res = res;
            this.maxMul = maxMul;
        }
    }

    public static void main(String s[]) {
        MinimumCostTreeFromLeaf minimumCostTreeFromLeaf = new MinimumCostTreeFromLeaf();
        int[] arr = {6, 2, 4};
        minimumCostTreeFromLeaf.mctFromLeafValues(arr);
    }

    Result[][] dp;

    public int mctFromLeafValues(int[] arr) {
        int length = arr.length;
        dp = new Result[length][length];
        int res = findMaxCostFromLeaf(arr, 0, length - 1).res;
        System.out.println(res);
        return res;
    }

    private Result findMaxCostFromLeaf(int[] arr, int i, int j) {

        if (i == j) return new Result(0, arr[i]);
        if (dp[i][j] != null) return dp[i][j];
        if (i + 1 == j) {
            return new Result(arr[i] * arr[j] , Math.max(arr[i], arr[j]));
        }
        int min = Integer.MAX_VALUE;
        int m = Integer.MAX_VALUE;
        for (int k = i; k < j; k++) {
            Result temp1 = findMaxCostFromLeaf(arr, i, k);
            Result temp2 = findMaxCostFromLeaf(arr, k + 1, j);
            int count = temp1.res + temp2.res + temp1.maxMul * temp2.maxMul;
            if (min > count) {
                min = count;
                m = Math.max(temp1.maxMul, temp2.maxMul);
            }
        }
        Result p = new Result(min, m);
        dp[i][j] = p;
        return p;
    }
}
