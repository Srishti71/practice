package August.DynamicProgramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class UniqueBinarySearchTree {

    HashMap<String, List<List<Integer>>> dp; // {{1,2, 3, 4,5}, {3, 2,1,  4,5}

    public int numTrees(int n) {
        dp = new HashMap();
        if(n<=0) {
            return 0;
        }
        else if(n == 1) {
            return 1;
        }
        List<List<Integer>> result = generateTree(1, n, dp);
        System.out.println(result);
        System.out.println(result.size());
        return result.size();
    }

    private List<List<Integer>> generateTree(int start, int end, HashMap<String, List<List<Integer>>> dp) {

        if (dp.containsKey(start + "-" + end)) {
            return dp.get(start + "-" + end);
        }

        if (start == end) {
            List<List<Integer>> list = new ArrayList<List<Integer>>();
            list.add(Arrays.asList(start));
            return list;
        }
        if (start > end) {
            return null;
        }

        List<List<Integer>> result = null;
        for (int i = start; i <= end; i++) { // i =1 start = 1 end = 1
            List<List<Integer>> leftTree = generateTree(start, i - 1, dp); //
            List<List<Integer>> rightTree = generateTree(i + 1, end, dp);

            if (leftTree != null && rightTree != null) {
                for (List<Integer> leftList : leftTree) {
                    for (List<Integer> rightList : rightTree) {
                        if (result == null) {
                            result = new ArrayList<>();
                        }
                        List<Integer> curr = new ArrayList<>();
                        curr.add(i);
                        curr.addAll(leftList);
                        curr.addAll(rightList);
                        result.add(curr);
                    }
                }
            } else if (leftTree == null && rightTree != null) {
                for (List<Integer> rightList : rightTree) {
                    if (result == null) {
                        result = new ArrayList<>();
                    }
                    List<Integer> curr = new ArrayList<>();
                    curr.add(i);
                    curr.addAll(rightList);
                    result.add(curr);
                }
            } else if (rightTree == null && leftTree != null) {
                for (List<Integer> leftList : leftTree) {
                    if (result == null) {
                        result = new ArrayList<>();
                    }
                    List<Integer> curr = new ArrayList<>();
                    curr.add(i);
                    curr.addAll(leftList);
                    result.add(curr);
                }
            }


        }

        dp.put(start + "-" + end, result);
        return result;
    }

    public static void main(String s[]) {
        UniqueBinarySearchTree uniqueBinarySearchTree = new UniqueBinarySearchTree();
        uniqueBinarySearchTree.numTrees(5);
        uniqueBinarySearchTree.findNumber(5);
    }

    public void findNumber (int n) {
        int dp[] = new int[n + 1];
        Arrays.fill(dp, 0);

        // Base case
        dp[0] = 1;
        dp[1] = 1;

        for (int i = 2; i <= n; i++)
        {
            for (int j = 1; j <= i; j++)
            {

                // n-i in right * i-1 in left
                dp[i] = dp[i] + (dp[i - j] *
                        dp[j - 1]);
            }
        }
        System.out.println(dp[n]);
    }
}

/*
1... n

1 --> root
<1 --> startBuildingRecursively

>1 .. n --> startBuildingRecursively

        n =5

3

1.. 2 --> startBuildingRecursively
        {{1,2}, {2,1} } -- preorder

4..5 --> startBuildingRecursively
        {{4, 5}, {5, 4}}

        3
           left = {1,2}
           right = {4, 5}

           or
        3
        left = {2, 1}
        right = {4, 5}

        or

        3
        left = {1, 2}
        right = {5, 4}

        or

        3
        left = {2, 1}
        right = {5, 4}

        or


for(left: lefts) {
    for(right: rights){
        Root root = new Root(3);
        root.left = left;
        root.right = right;
        solution.add(root);

        }
        }

*/  
