package August.Mock;

public class WaterSupplyRecursive {

    int globalMin = Integer.MAX_VALUE;

    public int findMinimumCostOfWaterSupply(int[] source_cost, int[][] pipe_cost) {

        int n = source_cost.length;

        int value = helper(source_cost, pipe_cost, 0, new boolean[n]);

        return -1;

    }


        public int helper(int[] source_cost, int[][] pipe_cost, int index, boolean[] wellbuilt) {
            if (index == source_cost.length) {
                return 0;
            }
            int n = source_cost.length;
            int buildWellAtIndex = 0;
            int minCostPipe = Integer.MAX_VALUE;
            int totalCost = 0;
            for (int i = index; i < n; i++) {
                if(!wellbuilt[i]) {
                    wellbuilt[i] = true;
                    buildWellAtIndex = helper(source_cost, pipe_cost, i + 1, wellbuilt) + source_cost[i];

                    wellbuilt[i] = false;
                    for (int j = 0; j < n; j++) {
                        if (j == i || !wellbuilt[i]) {
                            continue;
                        } else {
                            wellbuilt[j] = true;
                            minCostPipe = Math.min(minCostPipe, helper(source_cost, pipe_cost, i + 1, wellbuilt) + pipe_cost[i][j] ) ;
                        }
                    }
                    int result = Math.min(minCostPipe, buildWellAtIndex);
                    totalCost +=result;
                }


               // System.out.println("Result is: " + result);
            }

            System.out.println("Total Cost: "+totalCost);
            return 0;
        }


    public static void main(String[] args) {
        WaterSupplyRecursive waterSupplyRecursive = new WaterSupplyRecursive();

        int[] source_cost = {1, 5, 3};

        int[][] pipe_cost = {{0, 6, 1}, // 2
                {6, 0, 2},
                {1, 2, 0}};
        waterSupplyRecursive.findMinimumCostOfWaterSupply(source_cost, pipe_cost);
    }
}
