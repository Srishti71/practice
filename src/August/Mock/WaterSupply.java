package August.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
// August 23rd

//
public class WaterSupply {

    static public void main(String args[]) {

        WaterSupply obj = new WaterSupply();

        int[] source_cost = {6};

        int[][] pipe_cost = {{0}};

        System.out.println(obj.findMinimumCostOfWaterSupply(source_cost, pipe_cost));
    }


    class House {

        int source_cost_value;
        int position;

        House(int source_cost_value, int position) {
            this.source_cost_value = source_cost_value;
            this.position = position;
        }
    }


    public House[] sortHouses(int[] source_cost) {
        int n = source_cost.length;

        House[] houses = new House[n];

        for (int i = 0; i < n; i++) {
            houses[i] = new House(source_cost[i], i);
        }

        Arrays.sort(houses, (a, b) -> a.source_cost_value - b.source_cost_value);

        return houses;
    }

    public int findMinimumCostOfWaterSupply(int[] source_cost, int[][] pipe_cost) {


        int noOfHouses = source_cost.length;

        if (noOfHouses == 1) {
            return source_cost[0];
        }

        House[] houses = sortHouses(source_cost);
        int totalCost = 0;

        Set<Integer> visited = new HashSet();

        for (House house : houses) {
            if (!visited.contains(house.position)) {

                totalCost += house.source_cost_value;
                visited.add(house.position);

                int[] pipeCostAndIndex = pipeCostAndIndexForNextNode(house, visited, pipe_cost, noOfHouses);

                totalCost += Math.min(source_cost[pipeCostAndIndex[1]], pipeCostAndIndex[0]); // node: 0 --> 1, 0--> 2, 3--> 2, 2

                visited.add(pipeCostAndIndex[1]);

            }

        }
        return totalCost;

    }


    public int[] pipeCostAndIndexForNextNode(House currentHouse, Set<Integer> visited, int[][] pipe_cost, int noOfHouses) {
        int minPipeCost = Integer.MAX_VALUE;
        int minNextNodeIndex = 0;
        int currNode = currentHouse.position;

        for (int i = 0; i < noOfHouses; i++) {
            if (i == currNode || visited.contains(i)) {
                continue;
            }

            if (minPipeCost > pipe_cost[currNode][i]) {
                minPipeCost = pipe_cost[currNode][i];
                minNextNodeIndex = i;
            }
        }

        return new int[]{minPipeCost, minNextNodeIndex};

    }
}


