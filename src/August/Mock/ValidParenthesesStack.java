package August.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ValidParenthesesStack {
    public String removeOuterParentheses(String S) {
        if (S == null || S.length() == 0) {
            return "";
        }

        int len = S.length();
        List<Integer> value = new ArrayList();
        int prev = 1;
        int curr;
        for (int i = 1; i < len; i++) {
            if (S.charAt(i) == '(') {
                curr = prev + 1;
            } else {
                curr = prev - 1;
            }
            if (curr == 0) {
                value.add(i);
            }
            prev = curr;
        }

        System.out.println(value);
        prev = 0;
        String res = "";
        for (int ele : value) {
            res += S.substring(prev + 1, ele);
            prev = ele + 1;
        }

        System.out.println(res);
        return res;
    }

    public static void main(String[] args) {
        ValidParenthesesStack validParen = new ValidParenthesesStack();
        validParen.removeOuterParentheses("(()())(())");
    }
}
