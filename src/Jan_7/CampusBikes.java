package Jan_7;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

class DistanceBetweenBikes {
    int workerIndex, bikeIndex, distance;

    public DistanceBetweenBikes(int workerIndex, int bikeIndex, int distance) {
        this.workerIndex = workerIndex;
        this.bikeIndex = bikeIndex;
        this.distance = distance;
    }
}

public class CampusBikes {

    void findDistanceBetweenBikesAndWorkers(int[][] workers, int[][] bikes) {

        PriorityQueue<DistanceBetweenBikes> queue = new PriorityQueue(new Comparator<DistanceBetweenBikes>() {
            @Override
            public int compare(DistanceBetweenBikes distanceBetweenBikes, DistanceBetweenBikes t1) {
                if (distanceBetweenBikes.distance != t1.distance)
                    return (distanceBetweenBikes.distance - t1.distance);
                else if (distanceBetweenBikes.workerIndex != t1.workerIndex)
                    return distanceBetweenBikes.workerIndex - t1.workerIndex;
                else
                    return (distanceBetweenBikes.bikeIndex - t1.bikeIndex);
            }
        });


        for (int i = 0; i < workers.length; i++) {
            for (int j = 0; j < bikes.length; j++) {
                int distance = Math.abs(workers[i][0] - bikes[j][0]) + Math.abs(workers[i][1] - bikes[j][1]);
                queue.add(new DistanceBetweenBikes(i, j, distance));
            }
        }

        int ans[] = new int[workers.length];
        Arrays.fill(ans, -1);
        while (!queue.isEmpty()) {

            DistanceBetweenBikes distanceBetweenBikes = queue.poll();
            int workerIndex = distanceBetweenBikes.workerIndex;

            if (ans[workerIndex] == -1 && !contains(ans, distanceBetweenBikes.bikeIndex)) {
                ans[workerIndex] = distanceBetweenBikes.bikeIndex;
            }
        }

        printAnswer(ans);

    }

    private boolean contains(int[] ans, int bikeIndex) {
        for (int i = 0; i < ans.length; i++) {
            if (ans[i] == bikeIndex)
                return true;
        }
        return false;
    }

    private void printAnswer(int[] ans) {

        for (int i = 0; i < ans.length; i++) {

            System.out.print(ans[i] + " ");
        }
    }

    public static void main(String s[]) {
        CampusBikes campusBikes = new CampusBikes();
        int workers[][] = {{0, 0}, {1, 1}, {2, 0}};
        int bikes[][] = {{1, 0}, {2, 2}, {2, 1}};
        campusBikes.findDistanceBetweenBikesAndWorkers(workers, bikes);
    }
}
