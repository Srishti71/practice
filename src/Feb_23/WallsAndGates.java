package Feb_23;

public class WallsAndGates {


    public static void main(String s[]) {

        int INF = Integer.MAX_VALUE;
        int[][] rooms = {{INF, -1, 0, INF},
                {INF, INF, INF, -1},
                {INF, -1, INF, -1},
                {0, -1, INF, INF}};

        wallsAndGates(rooms);

        print(rooms);
    }

    private static void print(int[][] rooms) {

        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[0].length; j++) {
                System.out.println(rooms[i][j]);
            }
        }
    }

    public static void wallsAndGates(int[][] rooms) {

        int m = rooms.length, n = rooms[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (rooms[i][j] == 0) {
                    boolean[][] visited = new boolean[m][n];
                    //findDistanceToAllPlace(rooms, i, j, visited, 0, true);


                    findDistanceToAllPlace(rooms, i + 1, j, visited,  1, false);
                    findDistanceToAllPlace(rooms, i - 1, j, visited,  1, false);
                    findDistanceToAllPlace(rooms, i, j + 1, visited, 1, false);
                    findDistanceToAllPlace(rooms, i, j - 1, visited, 1, false);
                }
            }
        }

    }

    private static void findDistanceToAllPlace(int[][] rooms, int i, int j, boolean[][] visited, int dist, boolean first) {

        if (i >= rooms.length || j >= rooms[0].length || i < 0 || j < 0 ||  rooms[i][j] < dist) {
            return;
        }

        visited[i][j] = true;
        rooms[i][j] = Math.min(rooms[i][j], dist);

        findDistanceToAllPlace(rooms, i + 1, j, visited, dist + 1, false);
        findDistanceToAllPlace(rooms, i - 1, j, visited, dist + 1, false);
        findDistanceToAllPlace(rooms, i, j + 1, visited, dist + 1, false);
        findDistanceToAllPlace(rooms, i, j - 1, visited, dist + 1, false);

    }
}