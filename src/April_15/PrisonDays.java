package April_15;

import java.util.*;

public class PrisonDays {
    public static void main(String s[]) {
        PrisonDays prison = new PrisonDays();
        int[] cells = {1,0,0,1,0,0,1,0};
        int N = 1000000000;
        int[] result = prison.prisonAfterNDays(cells, N);
        System.out.println(Arrays.toString(result));
    }

    public int[] prisonAfterNDays(int[] cells, int N) {
        Set<String> set = new HashSet<>();
        //List<int[]> list = new ArrayList<>();
        int block = 0;
        boolean flag = false;

        for (int i = 0; i < N; i++) {
            int[] array = getNextDay(cells);
            String s = Arrays.toString(array);
            if (set.contains(s)) {

                flag = true;
                break;
            }
            set.add(s);
            block++;
            //list.add(array);
            cells = array;
        }


        if (flag) {
            N = N % block;
            for(int i =1; i<= N ; i++) {
                cells = getNextDay(cells);
            }
        }
        return cells;
    }

    private int[] getNextDay(int[] cells) {
        int size = cells.length;

        int result[] = new int[size];

        for (int i = 1; i < size - 1; i++) {
            if (cells[i - 1] == cells[i + 1])
                result[i] = 1;
        }
        return result;
    }
}
