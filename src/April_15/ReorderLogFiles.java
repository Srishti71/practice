package April_15;

/*
https://leetcode.com/problems/reorder-data-in-log-files/

You have an array of logs.  Each log is a space delimited string of words.

For each log, the first word in each log is an alphanumeric identifier.  Then, either:

Each word after the identifier will consist only of lowercase letters, or;
Each word after the identifier will consist only of digits.
We will call these two varieties of logs letter-logs and digit-logs.  It is guaranteed that each log has at least one word after its identifier.

Reorder the logs so that all of the letter-logs come before any digit-log.
The letter-logs are ordered lexicographically ignoring identifier, with the identifier used in case of ties.
The digit-logs should be put in their original order.

Return the final order of the logs.

Example 1:

Input: logs = ["dig1 8 1 5 1","let1 art can","dig2 3 6","let2 own kit dig","let3 art zero"]
Output: ["let1 art can","let3 art zero","let2 own kit dig","dig1 8 1 5 1","dig2 3 6"]

 */

import java.util.PriorityQueue;

public class ReorderLogFiles {

    public static void main(String s[]) {

        String[] logs = {"dig1 8 1 5 1", "let1 art can", "dig2 3 6", "let2 own kit dig", "let3 art zero"};
        PriorityQueue<String> queue = new PriorityQueue<>((a, b) -> {
            String first[] = a.split(" ");
            String second[] = b.split(" ");

            boolean firstDigit = Character.isDigit(first[1].charAt(0));
            boolean secondDigit = Character.isDigit(second[1].charAt(0));
            if (!firstDigit && !secondDigit) {
                int cmp = first[1].compareTo(second[1]);
                if (cmp != 0) return cmp;
                return first[0].compareTo(second[0]);
            }
            return firstDigit ? (secondDigit ? 0 : 1) : -1;
        });

        for (String log : logs) {
            queue.add(log);
        }

        String[] result = new String[queue.size()];
        int i = 0;
        while (!queue.isEmpty()) {
            result[i] = queue.poll();
            System.out.println(result[i]);
            i++;
        }

    }


}
