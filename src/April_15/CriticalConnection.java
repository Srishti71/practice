package April_15;

import java.util.*;

public class CriticalConnection {

    public static void main(String s[]) {
        CriticalConnection cc = new CriticalConnection();
        int n = 5;
        List<List<Integer>> connections = new ArrayList<>();
        connections.add(Arrays.asList(1, 0)); //[[0,1],[1,2],[2,0],[1,3]]
        connections.add(Arrays.asList(2, 0)); //[[0,1],[1,2],[2,0],[1,3]]
        connections.add(Arrays.asList(3, 2)); //[[0,1],[1,2],[2,0],[1,3]]
        connections.add(Arrays.asList(4, 2)); //[[0,1],[1,2],[2,0],[1,3]]
        connections.add(Arrays.asList(4, 3)); //[[0,1],[1,2],[2,0],[1,3]]
        connections.add(Arrays.asList(3, 0)); //[[0,1],[1,2],[2,0],[1,3]]
        connections.add(Arrays.asList(4, 0)); //[[0,1],[1,2],[2,0],[1,3]]
        cc.criticalConnections(n, connections);
    }

    int parent[], low[], disc[];
    int id = 0;
    Set<List<Integer>> result = new HashSet<>();

    public Set<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        buildGrap(connections, map);
        parent = new int[n];
        low = new int[n];
        disc = new int[n];

        for (int i = 0; i < n; i++) {
            parent[i] = -1;
        }
        Set<Integer> seen = new HashSet<>();
        System.out.println(map);
        for (int i = 0; i < n; i++) {

            if (!seen.contains(i)) {
                dfs(map, i, seen);
            }

        }

        System.out.println(result);
        return result;

    }

    private void dfs(Map<Integer, List<Integer>> map, int i, Set<Integer> seen) {
        low[i] = disc[i] = id++;
        int child = 0;
        seen.add(i);

        for (Integer adj : map.getOrDefault(i, new ArrayList<>())) {

            if (!seen.contains(adj)) {
                parent[adj] = i;
                child++;
                dfs(map, adj, seen);

                low[i] = Math.min(low[i], low[adj]);

                if (disc[i] < low[adj]) {
                    result.add(Arrays.asList(i, adj));
                }

            } else if (adj != parent[i]) {
                low[i] = Math.min(disc[adj], low[i]);
            }

        }
    }

    public void buildGrap(List<List<Integer>> connections, Map<Integer, List<Integer>> map) {

        for (List<Integer> connect : connections) {
            List<Integer> list = map.getOrDefault(connect.get(0), new ArrayList<>());
            list.add(connect.get(1));

            map.put(connect.get(0), list);
            List<Integer> list1 = map.getOrDefault(connect.get(1), new ArrayList<>());
            list1.add(connect.get(0));
            map.put(connect.get(1), list1);
        }
    }
}
