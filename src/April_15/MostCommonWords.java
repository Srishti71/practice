package April_15;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MostCommonWords {

    public static void main(String s[]) {
        MostCommonWords most = new MostCommonWords();
        String paragraph = "Bob. hIt, baLl";
        String[] banned = {"bob", "hit"};
        most.mostCommonWord(paragraph, banned);
    }

    public String mostCommonWord(String paragraph, String[] banned) {
        if (paragraph == null || paragraph.isEmpty())
            return "";

        paragraph = paragraph.replaceAll("[^a-zA-Z ]", " ");
        Set<String> set = new HashSet();
        for (String bannedWord : banned) {
            set.add(bannedWord.toLowerCase());
        }

        Map<String, Integer> map = new HashMap();
        String[] paragraphs = paragraph.split(" ");
        System.out.println("Set: " + set);
        for (String word : paragraphs) {
            String w = word.toLowerCase().trim();
            if (!w.isEmpty()) {

                if (set.contains(w))
                    continue;
                int frequency = map.getOrDefault(w, 0);
                map.put(w, frequency + 1);
            }
        }

        System.out.println(map);
        int max = Integer.MIN_VALUE;
        String result = "";
        for (String key : map.keySet()) {
            if (max < map.get(key)) {
                max = map.get(key);
                result = key;
            }
        }
        System.out.println(result);
        return result;
    }
}
