package June_20;

import java.util.ArrayList;
import java.util.List;

public class SubArray {
    List<List<Integer>> set = new ArrayList<>();

    public static void main(String s[]) {
        SubArray sub = new SubArray();
        int[] nums = {1, 2, 3};
        sub.generateSubArrays(new ArrayList<>(), 0, nums);
        System.out.println(sub.set);
    }

    public void generateSubArrays(List<Integer> subArray, int index, int[] nums) {
        set.add(new ArrayList(subArray));

        if (index == nums.length) {
            return;
        }
        
        for (int i = index; i < nums.length; i++) {
            subArray.add(nums[i]);
            generateSubArrays(subArray, i + 1, nums);
            subArray.clear();
        }
    }
}
