package April_8;

import java.util.PriorityQueue;

public class MinimumMaximumPath {

    static int maxScore = Integer.MIN_VALUE;

    public int maximumMinimumPath(int[][] A) {

        int row = A.length;
        int col = A[0].length;
        boolean[][] visited = new boolean[row][col];
        int[][] dirs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
        PriorityQueue<int[]> queue = new PriorityQueue<>((a, b) -> b[2] - a[2]);
        queue.add(new int[]{0, 0, A[0][0]});
        visited[0][0] = true;

        while (!queue.isEmpty()) {

            int[] ele = queue.poll();

            if (ele[0] == row - 1 && ele[1] == col - 1) return ele[2];

            for (int[] dir : dirs) {
                int newX = ele[0] + dir[0];
                int newY = ele[1] + dir[1];

                if (newX < 0 || newY < 0 || newX >= A.length || newY >= A[0].length || visited[newX][newY])
                    continue;
                queue.add(new int[]{newX, newY, Math.min(ele[2], A[newX][newY])});
                visited[newX][newY] = true;
            }

        }
        return -1;
    }

    private void maximumMinimumPathDFS(int[][] a, int row, int col, int minValue, boolean[][] visited) {

        if (isValid(a, row, col, visited)) {
            if (row == a.length - 1 && col == a[0].length - 1) {
                minValue = Math.min(minValue, a[row][col]);
                maxScore = Math.max(minValue, maxScore);
            }
            visited[row][col] = true;

            maximumMinimumPathDFS(a, row + 1, col, Math.min(minValue, a[row][col]), visited);
            maximumMinimumPathDFS(a, row - 1, col, Math.min(minValue, a[row][col]), visited);
            maximumMinimumPathDFS(a, row, col + 1, Math.min(minValue, a[row][col]), visited);
            maximumMinimumPathDFS(a, row, col - 1, Math.min(minValue, a[row][col]), visited);

            //  visited[row][col] = false;
        }
    }

    public boolean isValid(int[][] a, int row, int col, boolean[][] visited) {
        if (row < 0 || col < 0 || row >= a.length || col >= a[0].length || visited[row][col]) {
            return false;
        }
        return true;
    }

    public static void main(String s[]) {
        MinimumMaximumPath minMax = new MinimumMaximumPath();
        int[][] A = {{3, 4, 6, 3, 4}, {0, 2, 1, 1, 7}, {8, 8, 3, 2, 7}, {3, 2, 4, 9, 8}, {4, 1, 2, 0, 0}, {4, 6, 5, 4, 3}};//{{0, 0, 0, 0, 0, 1, 0}, {0, 1, 0, 1, 1, 0, 1}, {0, 1, 1, 1, 1, 0, 0}, {0, 1, 0, 0, 0, 0, 0}, {1, 0, 0, 0, 0, 0, 0}, {1, 0, 0, 0, 0, 0, 1}};
        System.out.println(minMax.maximumMinimumPath(A));
    }
}
