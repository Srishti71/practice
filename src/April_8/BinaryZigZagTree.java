package April_8;

import common.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Data {
    TreeNode value;
    boolean reverse;
    int level;

    Data(TreeNode value, boolean reverse, int level) {
        this.value = value;
        this.reverse = reverse;
        this.level = level;
    }
}

/*

    3
   / \
  9  20
    /  \
   15   7

 */
public class BinaryZigZagTree {

    public static void main(String s[]) {

        BinaryZigZagTree binary = new BinaryZigZagTree();
        TreeNode root = new TreeNode(3);
        TreeNode node1 = new TreeNode(9);
        TreeNode node2 = new TreeNode(20);
        TreeNode node3 = new TreeNode(15);
        TreeNode node4 = new TreeNode(7);
        TreeNode node5 = new TreeNode(34);
        TreeNode node6 = new TreeNode(44);

        TreeNode node7 = new TreeNode(55);
        TreeNode node8 = new TreeNode(66);

        root.left = node1;
        root.right = node2;

        node1.left = node5;
        node1.right = node6;

        node2.left = node3;
        node2.right = node4;

        node5.left = node7;
        node5.right = node8;

        binary.zigzagLevelOrder(root);
    }

    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {

        Queue<Data> queue = new LinkedList<>();

        List<List<Integer>> result = new ArrayList<>();

        if (root == null)
            return null;

        queue.add(new Data(root, false, 1));

        while (!queue.isEmpty()) {
            Data q = queue.poll();
            if (q.value == null)
                continue;
            int level = q.level;
            if (result.size() < level) {
                List l = new ArrayList();
                insertIntoList(l, q);
                result.add(l);
            } else {
                List l = result.get(level - 1);
                insertIntoList(l, q);
            }
            queue.add(new Data(q.value.left, !q.reverse, level + 1));
            queue.add(new Data(q.value.right, !q.reverse, level + 1));
        }
        System.out.println(result);
        return result;
    }

    private void insertIntoList(List l, Data q) {
        if (q.reverse) {
            if (l.isEmpty())
                l.add(q.value.val);
            else
                l.add(0, q.value.val);
        } else {
            l.add(q.value.val);
        }
    }
}
