package April_8;

import java.util.Arrays;

public class TwoSumLessThanK {

    public static void main(String s[]) {
        TwoSumLessThanK twoSumLessThanK = new TwoSumLessThanK();
        twoSumLessThanK.twoSumLessThanKNew(new int[]{34,23,1,24,75,33,54,8}, 60);
    }

    public int twoSumLessThanK(int[] A, int K) {
        int size = A.length;
        int max = 0;
        for (int i = 0; i < size; i++) {
            for (int j = i + 1; j < size; j++) {
                if (i == j) continue;
                int sum = A[i] + A[j];
                if (sum < K)
                    max = Math.max(sum, max);
            }
        }
        return max;
    }

    public int twoSumLessThanKNew(int[] A, int K) {

        Arrays.sort(A);
        int max = -1;
        int low = 0, high = A.length - 1;
        while (low < high) {
            int sum = A[low] + A[high];

            if (sum >= K) {
                high--;
            } else {
                max = Math.max(max, sum);
                low++;
            }
        }
        System.out.println(max);
        return max;
    }
}
