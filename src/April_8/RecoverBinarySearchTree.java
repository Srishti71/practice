package April_8;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class RecoverBinarySearchTree {

    int large, small;

    public static void main(String s[]) {

        RecoverBinarySearchTree rBst = new RecoverBinarySearchTree();

        TreeNode root = new TreeNode(1);
        TreeNode node1 = new TreeNode(3);
        TreeNode node3 = new TreeNode(2);

        root.left = node1;
        node1.right = node3;

        List<Integer> orderedList = new ArrayList<>();

        rBst.inorderTraversal(root, orderedList);
        int[] e = rBst.findDuplicate(orderedList);
        rBst.recover(root, 2, e[0], e[1]);
        //rBst.inorderTraversal(root, orderedList);
        System.out.println(orderedList);
    }

    public void recover(TreeNode root, int count, int small, int large) {

        if (root == null)
            return;

        if (root.val == large) {
            root.val = small;
        } else if (root.val == small) {
            root.val = large;
            return;
        }
        recover(root.left, count, small, large);
        recover(root.right, count, small, large);

    }

    public void inorderTraversal(TreeNode root, List<Integer> orderedList) {
        if (root == null)
            return;

        inorderTraversal(root.left, orderedList);
        orderedList.add(root.val);
        inorderTraversal(root.right, orderedList);
    }

    public int[] findDuplicate(List<Integer> orderedList) {

        int n = orderedList.size();
        int x = -1, y = -1;
        for (int i = 0; i < n - 1; ++i) {
            if (orderedList.get(i + 1) < orderedList.get(i)) {
                y = orderedList.get(i + 1);
                if (x == -1) x = orderedList.get(i);
                else break;
            }
        }
        return new int[]{x, y};
    }
}
