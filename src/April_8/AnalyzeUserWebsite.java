package April_8;

import javafx.util.Pair;

import java.util.*;

public class AnalyzeUserWebsite {

    // sort the website in the sequence of visit.

    // build graph which shows all the website visited by the user, sequencial order
    // generate all 3 sequence store it in map.
    // when I see the 3 sequence again I will increment the counter.

    public static void main(String s[]) {

        String[] username = {"h", "eiy", "cq", "h", "cq", "txldsscx", "cq", "txldsscx", "h", "cq", "cq"};
        int[] timestamp = {527896567, 334462937, 517687281, 134127993, 859112386, 159548699, 51100299, 444082139, 926837079, 317455832, 411747930};
        String[] website = {"hibympufi", "hibympufi", "hibympufi", "hibympufi", "hibympufi", "hibympufi", "hibympufi", "hibympufi", "yljmntrclw", "hibympufi", "yljmntrclw"};
        AnalyzeUserWebsite analyzeUserWebsite = new AnalyzeUserWebsite();
        analyzeUserWebsite.mostVisitedPattern(username, timestamp, website);

    }

    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {

        Map<String, List<Pair<String, Integer>>> map = new HashMap<>();

        for (int i = 0; i < website.length; i++) {
            List<Pair<String, Integer>> list = map.getOrDefault(username[i], new ArrayList<>());
            list.add(new Pair(website[i], timestamp[i]));
            map.put(username[i], list);
        }

        sortMap(map);
        System.out.println(map);
        Map<List<String>, Integer> result = new HashMap<>();

        generate3Sequence(map, result);

        if (result.isEmpty()) {
            return null;
        }

        List<String> l = getMaxElements(result);
        System.out.println(l);
        return l;
    }

    private List<String> getMaxElements(Map<List<String>, Integer> result) {
        int max = 0;
        for (Integer value : result.values()) {
            max = Math.max(value, max);
        }

        PriorityQueue<List<String>> listOfResult = new PriorityQueue<>((b, a) -> {
            int value = b.get(0).compareTo(a.get(0));
            if (value == 0) {
                value = b.get(1).compareTo(a.get(1));
                if (value == 0) {
                    return b.get(2).compareTo(a.get(2));
                } else {
                    return value;
                }
            }
            return value;
        });

        for (List<String> key : result.keySet()) {
            if (result.get(key) == max) {
                listOfResult.add(key);
            }
        }
        if (listOfResult.isEmpty())
            return null;
        return listOfResult.poll();
    }

    private void generate3Sequence(Map<String, List<Pair<String, Integer>>> map, Map<List<String>, Integer> result) {

        for (String key : map.keySet()) {
            List<Pair<String, Integer>> list = map.get(key);
            int size = list.size();
            if (size < 3)
                continue;
            Set<List<String>> set = new HashSet<>();
            for (int i = 0; i < size; i++) {
                for (int j = i + 1; j < size; j++) {
                    for (int k = j + 1; k < size; k++) {
                        List<String> l = new ArrayList<>();
                        l.add(list.get(i).getKey());
                        l.add(list.get(j).getKey());
                        l.add(list.get(k).getKey());
                        set.add(l);
                    }
                }
            }

            for (List<String> l : set) {
                int count = result.getOrDefault(l, 0);
                result.put(l, count + 1);
            }
        }
    }

    private void sortMap(Map<String, List<Pair<String, Integer>>> map) {

        for (String key : map.keySet()) {
            Collections.sort(map.get(key), Comparator.comparingInt(Pair::getValue));
        }
    }
}
