package feb_1;

import java.util.HashSet;
import java.util.Set;

public class Solution {

    Set<String> set = new HashSet();

    public int numTilePossibilities(String tiles) {

        findAllPossibleTitles(tiles, "");

        for(String s: set) {
            System.out.println(s);
        }
        return set.size();
    }

    private void findAllPossibleTitles(String tiles, String s) {

        if (tiles.isEmpty()) {
            return;
        }

        for (int i = 0; i < tiles.length(); i++) {

            String newStr = tiles.substring(0, i) + tiles.substring(i + 1);
            set.add(s + tiles.charAt(i));
            findAllPossibleTitles(newStr, s + tiles.charAt(i));

        }
    }

    public static void main(String s[]) {
        Solution sol = new Solution();
        System.out.println(sol.numTilePossibilities("AAABBC"));
    }
}