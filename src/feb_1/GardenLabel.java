package feb_1;

import javax.swing.*;
import java.util.*;

public class GardenLabel {

    Map<Integer, List<Integer>> map = new HashMap<>();

    public int[] gardenNoAdj(int N, int[][] paths) {
        if(paths.length == 0) {
            int[] answerArray = new int[N];
            for( int i =0; i< N ; i++) {
                answerArray[i] = 1;
            }
            return answerArray;
        }

        for (int i = 0; i < paths.length; i++) {
            List list = map.getOrDefault(paths[i][0], new ArrayList<Integer>());
            list.add(paths[i][1]);
            map.put(paths[i][0], list);
            List list2 = map.getOrDefault(paths[i][1], new ArrayList<Integer>());
            list2.add(paths[i][0]);
            map.put(paths[i][1], list2);
        }

        int gardenPosibility[][] = new int[N][4];

        for (int i = 0; i < N; i++) {
            Arrays.fill(gardenPosibility[i], -1);
        }

        ArrayList<Integer> answer = new ArrayList<Integer>();

        for (int i = 0; i < N; i++) {
            int flowerType = findPossibleFlowerType(gardenPosibility[i]);
            if (flowerType != -1) {
                answer.add(flowerType + 1);
                if(map.get(i + 1) != null) {

                    assignImposibleFlowerTypeToConnection(map.get(i + 1), gardenPosibility, flowerType);
                }
            }
        }

        int[] answerArray = new int[answer.size()];
        int i = 0;
        for (int element : answer) {
            answerArray[i] = element;
            i++;
        }

        return  answerArray;
    }

    private void assignImposibleFlowerTypeToConnection(List<Integer> list, int[][] gardenPosibility, int flowerType) {
        for (int element : list) {
            gardenPosibility[element-1][flowerType] = 0;
        }
    }

    private int findPossibleFlowerType(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == -1) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String s[]) {
        GardenLabel sol = new GardenLabel();
        int N = 1000;
        int[][] path = {{1,2}};
        System.out.println(sol.gardenNoAdj(N, path));
    }
}