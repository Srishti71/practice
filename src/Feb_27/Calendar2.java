package Feb_27;

import java.util.ArrayList;

//https://leetcode.com/problems/my-calendar-ii/
public class Calendar2 {
    ArrayList<Integer[]> calendar = new ArrayList<>();
    ArrayList<Integer[]> overlap = new ArrayList<>();

    public static void main(String s[]) {

    }

    public Calendar2() {

    }

    public boolean book(int start, int end) {
        for (Integer[] ele : calendar) {
            if (ele[0] < end && start < ele[1]) {
                Integer[] newEle = new Integer[2];
                newEle[0] = Math.max(start, ele[0]);
                newEle[1] = Math.min(end, ele[1]);
                overlap.add(newEle);
            }
        }

        for (Integer[] ele : overlap) {
            if (ele[0] < end && start < ele[1]) return false;
        }

        calendar.add(new Integer[]{start, end});
        return true;
    }
}