package Feb_27;

public class LargestRectangeHist {

    int max = Integer.MIN_VALUE;
    public int largestRectangleArea(int[] heights) {
        if(heights.length == 0){
            return 0;
        }
        for(int i =0; i< heights.length; i++) {
            int currentMin = heights[i];
            for( int j = i; j < heights.length; j++ ) {
                currentMin = Math.min(currentMin, heights[j]);
                max = Math.max(max, currentMin*(j-i+1));
            }
        }
        return max;
    }

    public static void main(String s[]) {
        int heights[] = {2,1,5,6,2,3};
        LargestRectangeHist largestRectangeHist = new LargestRectangeHist();
        System.out.println(largestRectangeHist.largestRectangleArea(heights));
    }
}
